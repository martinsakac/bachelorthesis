<%-- 
    Document   : form
    Created on : Dec 25, 2013, 2:37:23 PM
    Author     : martin
--%>

<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="form-group">
    <s:label class="col-md-2 control-label" for="b1" name="vlastnik.label.meno"/>
    <div class="col-md-4">
        <s:text class="form-control" id="b1" name="vlastnik.meno"/>
    </div>
</div>
    
<div class="form-group">
    <s:label class="col-md-2 control-label" for="b2" name="vlastnik.label.priezvisko"/>
    <div class="col-md-4">
        <s:text class="form-control" id="b2" name="vlastnik.priezvisko"/>
    </div>
</div>
    
<div class="form-group">
    <s:label class="col-md-2 control-label" for="b3" name="vlastnik.label.titul"/>
    <div class="col-md-2">
        <s:text class="form-control" id="b3" name="vlastnik.titul"/>
    </div>
</div>
    
<div class="form-group">
    <s:label class="col-md-2 control-label" for="datepicker" name="vlastnik.label.datumNarodenia"/>
    <div class="col-md-2">
        <s:text class="form-control" id="datepicker" name="vlastnik.datumNarodenia"/>
    </div>
</div>
    
<div class="form-group">
    <s:label class="col-md-2 control-label" for="b5" name="vlastnik.label.miestoNarodenia"/>
    <div class="col-md-4">
        <s:text class="form-control" id="b5" name="vlastnik.miestoNarodenia"/>
    </div>
</div>
    
<div class="form-group">
    <s:label class="col-md-2 control-label" for="b6" name="vlastnik.label.adresa"/>
    <div class="col-md-6">
        <s:text class="form-control" id="b6" name="vlastnik.adresa"/>
    </div>
</div>
    
<div class="form-group">
    <s:label class="col-md-2 control-label" for="b7" name="vlastnik.label.cisloUctu"/>
    <div class="col-md-4">
        <s:text class="form-control" id="b7" name="vlastnik.cisloUctu"/>
    </div>
</div>
    
<div class="form-group">
    <s:label class="col-md-2 control-label" for="b8" name="vlastnik.label.ico"/>
    <div class="col-md-2">
        <s:text class="form-control" id="b8" name="vlastnik.ico"/>
    </div>
</div>


