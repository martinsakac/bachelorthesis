<%-- 
    Document   : listPodielyVlastnika
    Created on : Jan 27, 2014, 9:51:37 PM
    Author     : martin
--%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>

<h4><f:message key="vlastnik.detaily.podiely.title"/></h4>
<table class="table table-bordered">
    <tr>
        <th><f:message key="vlastnik.detaily.oblast"/></th>
        <th><f:message key="vlastnik.detaily.rozloha"/></th>
    </tr>
    <c:forEach items="${vlastnik.listParciel}" var="parcela">
        <tr>
            <td><c:out value="${parcela.katastralneUzemie.nazov}"/></td>
            <td><c:out value="${parcela.vymera}"/></td>
        </tr>
    </c:forEach>
</table>