<%-- 
    Document   : edit
    Created on : Dec 25, 2013, 4:55:41 PM
    Author     : martin
--%>

<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>

<s:layout-render name="/layout.jsp" titlekey="vlastnik.edit.title">
    <s:layout-component name="body">
        <s:useActionBean beanclass="fbm.vutbr.martinsakac.bakalarka.web.VlastnikActionBean" var="actionBean"/>

        <s:form beanclass="fbm.vutbr.martinsakac.bakalarka.web.VlastnikActionBean">
            <s:hidden name="vlastnik.id"/>
            <fieldset><legend><f:message key="vlastnik.edit.edit"/></legend>
                <%@include file="form.jsp"%>
                <s:submit name="save"><f:message key="vlastnik.edit.save"/></s:submit>
            </fieldset>
        </s:form>

    </s:layout-component>
</s:layout-render>
