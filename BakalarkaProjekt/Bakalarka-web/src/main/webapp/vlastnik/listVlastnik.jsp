<%-- 
    Document   : list
    Created on : Dec 23, 2013, 4:31:33 PM
    Author     : martin
--%>

<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>

<s:layout-render name="/layoutIntraPortal.jsp" titlekey="fieldagenda.title.vlastnik">
    <s:layout-component name="bodyIntra">
        
        <div class="row">
            <div class="span6" style="text-align:center">
                <h4 id="newVlastnikTooltip" class="btn btn-default newRecordTitle" data-toggle="tooltip" title="Click to hide the form" data-placement="right">
                    <f:message key="vlastnik.list.newvlastnik"/>
                </h4>
            </div>
        </div>
        
        <s:useActionBean beanclass="fbm.vutbr.martinsakac.bakalarka.web.VlastnikActionBean" var="actionBean"/>
        <div class="container newRecordForm paddingTop30">
            <div class="row">
                <div class="col-md-12">
                    <s:form class="form-horizontal" beanclass="fbm.vutbr.martinsakac.bakalarka.web.VlastnikActionBean">
                        <s:hidden name="vlastnik.id"/>
                        <%@include file="form.jsp"%>
                        <div class="form-group">
                            <div class="col-sm-6 col-sm-offset-2">
                                <s:submit class="btn btn-primary" name="add"><f:message key="vlastnik.addupdate.save"/></s:submit>
                                <s:submit class="btn btn-default" name="cancel"><f:message key="vlastnik.addupdate.cancel"/></s:submit>
                                <button class="btn btn-default" type="reset"><f:message key="control.reset"/></button>
                            </div>
                        </div>
                    </s:form>
                </div>
            </div>
        </div>
        
        <div class="row paddingTop30">
            <div class="span8" style="text-align:center">
                <h2><f:message key="vlastnik.list.allvlastnik"/></h2>
            </div>
        </div>
        
        <!-- Checkbox na deselectovanie zobrazovanych stlpcov -->    
            <div class="row">
                <div class="span6" style="text-align:center">
                    <h4 id="vlastnikCheckbox" class="btn btn-default deselectColumnsCheckbox" 
                        data-toggle="tooltip" title="Click to show options" data-placement="left">
                        <f:message key="vlastnik.list.deselectcolumns"/>
                    </h4>
                
                <div class="col-md-18 deselectColumnsCheckboxContent" style="display: none;">
                    <label class="checkbox-inline">
                        <input id="checkboxMeno" type="checkbox" value="vlastnik.label.meno">
                        <f:message key="vlastnik.label.meno"/>
                    </label>
                    <label class="checkbox-inline">
                        <input id="checkboxPriezvisko" type="checkbox" value="vlastnik.label.priezvisko">
                        <f:message key="vlastnik.label.priezvisko"/>
                    </label>
                    <label class="checkbox-inline">
                        <input id="checkboxTitul" type="checkbox" value="vlastnik.label.titul">
                        <f:message key="vlastnik.label.titul"/>
                    </label>
                    <label class="checkbox-inline">
                        <input id="checkboxEvCislo" type="checkbox" value="vlastnik.label.evCislo">
                        <f:message key="vlastnik.label.evCislo"/>
                    </label>
                    <label class="checkbox-inline">
                        <input id="checkboxDatumNarodenia" type="checkbox" value="vlastnik.label.datumNarodenia">
                        <f:message key="vlastnik.label.datumNarodenia"/>
                    </label>
                    <label class="checkbox-inline">
                        <input id="checkboxMiestoNarodenia" type="checkbox" value="vlastnik.label.miestoNarodenia">
                        <f:message key="vlastnik.label.miestoNarodenia"/>
                    </label>
                    <label class="checkbox-inline">
                        <input id="checkboxAdresa" type="checkbox" value="vlastnik.label.adresa">
                        <f:message key="vlastnik.label.adresa"/>
                    </label>
                    <label class="checkbox-inline">
                        <input id="checkboxCisloUctu" type="checkbox" value="vlastnik.label.cisloUctu">
                        <f:message key="vlastnik.label.cisloUctu"/>
                    </label>
                    <label class="checkbox-inline">
                        <input id="checkboxIco" type="checkbox" value="vlastnik.label.ico">
                        <f:message key="vlastnik.label.ico"/>
                    </label>
                </div>
                    </div>
            </div>
        
        <div class="container">
            <div class="row paddingTop30">
                <div class="col-lg-10">
                <table class="table-hover table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th> </th>
                        <th>#</th>
                        <th id="ownerPriezviskoHead">
                            <f:message key="vlastnik.label.priezvisko"/>
                        </th>
                        <th id="ownerMenoHead"><f:message key="vlastnik.label.meno"/></th>
                        <th id="ownerTitulHead"><f:message key="vlastnik.label.titul"/></th>
                        <th id="ownerEvCisloHead"><f:message key="vlastnik.label.evCislo"/></th>
                        <th id="ownerDatumNarodeniaHead">
                            <f:message key="vlastnik.label.datumNarodenia"/>
                        </th>
                        <th id="ownerMiestoNarodenia"><f:message key="vlastnik.label.miestoNarodenia"/></th>
                        <th id="ownerAdresaHead"><f:message key="vlastnik.label.adresa"/></th>
                        <th id="ownerCisloUctuHead"><f:message key="vlastnik.label.cisloUctu"/></th>
                        <th id="ownerIcoHead"><f:message key="vlastnik.label.ico"/></th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <!--tlacitka na radenie-->
                    <tbody>
                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                            <s:link class="btn btn-default narrow-button" beanclass="fbm.vutbr.martinsakac.bakalarka.web.VlastnikActionBean" event="list" title="Ascending">
                                <s:param name="owner.order.column" value="priezvisko"/>
                                <s:param name="owner.order.direction" value="asc"/>
                                <span class="glyphicon glyphicon-arrow-up"></span>
                            </s:link>
                            <s:link class="btn btn-default narrow-button" beanclass="fbm.vutbr.martinsakac.bakalarka.web.VlastnikActionBean" event="list" title="Descending">
                                <s:param name="owner.order.column" value="priezvisko"/>
                                <s:param name="owner.order.direction" value="desc"/>
                                <span class="glyphicon glyphicon-arrow-down"></span>                                
                            </s:link>
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>
                            <s:link class="btn btn-default narrow-button" beanclass="fbm.vutbr.martinsakac.bakalarka.web.VlastnikActionBean" event="list" title="Ascending">
                                <s:param name="owner.order.column" value="datumNarodenia"/>
                                <s:param name="owner.order.direction" value="asc"/>
                                <span class="glyphicon glyphicon-arrow-up"></span>
                            </s:link>
                            <s:link class="btn btn-default narrow-button" beanclass="fbm.vutbr.martinsakac.bakalarka.web.VlastnikActionBean" event="list" title="Descending">
                                <s:param name="owner.order.column" value="datumNarodenia"/>
                                <s:param name="owner.order.direction" value="desc"/>
                                <span class="glyphicon glyphicon-arrow-down"></span>                                
                            </s:link>
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <c:set var="counter" value="0" scope="page" />
                    <c:forEach items="${actionBean.vlastniciSoZmluvami}" var="vlastnik">
                        <c:set var="counter" value="${counter + 1}" scope="page"/>
                        <tr>
                            <td class="expandDetails" >
                                <span class="btn btn-default glyphicon glyphicon-circle-arrow-right" 
                                      title="Click to expand details"></span>
                            </td>
                            <td>${counter}</td>
                            <td><c:out value="${vlastnik.priezvisko}"/></td>
                            <td><c:out value="${vlastnik.meno}"/></td>
                            <td><c:out value="${vlastnik.titul}"/></td>
                            <td><c:out value="${vlastnik.evidencneCislo}"/></td>
                            <td><c:out value="${vlastnik.datumNarodenia}"/></td>
                            <td><c:out value="${vlastnik.miestoNarodenia}"/></td>
                            <td><c:out value="${vlastnik.adresa}"/></td>
                            <td><c:out value="${vlastnik.cisloUctu}"/></td>
                            <td><c:out value="${vlastnik.ico}"/></td>
                            <td>
                                <s:link class="btn btn-default" beanclass="fbm.vutbr.martinsakac.bakalarka.web.VlastnikActionBean" event="edit">
                                    <s:param name="vlastnik.id" value="${vlastnik.id}"/>
                                    <f:message key="vlastnik.list.edit"/>
                                </s:link>
                            </td>
                            <td>
                                <s:form beanclass="fbm.vutbr.martinsakac.bakalarka.web.VlastnikActionBean">
                                    <s:hidden name="vlastnik.id" value="${vlastnik.id}"/>
                                    <s:submit class="btn btn-danger" name="delete" onclick="return confirm('Are you sure you want delete this record?')">
                                        <f:message key="vlastnik.list.delete"/>
                                    </s:submit>
                                </s:form>
                            </td>
                        </tr>
                        <!-- Detaily o Vlastnikovi - tabulka parcel(resp. kat. uzemi) + zoznam zmluv -->
                        <tr class="hiddenRow">
                            <td></td>
                            <td colspan="3">
                                <%@include file="listPodielyVlastnika.jsp" %>
                            </td>
                            <td colspan="10">
                                <%@include file="listZmluvyVlastnika.jsp" %>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                </div>
            <!-- koniec obalovacich divov -->
            </div>
        </div>
                
    </s:layout-component>
</s:layout-render>
