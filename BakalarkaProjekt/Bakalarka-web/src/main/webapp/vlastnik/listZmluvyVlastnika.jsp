<%-- 
    Document   : listZmluvyVlastnika
    Created on : Jan 27, 2014, 9:56:41 PM
    Author     : martin
--%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>

<h4><f:message key="vlastnik.detaily.zmluvy.title"/></h4>
<table class="table table-bordered">
    <tr>
        <th><f:message key="vlastnik.detaily.zmluvy.od"/></th>
        <th><f:message key="vlastnik.detaily.zmluvy.do"/></th>
        <th><f:message key="vlastnik.detaily.zmluvy.pocetRokov"/></th>
        <th><f:message key="vlastnik.detaily.zmluvy.sadzba"/></th>
        <th><f:message key="vlastnik.detaily.zmluvy.sucetVymier"/></th>
        <th><f:message key="vlastnik.detaily.zmluvy.vyplatenaCiastka"/></th>
    </tr>
    <c:forEach items="${vlastnik.listZmluv}" var="zmluva">
        <tr>
            <td><c:out value="${zmluva.platnostOd}"/></td>
            <td><c:out value="${zmluva.platnostDo}"/></td>
            <td><c:out value="${zmluva.pocetRokov}"/></td>
            <td><c:out value="${zmluva.sadzba}"/></td>
            <td><c:out value="${zmluva.sucetVymier}"/></td>
            <td><c:out value="${zmluva.vyplatenaCiastka}"/></td>
            <td>
                <s:link class="btn btn-info" event="displayDetail" 
                        beanclass="fbm.vutbr.martinsakac.bakalarka.web.NajomnaZmluvaActionBean">
                    <s:param name="najomnaZmluva.id" value="${zmluva.id}" />
                    <f:message key="najomnaZmluva.label.details" />
                    <s:param name="chosenTab" value="najomnaZmluva"/>
                </s:link>
            </td>
        </tr>
    </c:forEach>
</table>
