$(function() {
    $( "#datepicker" ).datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: "yy-mm-dd"
    });
});

$(function() {
    $( "#datepicker1" ).datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: "yy-mm-dd"
    });
});

$(function() {
    $( "#datepicker2" ).datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: "yy-mm-dd"
    });
});

function vyberVlastnikaNaPodiel(idVlastnika, menoVlastnika, priezviskoVlastnika, datumNarodeniaVlastnika)
{
    document.formularPodiel.vybranyVlastnikPopis.value = 
            menoVlastnika + " " + priezviskoVlastnika + ", " + datumNarodeniaVlastnika;
    document.formularPodiel.podielHidden.value = idVlastnika;
};

//function rozbalZvyrazniVybratyRiadok(vybratyRiadok)
//{
//    var rodic = vybratyRiadok.parentNode;
//    if (vybratyRiadok.className === "unRevealedRowSprite"){
//        vybratyRiadok.className = "revealedRowSprite";
//        rodic.setAttribute("class", "listTableRowHooveredOrClicked");
//        
//    }
//    else{
//        vybratyRiadok.className = "unRevealedRowSprite";
//        rodic.className = "listTableRow";
//    }
//    $(rodic).next().slideToggle("slow");
//};

// zmena ikony pri rozklinuti detailov o majitelovi
$(function(){
    $(".expandDetails").click(function(){
        $(this).parent().next().slideToggle();
        $(this).children("span:first-child").toggleClass("glyphicon-circle-arrow-right");
        $(this).children("span:first-child").toggleClass("glyphicon-circle-arrow-down");
    });
});

//-----------------------------------------------------------------------------
// Sliding to top

$(function() {
    $('.goToTop').click(function(){
        $('html, body').animate({scrollTop:0}, 1000);
        return false;
    });
});

// sliding to each section of index(home) page
$(function() {
    $("#goToStaff").click(function(){
        $("html, body").animate({
            scrollTop:$("#staffSection").offset().top}, 1000);
        return false;
    });
    
    $("#goToAddress").click(function(){
        $("html, body").animate({
            scrollTop:$("#addressSection").offset().top}, 1000);
        return false;
    });
    
    $(".goToContactDetails").click(function(){
        $("html, body").animate({
            scrollTop:$("#contactDetailsSection").offset().top}, 1000);
        return false;
    });
    
    $("#goToMilkProduction").click(function(){
        $("html, body").animate({
            scrollTop:$("#milkProductionSection").offset().top}, 1000);
        return false;
    });
});
//-----------------------------------------------------------------------------

$(function(){
    $(".myListItem").click(function(){ 
        $(".active").removeClass("active");
        $(this).addClass("active");
    });
});

//$(function(){
//    $(".myListItem2").click(function(){ 
//        $(".active").removeClass("active");
//        $(this).addClass("active");
//    });
//});

$(document).ready(function() {
    $(".myListItem2").click(function(){ 
        $(".active").removeClass("active");
        $(this).addClass("active");
    });
});

$(function(){
    $(".myRollTab").click(function(){
        $(".myRollTabBody").slideToggle();
    });
});

// rolovanie ponuk 
$(function(){
    $(".newRecordTitle").click(function(){
        $(".newRecordForm").slideToggle();
    });
});

$(function(){
    $(".deselectColumnsCheckbox").click(function(){
        $(".deselectColumnsCheckboxContent").slideToggle();
    });
});

//$(function(){
//    $(".editRecord").click(function(){
//        $("document").ready(function(){
//            $(".newRecordForm").slideToggle();
//        });
//    });
//});

//$("document").ready(function(){
//    $(".editRecord").click(function(){
//        $(".newRecordForm").slideToggle();
//    });
//});

// deselectovanie zobrazenych stlpcov v zozname majitelov
$("document").ready(function(){
    $("#checkboxMeno").click(function(){
        $('td:nth-child(3),th:nth-child(3)').toggle();
    });
    $("#checkboxPriezvisko").click(function(){
        $('td:nth-child(4),th:nth-child(4)').toggle();
    });
    $("#checkboxTitul").click(function(){
        $('td:nth-child(5),th:nth-child(5)').toggle();
    });
    $("#checkboxEvCislo").click(function(){
        $('td:nth-child(6),th:nth-child(6)').toggle();
    });
    $("#checkboxDatumNarodenia").click(function(){
        $('td:nth-child(7),th:nth-child(7)').toggle();
    });
    $("#checkboxMiestoNarodenia").click(function(){
        $('td:nth-child(8),th:nth-child(8)').toggle();
    });
    $("#checkboxAdresa").click(function(){
        $('td:nth-child(9),th:nth-child(9)').toggle();
    });
    $("#checkboxCisloUctu").click(function(){
        $('td:nth-child(10),th:nth-child(10)').toggle();
    });
    $("#checkboxIco").click(function(){
        $('td:nth-child(11),th:nth-child(11)').toggle();
    });
});

// tooltipy - info ked hoovernes button na pridanie noveho recordu
$("document").ready(function(){
    $("#newVlastnikTooltip").tooltip();
    $("#vlastnikCheckbox").tooltip();
});


