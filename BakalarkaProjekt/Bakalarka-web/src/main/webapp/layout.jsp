<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ page session="true" %>
<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<s:layout-definition>
<!DOCTYPE html>
<html lang="${pageContext.request.locale}">
<head>
    <title><f:message key="${titlekey}"/></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" >
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css" >
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style.css" />
    
    <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
    <script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/javascript/script.js"></script>
    <script src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.js" ></script>
    <s:layout-component name="header"/>
</head>
<body>
<!-------------------------- Horny navigacny bar ------------------------------>
    <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <a href="${pageContext.request.contextPath}" class="navbar-brand">Poľnohospodárske družstvo vo Vrábľoch</a>
            <button class="navbar-toggle" data-toggle="collapse" data-target=".navHeaderCollapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="collapse navbar-collapse navHeaderCollapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="active">
                        <a href="${pageContext.request.contextPath}"><f:message key="navigation.top.home"/></a>
                    </li>
                    <li><s:link id="goToStaff" href="${pageContext.request.contextPath}/staffSection"><f:message key="navigation.top.staff"/></s:link></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><f:message key="navigation.top.contact"/> <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><s:link id="goToAddress" href="${pageContext.request.contextPath}/addressSection"><f:message key="navigation.top.contact.address"/></s:link></li>
                            <li><s:link class="goToContactDetails" href="${pageContext.request.contextPath}/contactDetailsSection"><f:message key="navigation.top.contact.phone"/></s:link></li>
                            <li><s:link class="goToContactDetails" href="${pageContext.request.contextPath}/contactDetailsSection"><f:message key="navigation.top.contact.mail"/></s:link></li>
                            <li><s:link class="goToContactDetails" href="${pageContext.request.contextPath}/contactDetailsSection"><f:message key="navigation.top.contact.ico"/></s:link></li>
                        </ul>
                    </li>
                    <li><s:link id="goToMilkProduction" href="${pageContext.request.contextPath}/milkProductionSection"><f:message key="navigation.top.milkProduction"/></s:link></li>
                    <c:choose>
                        <c:when test="${sessionScope.loggedIn == true}">
                            <li>
                                <s:link beanclass="fbm.vutbr.martinsakac.bakalarka.web.NavigationActionBean" event="goToIntraportal">
                                    <f:message key="navigation.top.intraportal"/>
                                </s:link>
                            </li>
                            <li id="userNameSurname">
                                <s:link beanclass="fbm.vutbr.martinsakac.bakalarka.web.SecurityActionBean" event="logout">
                                    <f:message key="app.logout"/>
                                </s:link>
                            </li>
                        </c:when>
                        <c:otherwise>
                            <li>
                                <a href="#loginModal" data-toggle="modal"><f:message key="navigation.top.login"/></a>
                            </li>
                        </c:otherwise>
                    </c:choose>
                </ul>
            </div>
        </div>
    </div>
   
<!-- Obsah stranky -->        
    <div id="content">
        <s:errors/>
        <s:messages/>
        <s:layout-component name="body"/>
    </div>
    
<!-- Paticka -->
    <div class="navbar navbar-inverse navbar-fixed-bottom">
        <div class="container">
            <p class="navbar-text pull-left"><f:message key="app.site.footerDescription"/><a href="#"> Martin Sakáč</a></p>
            <s:link href="#" class="goToTop navbar-btn btn-primary btn pull-right"><f:message key="app.site.backToTop"/></s:link>
        </div>
    </div>
    
<!-- Login Modal -->
    <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            <s:form class="form-horizontal" beanclass="fbm.vutbr.martinsakac.bakalarka.web.SecurityActionBean">
                <div class="modal-header">
                    <h2><f:message key="app.login.title"/></h2>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <s:label class="col-lg-3 control-label" for="loginTextField" name="uzivatel.label.uzivatelskeMeno"/>
                        <div class="col-lg-8">
                            <s:text class="form-control" id="loginTextField" name="userId" />
                        </div>
                    </div>
                    <div class="form-group">
                        <s:label class="col-lg-3 control-label" for="passwordTextField" name="uzivatel.label.uzivatelskeMeno"/>
                        <div class="col-lg-8">
                            <s:password class="form-control" id="passwordTextField" name="password" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input  class="btn btn-default" type="reset" value="Reset" />
                    <button id="closeReset" type="button" class="btn btn-default" data-dismiss="modal"><f:message key="app.login.close"/></button>
                    <s:submit class="btn btn-primary" name="submitLogin"><f:message key="uzivatel.login"/></s:submit>
                </div>
            </s:form>
            </div>
        </div>
    </div>

</body>
</html>
</s:layout-definition>