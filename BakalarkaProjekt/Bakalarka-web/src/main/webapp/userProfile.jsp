<%-- 
    Document   : userProfile
    Created on : Feb 11, 2014, 3:40:36 PM
    Author     : martin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<s:layout-render name="/layoutIntraPortal.jsp" titlekey="titlekey.welcome">
    <s:layout-component name="bodyIntra">
        
        <s:form class="form-horizontal" beanclass="fbm.vutbr.martinsakac.bakalarka.web.ProfilActionBean">
            <div class="form-group">
                <s:label class="col-md-2 control-label" for="u1" name="uzivatel.label.meno"/>
                <div class="col-md-4">
                    <s:text class="form-control" id="u1" name="uzivatel.meno" />
                </div>
            </div>
            <div class="form-group">
                <s:label class="col-md-2 control-label" for="u2" name="uzivatel.label.priezvisko"/>
                <div class="col-md-4">
                    <s:text class="form-control" id="u2" name="uzivatel.priezvisko" />
                </div>
            </div>
            <div class="form-group">
                <s:label class="col-md-2 control-label" for="u3" name="uzivatel.label.uzivatelskeMeno"/>
                <div class="col-md-4">
                    <s:text class="form-control" id="u3" name="uzivatel.uzivatelskeMeno" />
                </div>
            </div>
            <div class="form-group">
                <s:label class="col-md-2 control-label" for="u4" name="uzivatel.label.uzivatelskeHeslo"/>
                <div class="col-md-4">
                    <s:password class="form-control" id="u4" name="uzivatel.uzivatelskeHeslo" />
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-6 col-sm-offset-2">
                    <s:submit class="btn btn-primary" name="updateProfile"><f:message key="control.save" /></s:submit>
                    <s:submit class="btn btn-default" name="cancel"><f:message key="control.cancel" /></s:submit>
                    <button class="btn btn-default" type="reset"><f:message key="control.reset"/></button>
                </div>
            </div>
        </s:form>
        
    </s:layout-component>
</s:layout-render>