<%-- 
    Document   : listVlastnik
    Created on : Jan 16, 2014, 10:56:17 PM
    Author     : martin
--%>

<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<table class="listTable">
    <tr class="listTableHead">
        <th>ID</th>
        <th><f:message key="vlastnik.label.meno"/></th>
        <th><f:message key="vlastnik.label.priezvisko"/></th>
        <th><f:message key="vlastnik.label.titul"/></th>
        <th><f:message key="vlastnik.label.datumNarodenia"/></th>
        <th><f:message key="vlastnik.label.miestoNarodenia"/></th>
        <th><f:message key="vlastnik.label.adresa"/></th>
        <th><f:message key="vlastnik.label.cisloUctu"/></th>
        <th><f:message key="vlastnik.label.ico"/></th>
    </tr>
    <c:forEach items="${actionBean.listVlastnik}" var="vlastnik">
        <tr class="listTableRow" onclick="vyberVlastnikaNaPodiel('${vlastnik.id}', 
                    '${vlastnik.meno}', '${vlastnik.priezvisko}', '${vlastnik.datumNarodenia}');" 
                    onmouseover="this.className='listTableRowHoovered';" 
                    onmouseout="this.className='listTableRow';">
            <td>${vlastnik.id}</td>
            <td><c:out value="${vlastnik.meno}"/></td>
            <td><c:out value="${vlastnik.priezvisko}"/></td>
            <td><c:out value="${vlastnik.titul}"/></td>
            <td><c:out value="${vlastnik.datumNarodenia}"/></td>
            <td><c:out value="${vlastnik.miestoNarodenia}"/></td>
            <td><c:out value="${vlastnik.adresa}"/></td>
            <td><c:out value="${vlastnik.cisloUctu}"/></td>
            <td><c:out value="${vlastnik.ico}"/></td>
        </tr>
    </c:forEach>
</table>