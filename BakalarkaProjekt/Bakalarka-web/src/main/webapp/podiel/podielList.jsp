<%-- 
    Document   : podielList
    Created on : Jan 14, 2014, 4:11:42 PM
    Author     : martin
--%>

<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>

<s:layout-render name="/layoutIntraPortal.jsp" titlekey="fieldagenda.title.podiel" >
    <s:layout-component name="bodyIntra">
        
        <s:useActionBean beanclass="fbm.vutbr.martinsakac.bakalarka.web.PodielActionBean" var="actionBean"/>
        <h4><f:message key="podiel.list.newpodiel"/></h4>
        <s:form beanclass="fbm.vutbr.martinsakac.bakalarka.web.PodielActionBean" name="formularPodiel">
            <fieldset>
                <%@include file="formPodiel.jsp" %>
                <s:hidden name="vlastnik.id" id="podielHidden"/>
                <s:submit name="save"><f:message key="control.save"/></s:submit>
                <s:submit name="cancel"><f:message key="control.cancel"/></s:submit>
                <h4><f:message key="podiel.list.allvlastnik"/></h4>
                <%@include file="listVlastnik.jsp" %>
            </fieldset>
        </s:form>
        
        <h4><f:message key="podiel.list.allpodiel"/></h4>
        
        <table class="listTable">
            <tr class="listTableHead">
                <th><f:message key="podiel.id"/></th>
                <th><f:message key="vlastnik.id"/></th>
                <th><f:message key="podiel.podiel"/></th>
            </tr>
            <c:forEach items="${actionBean.listPodiel}" var="podiel">
                <tr class="listTableRow">
                    <td>${podiel.id}</td>
                    <td>${podiel.vlastnik.id}</td>
                    <td>${podiel.percentualnyPodiel}</td>
                </tr>
            </c:forEach>
        </table>
        
    </s:layout-component>
</s:layout-render>
