<%-- 
    Document   : detailNajomnaZmluva
    Created on : 11.5.2014, 18:58:55
    Author     : martin sakac
--%>

<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>

<s:layout-render name="/layoutIntraPortal.jsp" titlekey="fieldagenda.title.najomnaZmluva">
    <s:layout-component name="bodyIntra">   
        
    <s:useActionBean beanclass="fbm.vutbr.martinsakac.bakalarka.web.NajomnaZmluvaActionBean" var="actionBean" />
    <h3><f:message key="najomnaZmluva.detail.title"/></h3>
    
    <table class="table table-hover table-striped">
        <tr>
            <th><f:message key="najomnaZmluva.detail.owner" /></th>
            <td>${actionBean.nzDetail.vlastnik.meno} ${actionBean.nzDetail.vlastnik.priezvisko}</td>
        </tr>
        <tr>
            <th><f:message key="najomnaZmluva.label.platnostOd" /></th>
            <td>${actionBean.nzDetail.platnostOd}</td>
        </tr>
        <tr>
            <th><f:message key="najomnaZmluva.label.platnostDo" /></th>
            <td>${actionBean.nzDetail.platnostDo}</td>
        </tr>
        <tr>
            <th><f:message key="najomnaZmluva.detail.pocetRokov" /></th>
            <td>${actionBean.nzDetail.pocetRokov}</td>
        </tr>
        <tr>
            <th><f:message key="najomnaZmluva.detail.land" /></th>
            <td>
                <c:forEach items="${actionBean.nzDetail.parcely}" var="parcela">
                    <c:out value="${parcela.vymera}"/> ha - 
                    <b><c:out value="${parcela.katastralneUzemie.nazov}"/></b>
                    <br>
                </c:forEach>
            </td>
        </tr>
        <tr>
            <th><f:message key="najomnaZmluva.detail.sucetVymier" /></th>
            <td>${actionBean.nzDetail.sucetVymier}</td>
        </tr>
        <tr>
            <th><f:message key="najomnaZmluva.detail.sadzba" /></th>
            <td>${actionBean.nzDetail.sadzba}</td>
        </tr>
        <tr>
            <th><f:message key="najomnaZmluva.detail.vyplatenaCiastka" /></th>
            <td><b>${actionBean.nzDetail.vyplatenaCiastka}</b></td>
        </tr>
    </table>
        
    <s:link event="addFormDisplay" class="btn btn-info" 
            beanclass="fbm.vutbr.martinsakac.bakalarka.web.NajomnaZmluvaActionBean" >
        <s:param name="najomnaZmluva.id" value="${actionBean.nzDetail.id}" />
        <f:message key="najomnaZmluva.detail.addNew" />
    </s:link>
    <s:link event="editNajomnaZmluva" class="btn btn-info" 
            beanclass="fbm.vutbr.martinsakac.bakalarka.web.NajomnaZmluvaActionBean" >
        <s:param name="najomnaZmluva.id" value="${actionBean.nzDetail.id}" />
        <f:message key="najomnaZmluva.detail.edit" />
    </s:link> 
    <s:link event="exportToPdf" class="btn btn-info" 
            beanclass="fbm.vutbr.martinsakac.bakalarka.web.NajomnaZmluvaActionBean" >
        <s:param name="najomnaZmluva.id" value="${actionBean.nzDetail.id}" />
        <f:message key="najomnaZmluva.detail.exportToPdf" />
    </s:link>
    <br/>
    <br/>
    <s:link event="list" beanclass="fbm.vutbr.martinsakac.bakalarka.web.NajomnaZmluvaActionBean">
        <f:message key="najomnaZmluva.detail.link.backToList" />
    </s:link> 
    
    </s:layout-component>
</s:layout-render>