<%-- 
    Document   : addNajomnaZmluva
    Created on : 19.5.2014, 10:37:32
    Author     : Sammy Guergachi <sguergachi at gmail.com>
--%>

<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>

<s:layout-render name="/layoutIntraPortal.jsp" titlekey="fieldagenda.title.najomnaZmluva">
    <s:layout-component name="bodyIntra">   

    <s:useActionBean beanclass="fbm.vutbr.martinsakac.bakalarka.web.NajomnaZmluvaActionBean" 
                     var="actionBean" />
    <h3><f:message key="najomnaZmluva.add.title"/></h3>
    
    
        
    <s:form class="form-horizontal" beanclass="fbm.vutbr.martinsakac.bakalarka.web.NajomnaZmluvaActionBean">
        <div class="form-group">
            <s:label class="col-md-3 control-label" for="nzOwner" name="najomnaZmluva.detail.owner" />
            <div class="col-md-4">
                <c:out value="${actionBean.nzDetail.vlastnik.meno} ${actionBean.nzDetail.vlastnik.priezvisko}"/>
            </div>
        </div>
        <div class="form-group">
            <s:label class="col-md-3 control-label" for="nzLand" name="najomnaZmluva.detail.land" />
            <div class="col-md-4">
                <c:forEach items="${actionBean.nzDetail.parcely}" var="parcela">
                    <c:out value="${parcela.vymera}"/> ha - 
                    <b><c:out value="${parcela.katastralneUzemie.nazov}"/></b>
                    <br>
                </c:forEach>
            </div>
        </div>
        <div class="form-group">
            <s:label class="col-md-3 control-label" for="nzSucerVymier" name="najomnaZmluva.detail.sucetVymier" />
            <div class="col-md-4">
                <span id="nzSucetVymier"><c:out value="${actionBean.nzDetail.sucetVymier}"/></span>
            </div>
        </div>
        <div class="form-group">
            <s:label class="col-md-3 control-label" for="datepicker1" name="najomnaZmluva.label.platnostOd" />
            <div class="col-md-4">
                <s:text class="form-control" id="datepicker1" name="nz.platnostOd" />
            </div>
        </div>
        <div class="form-group">
            <s:label class="col-md-3 control-label" for="datepicker2" name="najomnaZmluva.label.platnostDo" />
            <div class="col-md-4">
                <s:text class="form-control" id="datepicker2" name="nz.platnostDo" />
            </div>
        </div>
        <div class="form-group">
            <s:label class="col-md-3 control-label" for="nz3" name="najomnaZmluva.label.sadzba" />
            <div class="col-md-4">
                <s:text class="form-control" id="nz3" name="nz.sadzba" />
            </div>
        </div>
        <s:hidden name="najomnaZmluva.id" value="${actionBean.nzDetail.id}" />
        <div class="form-group">
            <div class="col-md-4 col-md-offset-3">
                <s:submit class="btn btn-primary" name="addNajomnaZmluva">
                    <f:message key="najomnaZmluva.add.save" />
                </s:submit>
                <s:submit class="btn btn-default" name="cancelNajomnaZmluva">
                    <f:message key="najomnaZmluva.add.cancel" />
                </s:submit>
                <button class="btn btn-default" type="reset"><f:message key="control.reset"/></button>
            </div>
        </div>
    </s:form>  
    
    <s:link event="displayDetail" beanclass="fbm.vutbr.martinsakac.bakalarka.web.NajomnaZmluvaActionBean">
        <s:param name="najomnaZmluva.id" value="${actionBean.nzDetail.id}" />
        <f:message key="najomnaZmluva.add.backToDetail" />
    </s:link> 
        
    </s:layout-component>
</s:layout-render>