<%-- 
    Document   : listNajomnaZmluva
    Created on : Feb 18, 2014, 2:46:58 PM
    Author     : martin
--%>

<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>

<s:layout-render name="/layoutIntraPortal.jsp" titlekey="fieldagenda.title.najomnaZmluva">
    <s:layout-component name="bodyIntra">
    
        <s:useActionBean beanclass="fbm.vutbr.martinsakac.bakalarka.web.NajomnaZmluvaActionBean" var="actionBean" />
        <h3><f:message key="najomnaZmluva.list.allNajomnaZmluva"/></h3>
        <p class="najomnaZmluvaWarning">* <f:message key="najomnaZmluva.list.warning"/></p>
        <p class="najomnaZmluvaDanger">* <f:message key="najomnaZmluva.list.danger"/></p>
        <table class="table-hover table">
            <thead>
                <tr>
                    <td></td>
                    <td>
                        <s:link class="btn btn-default" beanclass="fbm.vutbr.martinsakac.bakalarka.web.NajomnaZmluvaActionBean" event="list" title="Ascending">
                            <s:param name="najomnaZmluva.order.column" value="platnostDo" />
                            <s:param name="najomnaZmluva.order.direction" value="asc" />
                            <span class="glyphicon glyphicon-arrow-up"></span>
                        </s:link>
                        <s:link class="btn btn-default" beanclass="fbm.vutbr.martinsakac.bakalarka.web.NajomnaZmluvaActionBean" event="list" title="Descending">
                            <s:param name="najomnaZmluva.order.column" value="platnostDo" />
                            <s:param name="najomnaZmluva.order.direction" value="desc" />
                            <span class="glyphicon glyphicon-arrow-down"></span>
                        </s:link>
                    </td>
                    <td>
                        <s:link class="btn btn-default" beanclass="fbm.vutbr.martinsakac.bakalarka.web.NajomnaZmluvaActionBean" event="list" title="Ascending">
                            <s:param name="najomnaZmluva.order.column" value="platnostOd" />
                            <s:param name="najomnaZmluva.order.direction" value="asc" />
                            <span class="glyphicon glyphicon-arrow-up"></span>
                        </s:link>
                        <s:link class="btn btn-default" beanclass="fbm.vutbr.martinsakac.bakalarka.web.NajomnaZmluvaActionBean" event="list" title="Descending">
                            <s:param name="najomnaZmluva.order.column" value="platnostOd" />
                            <s:param name="najomnaZmluva.order.direction" value="desc" />
                            <span class="glyphicon glyphicon-arrow-down"></span>
                        </s:link>
                    </td>
                    <td>
                        <s:link class="btn btn-default" beanclass="fbm.vutbr.martinsakac.bakalarka.web.NajomnaZmluvaActionBean" event="list" title="Ascending">
                            <s:param name="najomnaZmluva.order.column" value="zhotovenie" />
                            <s:param name="najomnaZmluva.order.direction" value="asc" />
                            <span class="glyphicon glyphicon-arrow-up"></span>
                        </s:link>
                        <s:link class="btn btn-default" beanclass="fbm.vutbr.martinsakac.bakalarka.web.NajomnaZmluvaActionBean" event="list" title="Descending">
                            <s:param name="najomnaZmluva.order.column" value="zhotovenie" />
                            <s:param name="najomnaZmluva.order.direction" value="desc" />
                            <span class="glyphicon glyphicon-arrow-down"></span>
                        </s:link>
                    </td>
                </tr>
                <tr>
                    <th>#</th>
                    <th><f:message key="najomnaZmluva.label.platnostDo" /></th>
                    <th><f:message key="najomnaZmluva.label.platnostOd" /></th>
                    <th><f:message key="najomnaZmluva.label.zhotovenie" /></th>
                    <th><f:message key="najomnaZmluva.label.sadzba" /></th>
                    <th><f:message key="najomnaZmluva.label.uzivatel" /></th>
                </tr>
            </thead>
            <tbody>
                <c:set var="counter" value="0" scope="page" />
                <c:forEach items="${actionBean.nzList}" var="najomnaZmluva">
                    <c:set var="counter" value="${counter + 1}" scope="page"/>
                    <tr class="<c:if test="${najomnaZmluva.current == 0 && najomnaZmluva.hasValidFollower == 0}">najomnaZmluvaDanger</c:if><c:if test="${najomnaZmluva.current == 0 && najomnaZmluva.hasValidFollower == 1}">najomnaZmluvaWarning</c:if>">
                        <td>${counter}</td>
                        <td><c:out value="${najomnaZmluva.platnostDo}" /></td>
                        <td><c:out value="${najomnaZmluva.platnostOd}" /></td>
                        <td><c:out value="${najomnaZmluva.datumZhotovenia}" /></td>
                        <td><c:out value="${najomnaZmluva.sadzba}" /></td>
                        <td><c:if test="${najomnaZmluva.uzivatel == null}">N/A</c:if><c:out value="${najomnaZmluva.uzivatel.meno} ${najomnaZmluva.uzivatel.priezvisko}" /></td>
                        <td>
                            <s:link class="btn btn-info" event="displayDetail"
                                beanclass="fbm.vutbr.martinsakac.bakalarka.web.NajomnaZmluvaActionBean">
                                <s:param name="najomnaZmluva.id" value="${najomnaZmluva.id}" />
                                <f:message key="najomnaZmluva.label.details" />
                            </s:link>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
        
    </s:layout-component>
</s:layout-render>
