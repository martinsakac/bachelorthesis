<%-- 
    Document   : uzivatelList
    Created on : Feb 4, 2014, 2:30:26 PM
    Author     : martin
--%>

<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>

<s:layout-render name="/layoutIntraPortal.jsp" titlekey="fieldagenda.title.uzivatel">
    <s:layout-component name="bodyIntra">
        
        <s:useActionBean beanclass="fbm.vutbr.martinsakac.bakalarka.web.UzivatelActionBean" var="actionBean"/>        
        <s:errors/>
        <s:form class="form-horizontal" beanclass="fbm.vutbr.martinsakac.bakalarka.web.UzivatelActionBean">
            <s:hidden name="uzivatel.id"/>
            <fieldset><legend><f:message key="uzivatel.new.form"/></legend>
                <%@include file="newUzivatelForm.jsp" %>
                <div class="form-group">
                    <div class="col-sm-6 col-sm-offset-3">
                        <s:submit class="btn btn-primary" name="add"><f:message key="uzivatel.form.add"/></s:submit>
                        <s:submit class="btn btn-default" name="cancel"><f:message key="uzivatel.form.cancel"/></s:submit>
                        <button class="btn btn-default" type="reset"><f:message key="control.reset"/></button>
                    </div>
                </div>
            </fieldset>
        </s:form>
        <br/><br/>
        <fieldset><legend><f:message key="uzivatel.list.alluzivatel"/></legend>
        <table class="table table-striped table-hover">
            <tr>
                <th>#</th>
                <th><f:message key="uzivatel.label.meno"/></th>
                <th><f:message key="uzivatel.label.priezvisko"/></th>
                <th><f:message key="uzivatel.label.pracovneZaradenie"/></th>
                <th><f:message key="uzivatel.label.uzivatelskeMeno"/></th>
                <th><f:message key="uzivatel.label.rola"/></th>
            </tr>
            <c:set var="counter" value="0" scope="page" />
            <c:forEach items="${actionBean.listUzivatel}" var="uzivatel">
                <c:set var="counter" value="${counter + 1}" scope="page"/>
                <tr>
                    <td>${counter}</td>
                    <td><c:out value="${uzivatel.meno}"/></td>
                    <td><c:out value="${uzivatel.priezvisko}"/></td>
                    <td><c:out value="${uzivatel.pracovneZaradenie}"/></td>
                    <td><c:out value="${uzivatel.uzivatelskeMeno}"/></td>
                    <td>
                        <c:choose>
                            <c:when test="${uzivatel.uzivatelskaRola == 'ADMIN'}"><f:message key="uzivatel.rola.admin"/></c:when>
                            <c:when test="${uzivatel.uzivatelskaRola == 'USER'}"><f:message key="uzivatel.rola.user"/></c:when>
                        </c:choose>
                    </td>
                    <td>
                        <s:link class="btn btn-default" beanclass="fbm.vutbr.martinsakac.bakalarka.web.UzivatelActionBean" event="edit">
                            <s:param name="uzivatel.id" value="${uzivatel.id}"/>
                            <f:message key="control.edit"/>
                        </s:link>
                    </td>
                    <td>
                        <s:form beanclass="fbm.vutbr.martinsakac.bakalarka.web.UzivatelActionBean">
                            <s:hidden name="uzivatel.id" value="${uzivatel.id}"/>
                            <s:submit class="btn btn-danger" name="delete" onclick="return confirm('Are you sure you want delete this record?')">
                                <f:message key="control.delete"/>
                            </s:submit>
                        </s:form>
                    </td>
                </tr>
            </c:forEach>
        </table>
        </fieldset>    
            
    </s:layout-component>
</s:layout-render>