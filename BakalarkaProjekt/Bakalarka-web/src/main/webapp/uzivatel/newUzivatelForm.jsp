<%-- 
    Document   : newUzivatelForm
    Created on : Feb 4, 2014, 4:58:39 PM
    Author     : martin
--%>

<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>

        
<div class="form-group">
    <s:label class="col-md-3 control-label" for="t1" name="uzivatel.label.meno"/>
    <div class="col-md-4">
        <s:text class="form-control" id="t1" name="uzivatel.meno"/>
    </div>
</div>
<div class="form-group">
    <s:label class="col-md-3 control-label" for="t2" name="uzivatel.label.priezvisko"/>
    <div class="col-md-4">
        <s:text class="form-control" id="t2" name="uzivatel.priezvisko"/>
    </div>
</div>
<div class="form-group">
    <s:label class="col-md-3 control-label" for="t3" name="uzivatel.label.pracovneZaradenie"/>
    <div class="col-md-4">
        <s:text class="form-control" id="t3" name="uzivatel.pracovneZaradenie"/>
    </div>
</div>
<div class="form-group">
    <s:label class="col-md-3 control-label" for="t4" name="uzivatel.label.uzivatelskeMeno"/>
    <div class="col-md-4">
        <s:text class="form-control" id="t4" name="uzivatel.uzivatelskeMeno"/>
    </div>
</div>
<div class="form-group">
    <s:label class="col-md-3 control-label" for="t5" name="uzivatel.label.uzivatelskeHeslo"/>
    <div class="col-md-4">
        <s:password class="form-control" id="t5" name="uzivatel.uzivatelskeHeslo"/>
    </div>
</div>
<div class="form-group">
    <s:label class="col-md-3 control-label" for="t6" name="uzivatel.label.rola"/>
    <div class="col-md-4">
        <s:select class="btn btn-default" id="t6" name="uzivatel.uzivatelskaRola" >
            <s:option value=""><f:message key="control.select"/></s:option>
            <s:options-enumeration enum="fbm.vutbr.martinsakac.bakalarka.dto.UzivatelDto.Rola"/>
        </s:select>
    </div>
</div>
