<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>

<s:layout-render name="/layout.jsp" titlekey="titlekey.index.title">
    <s:layout-component name="body">
<!------------------------------- Carousel slider ----------------------------->
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
            <li data-target="#carousel-example-generic" data-slide-to="3"></li>
            <li data-target="#carousel-example-generic" data-slide-to="4"></li>
        </ol>
        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="item active">
                <img src="/Bakalarka-web/images/carousel/zatva2-1920.jpg" alt="..." class="img-responsive">
                <div class="carousel-caption">
                    <h1><f:message key="carousel.crops"/></h1>
                    <p><f:message key="carousel.crops.description.p1"/></p>
                    <p><f:message key="carousel.crops.description.p2"/></p>
                </div>
            </div>
            <div class="item">
                <img src="/Bakalarka-web/images/carousel/farma10-1920.jpg" alt="..." class="img-responsive">
                <div class="carousel-caption">
                    <h1><f:message key="carousel.livestock"/></h1>
                    <p><f:message key="carousel.livestock.description"/></p>
                </div>
            </div>
            <div class="item">
                <img src="/Bakalarka-web/images/carousel/zatva3-1920.jpg" alt="..." class="img-responsive">
                <div class="carousel-caption">
                    <h1><f:message key="carousel.crops"/></h1>
                    <p><f:message key="carousel.crops.description.p1"/></p>
                    <p><f:message key="carousel.crops.description.p2"/></p>
                </div>
            </div>
            <div class="item">
                <img src="/Bakalarka-web/images/carousel/zatva9-1920.jpg" alt="..." class="img-responsive">
                <div class="carousel-caption">
                    <h1><f:message key="carousel.crops"/></h1>
                    <p><f:message key="carousel.crops.description.p1"/></p>
                    <p><f:message key="carousel.crops.description.p2"/></p>
                </div>
            </div>
            <div class="item">
                <img src="/Bakalarka-web/images/carousel/zatva17-1920.jpg" alt="..." class="img-responsive">
                <div class="carousel-caption">
                    <h1><f:message key="carousel.machines"/></h1>
                    <p><f:message key="carousel.machines.description"/></p>
                </div>
            </div>
        </div>
        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
    </div>
                
<!------------------------- Sekcia popisu zamestnancov ------------------------>                
    <section id="staffSection" class="container">
        <div class="row">
            <div class="col-md-12">
                <h1><f:message key="navigation.top.staff"/></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <h2><f:message key="company.owner"/></h2>
                <h4>JUDr. Erik Solár</h4>
                <p>solar@pdvrable.sk</p>
            </div>
            <div class="col-md-3">
                <h2><f:message key="company.director"/></h2>
                <h4>Ing. Juraj Sakáč</h4>
                <p>sakac@pdvrable.sk</p>
                <p>0904/103 433</p>
                <p>037/ 783 21 24</p>
            </div>
            <div class="col-md-3">
                <h2><f:message key="company.economist"/></h2>
                <h4>Ing. Mária Galabová</h4>
                <p>galabova@pdvrable.sk</p>
                <p>0904/103 431</p>
                <p>037/ 783 31 02</p>
            </div>
            <div class="col-md-3">
                <h2><f:message key="company.agronomist"/></h2>
                <h4>Ing. Zuzana Penzešová</h4>
                <p>agronom@pdvrable.sk</p>
                <p>0904/835 837</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <h2><f:message key="company.agrolandresources"/></h2>
                <h4>Ing. Eva Tóthová</h4>
                <p>tothova@pdvrable.sk</p>
                <p>0904/186 355 </p>
                <p>037/ 783 21 24</p>
            </div>
            <div class="col-md-3">
                <h2><f:message key="company.mechanist"/></h2>
                <h4>Ing. Radovan Švarda, PhD.</h4>
                <p>mechanizator@pdvrable.sk</p>
                <p>0904/104 956</p>
            </div>
            <div class="col-md-3">
                <h2><f:message key="company.purchaser"/></h2>
                <h4>Jozef Urban</h4>
                <p>0911/ 835 838</p>
                <p>037/783 34 15</p>
            </div>
            <div class="col-md-3">
                <h2><f:message key="company.zootechnician"/></h2>
                <h4>Ing. Peter Gaži</h4>
                <p>hdohaj@pdvrable.sk</p>
                <p>0902/ 186 789</p>
                <p>037/ 783 23 50</p>
            </div>
         </div>
    </section>
                
<!----------------------- Sekcia popisu adresy -------------------------------->                
    <section id="addressSection" class ="container">
        <div class="row">
            <div class="col-md-12">
                <h1><f:message key="navigation.top.contact.address"/></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5">
                <iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=sk&amp;geocode=&amp;q=Levick%C3%A1+886,+Vr%C3%A1ble,+Slovensk%C3%A1+republika&amp;aq=1&amp;oq=Levick%C3%A1+886++&amp;sll=48.243731,18.308202&amp;sspn=0.098658,0.255775&amp;g=Levick%C3%A1+886++95280+Vr%C3%A1ble&amp;ie=UTF8&amp;hq=&amp;hnear=Levick%C3%A1+886%2F72,+952+01+Vr%C3%A1ble,+Slovensk%C3%A1+republika&amp;t=m&amp;ll=48.241939,18.320303&amp;spn=0.020007,0.036478&amp;z=14&amp;iwloc=A&amp;output=embed"></iframe>
                <br/>
                <button type="button" class="btn btn-default btn-lg" title="Display in Google Maps">
                    <a id="mapDetail" href="https://maps.google.com/maps?f=q&amp;source=embed&amp;hl=sk&amp;geocode=&amp;q=Levick%C3%A1+886,+Vr%C3%A1ble,+Slovensk%C3%A1+republika&amp;aq=1&amp;oq=Levick%C3%A1+886++&amp;sll=48.243731,18.308202&amp;sspn=0.098658,0.255775&amp;g=Levick%C3%A1+886++95280+Vr%C3%A1ble&amp;ie=UTF8&amp;hq=&amp;hnear=Levick%C3%A1+886%2F72,+952+01+Vr%C3%A1ble,+Slovensk%C3%A1+republika&amp;t=m&amp;ll=48.241939,18.320303&amp;spn=0.020007,0.036478&amp;z=14&amp;iwloc=A" ><span class="glyphicon glyphicon-zoom-in"></span></a>
                </button>
            </div>
            <div class="col-md-5 col-md-offset-2">
                <h2>
                    <f:message key="company.address.one"/><br>
                    <f:message key="company.address.two"/><br>
                    <f:message key="company.address.three"/>
                </h2>
            </div>
        </div>
    </section>
 
<!---------------------- Sekcia kontaktnych informacii ------------------------>                
    <section id="contactDetailsSection" class="container">
        <div class="row">
            <div class="col-md-12">
                <h1><f:message key="navigation.top.contact.details"/></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <h2><f:message key="navigation.top.contact.phone"/></h2>
                <h4>037/ 783 21 24</h4>
                <h2>Fax</h2>
                <h4>037/ 783 28 40</h4>
            </div>
            <div class="col-md-4">
                <h2><f:message key="navigation.top.contact.mail"/></h2>
                <h4>pdvrable@stonline.sk</h4>
            </div>
            <div class="col-md-4">
                <h2><f:message key="navigation.top.contact.ico"/></h2>
                <h4>IČO: 00198901</h4>
                <h4>IČ DPH: SK2020411173</h4>
            </div>
        </div>
    </section>

<!------------------------ Sekcia produkcie mlieka ---------------------------->                
    <section id ="milkProductionSection" class="container">
        <div class="row">
            <div class="col-md-12">
                <h1><f:message key="navigation.top.milkProduction"/></h1>
            </div>
        </div>
        <div class="col-md-4">
            <h4><f:message key="company.milkProduction.description"/></h4>
        </div>
        <div class="col-md-6 col-md-offset-2">
            <iframe width="420" height="315" src="//www.youtube.com/embed/_XMOyVBnVqs" frameborder="0" allowfullscreen></iframe>
        </div>
    </section>

    </s:layout-component>
</s:layout-render>