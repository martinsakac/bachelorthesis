/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fbm.vutbr.martinsakac.bakalarka.web;

import fbm.vutbr.martinsakac.bakalarka.dto.UzivatelDto;
import fbm.vutbr.martinsakac.bakalarka.service.UzivatelService;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.DontValidate;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.integration.spring.SpringBean;
import net.sourceforge.stripes.validation.Validate;
import net.sourceforge.stripes.validation.ValidateNestedProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author martin sakac
 */

@UrlBinding("/user/{event}")
public class ProfilActionBean extends BaseActionBean {
    
    final static Logger log = LoggerFactory.getLogger(UzivatelActionBean.class);
    
    @SpringBean
    private UzivatelService uzivatelService;
    
    @ValidateNestedProperties(value = {
        @Validate(on = {"updateProfile"}, field = "meno", required = true),
        @Validate(on = {"updateProfile"}, field = "priezvisko", required = true),
        @Validate(on = {"updateProfile"}, field = "uzivatelskeMeno", required = true),
        @Validate(on = {"updateProfile"}, field = "uzivatelskeHeslo", required = true)})
    private UzivatelDto uzivatel;
    
    public UzivatelDto getUzivatel() {
        return uzivatel;
    }

    public void setUzivatel(UzivatelDto uzivatel) {
        this.uzivatel = uzivatel;
    }
    
    @DefaultHandler
    @DontValidate
    public Resolution displayProfile() {
        log.info("Displaying user details of logged in user.");
        super.getContext().getRequest().getSession().setAttribute("chosenTab", "profil");
        uzivatel = (UzivatelDto)getContext().getRequest().getSession().getAttribute("user");
        return new ForwardResolution("/userProfile.jsp");
    }
    
    public Resolution updateProfile() {
        UzivatelDto loggedUzivatel = (UzivatelDto)getContext().getRequest().getSession().getAttribute("user");
        log.info("Editing logged in user {}", loggedUzivatel);
        loggedUzivatel.setMeno(uzivatel.getMeno());
        loggedUzivatel.setPriezvisko(uzivatel.getPriezvisko());
        loggedUzivatel.setUzivatelskeMeno(uzivatel.getUzivatelskeMeno());
        loggedUzivatel.setUzivatelskeHeslo(uzivatel.getUzivatelskeHeslo());
        
        uzivatelService.updateUzivatel(loggedUzivatel);
        getContext().getRequest().getSession().setAttribute("user", loggedUzivatel);
        return new RedirectResolution(this.getClass(), "displayProfile");
    }
    
    public Resolution cancel(){
        log.info("Canceling  editing logged in user!");
        return new RedirectResolution(this.getClass(), "displayProfile");
    }
}
