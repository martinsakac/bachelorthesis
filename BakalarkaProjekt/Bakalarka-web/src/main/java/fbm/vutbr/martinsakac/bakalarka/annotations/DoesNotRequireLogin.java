/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.annotations;

import java.lang.annotation.*;
/**
 *
 * @author martin
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface DoesNotRequireLogin {
    
}
