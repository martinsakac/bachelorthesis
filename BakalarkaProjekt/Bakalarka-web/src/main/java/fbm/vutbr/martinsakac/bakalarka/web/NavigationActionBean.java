/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.web;

import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.DontValidate;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;

/**
 *
 * @author martin
 */

public class NavigationActionBean extends BaseActionBean{
    
    @DefaultHandler
    @DontValidate
    public Resolution goToFieldAgenda(){
        return new ForwardResolution("/fieldAgenda.jsp");
    }
    
    @DontValidate
    public Resolution goToUserProfile(){
        return new ForwardResolution("/userProfile.jsp");
    }
    
    @DontValidate
    public Resolution goToIntraportal(){
        return new ForwardResolution("/userProfile.jsp");
    }
    
    @DontValidate
    public Resolution goToIndex(){
        return new ForwardResolution("/index.jsp");
    }
}
