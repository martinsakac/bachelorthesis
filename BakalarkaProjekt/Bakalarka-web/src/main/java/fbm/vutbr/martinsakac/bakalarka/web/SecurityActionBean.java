/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.web;

import fbm.vutbr.martinsakac.bakalarka.annotations.DoesNotRequireLogin;
import fbm.vutbr.martinsakac.bakalarka.dto.UzivatelDto;
import fbm.vutbr.martinsakac.bakalarka.service.UzivatelService;
import static fbm.vutbr.martinsakac.bakalarka.web.BaseActionBean.escapeHTML;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.integration.spring.SpringBean;
import net.sourceforge.stripes.validation.Validate;
import javax.servlet.http.HttpSession;
import net.sourceforge.stripes.action.LocalizableMessage;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.validation.LocalizableError;
import net.sourceforge.stripes.validation.SimpleError;

/**
 *
 * @author martin
 */

@DoesNotRequireLogin
@UrlBinding("/security/{$event}")
public class SecurityActionBean extends BaseActionBean{
    
    @SpringBean
    private UzivatelService uzivatelService;
    
    @Validate
    private String userId;
    
    @Validate
    private String password;

    public String getUserId() {
        return userId;
    }

    public String getPassword() {
        return password;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    
    @DefaultHandler
    public Resolution login(){
        return new ForwardResolution("/index.jsp");
    }
    
    public Resolution submitLogin(){
        HttpSession session = super.getContext().getRequest().getSession();

        UzivatelDto uzivatel = null;
        try {
            uzivatel = uzivatelService.login(userId, password);
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        
        if (uzivatel != null){
            session.setAttribute("loggedIn", true);
            session.setAttribute("user", uzivatel);
        }
        else {
            this.getContext().getValidationErrors().add("Login Failed!", new LocalizableError("app.login.failed"));
            return new ForwardResolution("/index.jsp");
        }
        
        getContext().getMessages().add(new LocalizableMessage
                    ("app.login.success",escapeHTML(uzivatel.getMeno()),escapeHTML(uzivatel.getPriezvisko())));
        super.getContext().getRequest().getSession().setAttribute("chosenTab", "profil");
        
        return new RedirectResolution(ProfilActionBean.class, "displayProfile");

    }
    
    public Resolution logout(){
        HttpSession session = super.getContext().getRequest().getSession();
        session.setAttribute("loggedIn", false);
        getContext().getMessages().add(new LocalizableMessage("app.logout.success"));
        return new RedirectResolution("/index.jsp");
    }
    
}
