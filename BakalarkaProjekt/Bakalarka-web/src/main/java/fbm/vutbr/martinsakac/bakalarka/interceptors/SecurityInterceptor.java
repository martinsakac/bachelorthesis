/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.interceptors;

import fbm.vutbr.martinsakac.bakalarka.annotations.DoesNotRequireLogin;
import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.controller.ExecutionContext;
import net.sourceforge.stripes.controller.Interceptor;
import net.sourceforge.stripes.controller.Intercepts;
import net.sourceforge.stripes.controller.LifecycleStage;

/**
 *
 * @author martin
 */

@Intercepts(LifecycleStage.HandlerResolution)
public class SecurityInterceptor implements Interceptor{

    @Override
    public Resolution intercept(ExecutionContext ec) throws Exception {
        Resolution resolution = ec.proceed();
        Class currentBean = ec.getActionBean().getClass();
        if (ec.getActionBean().getClass().isAnnotationPresent(DoesNotRequireLogin.class)){
            return resolution;
        }
        if (isLoggedIn(ec.getActionBeanContext())){
            return resolution;
        }
        else {
            return new RedirectResolution("/index.jsp");
        }
    }
    
    protected boolean isLoggedIn(ActionBeanContext ctx){
        Boolean loggedIn = (Boolean)
                ctx.getRequest().getSession().getAttribute("loggedIn");
        if (loggedIn != null && loggedIn == true){
            return true;
        }
        else {
            return false;
        }
    }
}
