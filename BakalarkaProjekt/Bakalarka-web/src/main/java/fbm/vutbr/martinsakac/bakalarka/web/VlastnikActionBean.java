/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.web;

import fbm.vutbr.martinsakac.bakalarka.dto.upravene.NajomnaZmluvaUpravenaDto;
import fbm.vutbr.martinsakac.bakalarka.dto.VlastnikDto;
import fbm.vutbr.martinsakac.bakalarka.dto.upravene.VlastnikSoZmluvamiDto;
import fbm.vutbr.martinsakac.bakalarka.service.NajomnaZmluvaService;
import fbm.vutbr.martinsakac.bakalarka.service.VlastnikService;
import static fbm.vutbr.martinsakac.bakalarka.web.BaseActionBean.escapeHTML;
import java.util.List;
import net.sourceforge.stripes.action.Before;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.DontValidate;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.LocalizableMessage;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.controller.LifecycleStage;
import net.sourceforge.stripes.integration.spring.SpringBean;
import net.sourceforge.stripes.validation.LocalizableError;
import net.sourceforge.stripes.validation.Validate;
import net.sourceforge.stripes.validation.ValidateNestedProperties;
import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.ArrayList;

/**
 *
 * @author martin
 */

@UrlBinding("/vlastnici/{$event}/{vlastnik.id}")
public class VlastnikActionBean extends BaseActionBean implements ValidationErrorHandler{
    
    final static Logger log = LoggerFactory.getLogger(VlastnikActionBean.class);
    
    @SpringBean
    protected VlastnikService vlastnikService;
    
    @SpringBean
    protected NajomnaZmluvaService nzService;
    
    private List<VlastnikDto> vlastnici;
    private List<VlastnikSoZmluvamiDto> vlastniciSoZmluvami;
    //private List<NajomnaZmluvaUpravenaDto> zmluvaUpravena;

    public List<VlastnikSoZmluvamiDto> getVlastniciSoZmluvami() {
        return vlastniciSoZmluvami;
    }

//    public List<NajomnaZmluvaUpravenaDto> getZmluvaUpravena() {
//        return zmluvaUpravena;
//    }
    
    public List<VlastnikDto> getVlastnici(){
        return vlastnici;
    }
    
    @ValidateNestedProperties(value = {
            @Validate(on = {"add", "save"}, field = "meno", required = true),
            @Validate(on = {"add", "save"}, field = "priezvisko", required = true)
    })
    private VlastnikDto vlastnik;
    
    public void setVlastnik(VlastnikDto vlastnik){
        this.vlastnik = vlastnik;
    }
    
    public VlastnikDto getVlastnik(){
        return this.vlastnik;
    }
    
    
    public Resolution add(){
        log.info("add() vlastnik={}", vlastnik);
        
        String datum = getContext().getRequest().getParameter("vlastnik.datumNarodenia");
        String[] datumPole = datum.split("-");
        vlastnik.setDatumNarodenia(new LocalDate(Integer.parseInt(datumPole[0]), Integer.parseInt(datumPole[1]), Integer.parseInt(datumPole[2])));
        
        if (vlastnik.getId() == null){
            vlastnikService.addVlastnik(vlastnik);
            getContext().getMessages().add(new LocalizableMessage
                    ("vlastnik.add.message",escapeHTML(vlastnik.getMeno()),escapeHTML(vlastnik.getPriezvisko())));
        }
        else {
            vlastnikService.updateVlastnik(vlastnik);
            getContext().getMessages().add(new LocalizableMessage
                    ("vlastnik.update.message",escapeHTML(vlastnik.getMeno()),escapeHTML(vlastnik.getPriezvisko())));
        }
        return new RedirectResolution(this.getClass(), "list");
    }
    
    
    @DefaultHandler
    @DontValidate
    public Resolution list(){
        log.info("list()");
        
        String chosenTab = getContext().getRequest().getParameter("chosenTab");
        if (chosenTab != null && chosenTab.equals("vlastnik")) {
            super.getContext().getRequest().getSession().setAttribute("chosenTab", "vlastnik");
        }
        
        String column = getContext().getRequest().getParameter("owner.order.column");
        String direction = getContext().getRequest().getParameter("owner.order.direction");
        if (column == null || direction == null) {
            column = "priezvisko";
            direction = "asc";
        }
        //vlastnici = vlastnikService.getAllVlastnik();
        vlastniciSoZmluvami = vlastnikService.getAllVlastnikSoZmluvami(column, direction);
        return new ForwardResolution("/vlastnik/listVlastnik.jsp");
    }
    
    // potrebujem este???
    public Resolution listSoZmluvami(){
        log.info("listSoZmluvami()");
        vlastniciSoZmluvami = vlastnikService.getAllVlastnikSoZmluvami("priezvisko", "asc");
        return new ForwardResolution("/vlastnik/list.jsp");
    }
    
    public Resolution delete(){
        log.info("delete({})", vlastnik.getId());
        vlastnik = vlastnikService.getVlastnikById(vlastnik.getId());
        vlastnikService.deleteVlastnik(vlastnik);
            getContext().getMessages().add(new LocalizableMessage("vlastnik.delete.message",
                    escapeHTML(vlastnik.getMeno()),
                    escapeHTML(vlastnik.getPriezvisko())));
        return new RedirectResolution(this.getClass(),"list");
    }

    @Override
    public Resolution handleValidationErrors(ValidationErrors ve) throws Exception {
        vlastniciSoZmluvami = vlastnikService.getAllVlastnikSoZmluvami("priezvisko", "asc");
        return null;
    }
    
    @Before(stages = LifecycleStage.BindingAndValidation, on = {"edit", "save"})
    public void loadVlastnikFromDatabase() {
        String ids = getContext().getRequest().getParameter("vlastnik.id");
        if (ids == null) return;
        vlastnik = vlastnikService.getVlastnikById(Long.parseLong(ids));
    }
    
    public Resolution edit() {
        log.info("edit() vlastnik={}", vlastnik);
        vlastniciSoZmluvami = vlastnikService.getAllVlastnikSoZmluvami("priezvisko", "asc");
        return new ForwardResolution("/vlastnik/listVlastnik.jsp");
    }

    public Resolution cancel(){
        log.info("Cancelling saving or editing.");
        return new RedirectResolution(this.getClass(), "list");
    }
}
