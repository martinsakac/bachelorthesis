/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fbm.vutbr.martinsakac.bakalarka.output;

//import java.io.BufferedWriter;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamSource;
import org.apache.fop.apps.FOPException;
import org.apache.fop.apps.FOUserAgent;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;

//import java.io.File;
//import java.io.FileWriter;
//import java.io.IOException;
//import java.io.OutputStream;
//import javax.xml.transform.Result;
//import javax.xml.transform.Source;
//import javax.xml.transform.Transformer;
//import javax.xml.transform.TransformerFactory;
//import javax.xml.transform.sax.SAXResult;
//import javax.xml.transform.stream.StreamSource;
//import org.apache.avalon.framework.logger.ConsoleLogger;
//import org.apache.avalon.framework.logger.Logger;
//import org.apache.fop.apps.Driver;
//import org.apache.fop.messaging.MessageHandler;



/**
 * 
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
public class ExportNajomnaZmluva {
    
    public ByteArrayOutputStream toPdf(String xmlData) {
    
        // creation of transform source
        StreamSource transformSource = new StreamSource
            (new File("C:\\Users\\Martin\\Dropbox\\BAKALARKA\\zdrojaky\\BakalarkaProjekt\\Bakalarka-web\\src\\main\\java\\fbm\\vutbr\\martinsakac\\bakalarka\\output\\najomnaZmluva.xsl"));
 
        // create an instance of fop factory
        FopFactory fopFactory = FopFactory.newInstance();
        fopFactory.setStrictValidation(false);

        // a user agent is needed for transformation
        FOUserAgent foUserAgent = fopFactory.newFOUserAgent();

        ByteArrayOutputStream pdfoutStream = new ByteArrayOutputStream();
        
        File xml = null;
        try {
//            xml = File.createTempFile("najomnaZmluva", ".xml");
//            xml.deleteOnExit();
            System.out.println(xmlData);
            xml = new File("C:\\DATA\\najomnaZmluva.xml");
            if (!xml.exists()) 
                xml.createNewFile();
            BufferedWriter out = new BufferedWriter(new FileWriter(xml));
            out.write(xmlData);
            out.close();
        }
        catch (IOException e) {
            //throw new Exception("Nepodarilo sa vytvorit xml subor.", e);
            e.printStackTrace();
        }
        
        Reader reader = null;
        try {
            InputStream inputStream= new FileInputStream(xml);
            reader = new InputStreamReader(inputStream,"UTF-8");
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        
        StreamSource source = new StreamSource(reader);

        try {
            TransformerFactory transfact = TransformerFactory.newInstance();
            Transformer xslfoTransformer = transfact.newTransformer(transformSource);

            // construct fop with desired output format
            Fop fop = fopFactory.newFop("application/pdf", foUserAgent, pdfoutStream);

            // generated FO will need to be fed to FOP
            Result res = new SAXResult(fop.getDefaultHandler());

            // everything happens here..
            xslfoTransformer.transform(source, res);

            // if you want to save the PDF file use following code
//            String pdfFile = Math.round(Math.random() * 100000) + ".pdf";
//            FileOutputStream str = new FileOutputStream(pdfFile);
//            str.write(pdfoutStream.toByteArray());
//            str.close();

        } catch(TransformerException e) {
            e.printStackTrace();

        } catch(FOPException e) {
            e.printStackTrace();
        }
//        } catch(FileNotFoundException e) {
//            e.printStackTrace();
//
//        } catch(IOException e) {
//            e.printStackTrace();
//        }
        return pdfoutStream;
    }
    
} // end of the class

//    public void toPdf(String xmlContent, String xsltFileName, String najomnaZmluvaId) throws Exception{
//        File tempXml = null;
//        File xslt = null;
//        File pdf = null;
//        
//        try {
//            tempXml = File.createTempFile("nz" + najomnaZmluvaId, ".xml");
//            BufferedWriter out = new BufferedWriter(new FileWriter(tempXml));
//            out.write(xmlContent);
//            out.close();
//            xslt = new File("najomnaZmluva.xsl");
//            pdf = new File("nz1.pdf\\");
//            if (!pdf.exists()) {
//                pdf.createNewFile();
//            }
//        }
//        catch (IOException e) {
//            throw new Exception("Nepodarilo sa otvorit subory.", e);
//        }
//        
//        //Construct driver 
//        Driver driver = new Driver(); 
//
//        //Setup logger 
//        Logger logger = new ConsoleLogger(ConsoleLogger.LEVEL_INFO); 
//        driver.setLogger(logger); 
//        MessageHandler.setScreenLogger(logger); 
//
//        //Setup Renderer (output format) 
//        driver.setRenderer(Driver.RENDER_PDF); 
//
//        OutputStream out = null;
//        try { 
//            //Setup output 
//            out = new java.io.FileOutputStream(pdf); 
//            
//            driver.setOutputStream(out); 
//
//            //Setup XSLT 
//            TransformerFactory factory = TransformerFactory.newInstance(); 
//            Transformer transformer = factory.newTransformer(new StreamSource(xslt)); 
//
//            //Setup input for XSLT transformation 
//            Source src = new StreamSource(tempXml); 
//
//            //Resulting SAX events (the generated FO) must be piped through to FOP 
//            Result res = new SAXResult(driver.getContentHandler()); 
//
//            //Start XSLT transformation and FOP processing 
//            transformer.transform(src, res); 
//        } 
//        catch (Exception ex) {
//            throw new Exception("Nepodarila sa transformacia.", ex);
//        }
//        finally { 
//            if (out != null) {
//                try{
//                    out.close();
//                }
//                catch (IOException e){
//                    throw new Exception("Nepodarilo sa zavriet output stream.", e);
//                }
//            }
//        }
//    }
    

