/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.web;

import com.thoughtworks.xstream.XStream;
import fbm.vutbr.martinsakac.bakalarka.dto.NajomnaZmluvaDto;
import fbm.vutbr.martinsakac.bakalarka.dto.upravene.NajomnaZmluvaDetailDto;
import fbm.vutbr.martinsakac.bakalarka.dto.upravene.NajomnaZmluvaDetailForXmlDto;
import fbm.vutbr.martinsakac.bakalarka.output.ExportNajomnaZmluva;
import fbm.vutbr.martinsakac.bakalarka.service.NajomnaZmluvaService;
import fbm.vutbr.martinsakac.bakalarka.utils.NajomnaZmluvaDetailForXmlConverter;
import static fbm.vutbr.martinsakac.bakalarka.web.BaseActionBean.escapeHTML;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.integration.spring.SpringBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.List;
import net.sourceforge.stripes.action.Before;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.DontValidate;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.LocalizableMessage;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.StreamingResolution;
import net.sourceforge.stripes.controller.LifecycleStage;
import net.sourceforge.stripes.validation.Validate;
import net.sourceforge.stripes.validation.ValidateNestedProperties;
import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;
import org.joda.time.LocalDate;

/**
 *
 * @author martin
 */
@UrlBinding("/najomnaZmluva/{$event}/{najomnaZmluva.id}")
public class NajomnaZmluvaActionBean extends BaseActionBean implements ValidationErrorHandler{
     final static Logger log = LoggerFactory.getLogger(VlastnikActionBean.class);
     
    @SpringBean
    protected NajomnaZmluvaService nzService;
     
    private List<NajomnaZmluvaDto> nzList;

    public List<NajomnaZmluvaDto> getNzList() {
        return nzList;
    }
    
    @ValidateNestedProperties(value = {
            @Validate(on = {"add"}, field = "platnostOd", required = true),
            @Validate(on = {"add"}, field = "platnostDo", required = true),
            @Validate(on = {"add"}, field = "sadzba", required = true)
    })
    private NajomnaZmluvaDto nz;

    public NajomnaZmluvaDto getNz() {
        return nz;
    }

    public void setNz(NajomnaZmluvaDto nz) {
        this.nz = nz;
    }
    
    private NajomnaZmluvaDetailDto nzDetail;

    public NajomnaZmluvaDetailDto getNzDetail() {
        return nzDetail;
    }

    public void setNzDetail(NajomnaZmluvaDetailDto nzDetail) {
        this.nzDetail = nzDetail;
    }
    
    // nechcem nacitat do nz !!!
    @Before(stages = LifecycleStage.BindingAndValidation, on = {"addFormDisplay", "addNajomnaZmluva", "cancelNajomnaZmluva"})
    public void loadFromDatabaseSpecial() {
        String id = getContext().getRequest().getParameter("najomnaZmluva.id");
        if (id == null) 
            return;
        nzDetail = nzService.getNajomnaZmluvaDetail(Long.parseLong(id));
    }
    
    
    @Before(stages = LifecycleStage.BindingAndValidation, on = {"displayDetail", "exportToPdf", "editNajomnaZmluva"})
    public void loadFromDatabase() {
        String id = getContext().getRequest().getParameter("najomnaZmluva.id");
        if (id == null || id.length() == 0) 
            return;
        nz = nzService.getNajomnaZmluvaById(Long.parseLong(id));
    }
    
    private void testChosenTab() {
        String chosenTab = getContext().getRequest().getParameter("chosenTab");
        if (chosenTab != null && chosenTab.equals("najomnaZmluva")) {
            super.getContext().getRequest().getSession().setAttribute("chosenTab", "najomnaZmluva");
        }
    }
    
    public Resolution addFormDisplay() {
        log.info("forwarding to addLease form.");
        //nzDetail = nzService.getNajomnaZmluvaDetail(nz.getId());
        return new ForwardResolution("/najomnaZmluva/addNajomnaZmluva.jsp");
    }
    
    public Resolution addNajomnaZmluva(){
        log.info("saving najomna zmluva ={}", nz);
        
        String platnostOd = getContext().getRequest().getParameter("nz.platnostOd");
        String platnostDo = getContext().getRequest().getParameter("nz.platnostDo");
        String[] odDatum = platnostOd.split("-");
        String[] doDatum = platnostDo.split("-");
        nz.setPlatnostOd(new LocalDate(Integer.parseInt(odDatum[0]), Integer.parseInt(odDatum[1]),Integer.parseInt(odDatum[2])));
        nz.setPlatnostDo(new LocalDate(Integer.parseInt(doDatum[0]), Integer.parseInt(doDatum[1]),Integer.parseInt(doDatum[2])));
        
        if (nz.getId() == null){
            nzService.addNajomnaZmluvaSPodielmi(nz, nzDetail.getPodiely());
            getContext().getMessages().add(new LocalizableMessage
                    ("najomnazmluva.add.message"));
        }
        else {
            nzService.updateNajomnaZmluva(nz);
            getContext().getMessages().add(new LocalizableMessage
                    ("najomnazmluva.update.message"));
        }
        return new RedirectResolution(this.getClass(), "list");
    }
    
    public Resolution editNajomnaZmluva() {
        log.info("editing najomnaZmluva {}", nz);
        nzDetail = nzService.getNajomnaZmluvaDetail(nz.getId());
        return new ForwardResolution("/najomnaZmluva/addNajomnaZmluva.jsp");
    }
    
    public Resolution cancelNajomnaZmluva(){
        log.info("Cancelling saving or editing.");
        return new RedirectResolution(this.getClass(), "displayDetail");
    }
    
    public Resolution displayDetail() {
        log.info("Gaining detail of lease.");
        testChosenTab();
        nzDetail = nzService.getNajomnaZmluvaDetail(nz.getId());
//        NajomnaZmluvaDetailForXmlDto forXml = NajomnaZmluvaDetailForXmlConverter.forXml(nzDetail);
//        XStream xstream = new XStream();
//        String xml = xstream.toXML(forXml);
//        super.getContext().getRequest().getSession().setAttribute("xml", xml);
        return new ForwardResolution("/najomnaZmluva/detailNajomnaZmluva.jsp");
        
    }
    
    public Resolution exportToPdf() {
        log.info("Exporting NajomnaZmluva to PDF.");
        nzDetail = nzService.getNajomnaZmluvaDetail(nz.getId());
        NajomnaZmluvaDetailForXmlDto forXml = NajomnaZmluvaDetailForXmlConverter.forXml(nzDetail);
        XStream xstream = new XStream();
        String xml = xstream.toXML(forXml);
        ExportNajomnaZmluva export = new ExportNajomnaZmluva();
        ByteArrayOutputStream result = export.toPdf(xml);
        return new StreamingResolution("application/pdf", new ByteArrayInputStream(result.toByteArray()))
                .setFilename("najomnaZmluva" + nzDetail.getVlastnik().getMeno() + nzDetail.getVlastnik().getPriezvisko());
    }
    
    @DefaultHandler
    @DontValidate
    public Resolution list(){
        log.info("list of all leases.");
        testChosenTab();
        
        String column = getContext().getRequest().getParameter("najomnaZmluva.order.column");
        String direction = getContext().getRequest().getParameter("najomnaZmluva.order.direction");
        if (column == null || direction == null) {
            column = "";
            direction = "";
        }
        
        nzList = nzService.getAllNajomnaZmluva(column, direction);
        return new ForwardResolution("/najomnaZmluva/listNajomnaZmluva.jsp");
    }

    @Override
    public Resolution handleValidationErrors(ValidationErrors ve) throws Exception {
        nzList = nzService.getAllNajomnaZmluva("platnostDo", "desc");
        return null;
    }
     
}
