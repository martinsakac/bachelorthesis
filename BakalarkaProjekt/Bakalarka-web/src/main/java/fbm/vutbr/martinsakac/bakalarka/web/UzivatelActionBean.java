/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.web;

import java.util.List;
import fbm.vutbr.martinsakac.bakalarka.dto.UzivatelDto;
import fbm.vutbr.martinsakac.bakalarka.service.UzivatelService;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.DontValidate;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.LocalizableMessage;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.integration.spring.SpringBean;
import net.sourceforge.stripes.validation.Validate;
import net.sourceforge.stripes.validation.ValidateNestedProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static fbm.vutbr.martinsakac.bakalarka.web.BaseActionBean.escapeHTML;
import net.sourceforge.stripes.action.Before;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.controller.LifecycleStage;
import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;

/**
 *
 * @author martin
 */

@UrlBinding("/uzivatelia/{$event}/{uzivatel.id}")
public class UzivatelActionBean extends BaseActionBean implements ValidationErrorHandler{
    
    final static Logger log = LoggerFactory.getLogger(UzivatelActionBean.class);
    
    @SpringBean
    private UzivatelService uzivatelService;
    
    private List<UzivatelDto> listUzivatel;
    
    @ValidateNestedProperties(value = {
        @Validate(on = {"add", "save"}, field = "meno", required = true),
        @Validate(on = {"add", "save"}, field = "priezvisko", required = true),
        @Validate(on = {"add", "save"}, field = "uzivatelskaRola", required = true)})
    private UzivatelDto uzivatel;

    public UzivatelDto getUzivatel() {
        return uzivatel;
    }

    public void setUzivatel(UzivatelDto uzivatel) {
        this.uzivatel = uzivatel;
    }

    public List<UzivatelDto> getListUzivatel() {
        return listUzivatel;
    }
    
    @DefaultHandler
    @DontValidate
    public Resolution list(){
        log.info("Listing all uzivatel!");
        
        String chosenTab = getContext().getRequest().getParameter("chosenTab");
        if (chosenTab != null && chosenTab.equals("uzivatel")) {
            super.getContext().getRequest().getSession().setAttribute("chosenTab", "uzivatel");
        }
        
        listUzivatel = uzivatelService.getAllUzivatel();
        return new ForwardResolution("/uzivatel/uzivatelList.jsp");
    }
    
    public Resolution add(){
        log.info("adding or updating Uzivatel!");
        
        if (uzivatel.getId() == null){
            uzivatelService.addUzivatel(uzivatel);
            getContext().getMessages().add(new LocalizableMessage
                    ("uzivatel.add.message", 
                    escapeHTML(uzivatel.getMeno()), 
                    escapeHTML(uzivatel.getPriezvisko())));
        }
        else {
            uzivatelService.updateUzivatel(uzivatel);
            getContext().getMessages().add(new LocalizableMessage
                    ("uzivatel.update.message", 
                    escapeHTML(uzivatel.getMeno()), 
                    escapeHTML(uzivatel.getPriezvisko())));
        }
        return new RedirectResolution(this.getClass(), "list");
    }
    
    public Resolution cancel(){
        log.info("Canceling saving or editing!");
        return new RedirectResolution(this.getClass(), "list");
    }
    
    public Resolution edit(){
        log.info("Editing user: {}!", uzivatel);
        listUzivatel = uzivatelService.getAllUzivatel();
        return new ForwardResolution("/uzivatel/uzivatelList.jsp");
    }
    
    @Before(stages = LifecycleStage.BindingAndValidation, on = {"edit"})
    public void loadUzivatelFromDb(){
        String id = getContext().getRequest().getParameter("uzivatel.id");
        if (id != null) {
            uzivatel = uzivatelService.getUzivatelById(Long.valueOf(id));
        }
    }
    
    public Resolution delete(){
        log.info("Deleting user: {}", uzivatel);
        uzivatel = uzivatelService.getUzivatelById(uzivatel.getId());
        uzivatelService.deleteUzivatel(uzivatel);
        getContext().getMessages().add(new LocalizableMessage
                ("uzivatel.delete.message", 
                escapeHTML(uzivatel.getMeno()), 
                escapeHTML(uzivatel.getPriezvisko())));
        return new RedirectResolution(this.getClass(), "list");
    }
    
    @Override    
    public Resolution handleValidationErrors(ValidationErrors ve) throws Exception {
        listUzivatel = uzivatelService.getAllUzivatel();
        return null;
    }
}
