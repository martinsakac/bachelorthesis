<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : najomnaZmluva.xsl
    Created on : Sobota, 2014, máj 17, 16:15
    Author     : Martin
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" 
                xmlns:fo="http://www.w3.org/1999/XSL/Format">

    <xsl:output version="1.0" method="xml" encoding="UTF-8" indent="yes"/>
    
    <xsl:template match="fbm.vutbr.martinsakac.bakalarka.dto.upravene.NajomnaZmluvaDetailForXmlDto"> 
        <fo:root>
            <fo:layout-master-set>
                <fo:simple-page-master master-name="my-page" 
                        page-height="29.7cm" 
                        page-width="21cm" 
                        margin-top="2cm" 
                        margin-bottom="2cm" 
                        margin-left="2.5cm" 
                        margin-right="2.5cm"> 
                    <fo:region-body margin-top="2cm" margin-bottom="1.5cm"/> 
                    <fo:region-before extent="3cm"/> 
                    <fo:region-after extent="1.5cm"/> 
                </fo:simple-page-master>
            </fo:layout-master-set>
            
            <fo:page-sequence master-reference="my-page">
                <!-- header --> 
                <fo:static-content flow-name="xsl-region-before"> 
                    <fo:block text-align="center" 
                            font-size="16pt" 
                            font-family="Times" 
                            line-height="25pt" 
                            font-weight="bold"> 
                            Nájomná zmluva
                    </fo:block> 
                </fo:static-content> 

                <fo:flow flow-name="xsl-region-body">
                    <fo:block>
                      <fo:table width="160mm" table-layout="fixed"
                                font-size="12pt" font-family="Times" line-height="20pt">
                         <fo:table-column column-number="1" column-width="60mm" />
                         <fo:table-column column-number="2" column-width="100mm" />
                         <fo:table-body>
                             <fo:table-row>
                                 <fo:table-cell border="solid 1px black" 
                                      text-align="left" font-weight="bold">
                                    <fo:block>
                                      Objednávatel
                                    </fo:block>
                                 </fo:table-cell>
                                 <fo:table-cell border="solid 1px black" 
                                        text-align="center">
                                    <fo:block>
                                        Polnohospodárske družstvo vo Vrábloch
                                    </fo:block>
                                 </fo:table-cell>
                             </fo:table-row>
                             <fo:table-row>
                                 <fo:table-cell border="solid 1px black" 
                                      text-align="left" font-weight="bold">
                                     <fo:block>
                                         Majitel
                                     </fo:block>
                                 </fo:table-cell>
                                 <fo:table-cell border="solid 1px black" text-align="center">
                                     <fo:block>
                                         <xsl:value-of select="vlastnikMeno"/>&#160;<xsl:value-of select="vlastnikPriezvisko"/> 
                                     </fo:block>
                                 </fo:table-cell>
                             </fo:table-row>
                             <fo:table-row>
                                <fo:table-cell border="solid 1px black" 
                                      text-align="left" font-weight="bold">
                                     <fo:block>
                                         Platnost zmluvy od
                                     </fo:block>
                                 </fo:table-cell>
                                 <fo:table-cell border="solid 1px black" text-align="center">
                                     <fo:block>
                                         <xsl:value-of select="platnostOdDen"/>.
                                         <xsl:value-of select="platnostOdMesiac"/>.
                                         <xsl:value-of select="platnostOdRok"/>
                                     </fo:block>
                                 </fo:table-cell>
                             </fo:table-row>
                             <fo:table-row>
                                <fo:table-cell border="solid 1px black" 
                                      text-align="left" font-weight="bold">
                                     <fo:block>
                                         Platnost zmluvy do
                                     </fo:block>
                                 </fo:table-cell>
                                 <fo:table-cell border="solid 1px black" text-align="center">
                                     <fo:block>
                                         <xsl:value-of select="platnostDoDen"/>.
                                         <xsl:value-of select="platnostDoMesiac"/>.
                                         <xsl:value-of select="platnostDoRok"/>
                                     </fo:block>
                                 </fo:table-cell>
                             </fo:table-row>
                             
                             <fo:table-row>
                                <fo:table-cell border="solid 1px black" 
                                      text-align="left" font-weight="bold">
                                     <fo:block>
                                         Sadzba [€/ha/rok]
                                     </fo:block>
                                 </fo:table-cell>
                                 <fo:table-cell border="solid 1px black" text-align="center">
                                     <fo:block>
                                         <xsl:value-of select="sadzba"/>
                                     </fo:block>
                                 </fo:table-cell>
                             </fo:table-row>
                             <fo:table-row>
                                <fo:table-cell border="solid 1px black" 
                                      text-align="left" font-weight="bold">
                                     <fo:block>
                                         Dlžka nájmu v rokoch
                                     </fo:block>
                                 </fo:table-cell>
                                 <fo:table-cell border="solid 1px black" text-align="center">
                                     <fo:block>
                                         <xsl:value-of select="pocetRokov"/>
                                     </fo:block>
                                 </fo:table-cell>
                             </fo:table-row>
                             <fo:table-row>
                                <fo:table-cell border="solid 1px black" 
                                      text-align="left" font-weight="bold">
                                     <fo:block>
                                         Pozemky v katastroch
                                     </fo:block>
                                 </fo:table-cell>
                                 <fo:table-cell border="solid 1px black" text-align="center">
                                     <xsl:for-each select="uzemiaRozloha/entry">
                                         <fo:block>
                                            <xsl:value-of select="string[2]"/> ha - <xsl:value-of select="string[1]"/>
                                        </fo:block>
                                     </xsl:for-each>
                                 </fo:table-cell>
                             </fo:table-row>
                             <fo:table-row>
                                <fo:table-cell border="solid 1px black" 
                                      text-align="left" font-weight="bold">
                                     <fo:block>
                                         Sucet vymier [ha]
                                     </fo:block>
                                 </fo:table-cell>
                                 <fo:table-cell border="solid 1px black" text-align="center">
                                     <fo:block>
                                         <xsl:value-of select="sucetVymier"/>
                                     </fo:block>
                                 </fo:table-cell>
                             </fo:table-row>
                             <fo:table-row>
                                <fo:table-cell border="solid 1px black" 
                                      text-align="left" font-weight="bold">
                                     <fo:block>
                                         Vyplatena ciastka [€]
                                     </fo:block>
                                 </fo:table-cell>
                                 <fo:table-cell border="solid 1px black" text-align="center" font-weight="bold">
                                     <fo:block>
                                         <xsl:value-of select="vyplatenaCiastka"/>
                                     </fo:block>
                                 </fo:table-cell>
                             </fo:table-row>
                             <fo:table-row>
                                <fo:table-cell border="solid 1px black" 
                                      text-align="left" font-weight="bold">
                                     <fo:block>
                                         Dátum zhotovenia zmluvy
                                     </fo:block>
                                 </fo:table-cell>
                                 <fo:table-cell border="solid 1px black" text-align="center">
                                     <fo:block>
                                         <xsl:value-of select="datumZhotoveniaDen"/>.
                                         <xsl:value-of select="datumZhotoveniaMesiac"/>.
                                         <xsl:value-of select="datumZhotoveniaRok"/>
                                     </fo:block>
                                 </fo:table-cell>
                             </fo:table-row>
                             
                          </fo:table-body>
                      </fo:table>
                    </fo:block>
                 </fo:flow>
              </fo:page-sequence>
            
            
        </fo:root>
     </xsl:template>
     
</xsl:stylesheet>

