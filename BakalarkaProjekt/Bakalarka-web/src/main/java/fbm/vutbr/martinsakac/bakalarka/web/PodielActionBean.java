/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.web;

import fbm.vutbr.martinsakac.bakalarka.dto.PodielDto;
import fbm.vutbr.martinsakac.bakalarka.dto.VlastnikDto;
import fbm.vutbr.martinsakac.bakalarka.service.PodielService;
import fbm.vutbr.martinsakac.bakalarka.service.VlastnikService;
import java.util.List;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.DontValidate;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.LocalizableMessage;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.integration.spring.SpringBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author martin
 */

@UrlBinding("/podiely/{$event}/{podiel.id}")
public class PodielActionBean extends BaseActionBean{
    
    final static Logger log = LoggerFactory.getLogger(VlastnikActionBean.class);
    
    private List<PodielDto> listPodiel;
    
    private List<VlastnikDto> listVlastnik;
    
    private PodielDto podiel;
    
    private VlastnikDto vlastnik;
    
    @SpringBean
    private PodielService podielService;
    
    @SpringBean
    private VlastnikService vlastnikService;

    public List<VlastnikDto> getListVlastnik() {
        return listVlastnik;
    }

    public List<PodielDto> getListPodiel() {
        return listPodiel;
    }

    public PodielDto getPodiel() {
        return podiel;
    }

    public void setPodiel(PodielDto podiel) {
        this.podiel = podiel;
    }

    public void setVlastnik(VlastnikDto vlastnik) {
        this.vlastnik = vlastnik;
    }

    public VlastnikDto getVlastnik() {
        return vlastnik;
    }
    
    @DefaultHandler
    @DontValidate
    public Resolution list(){
        log.info("list all podiel.");
        
        String chosenTab = getContext().getRequest().getParameter("chosenTab");
        if (chosenTab != null && chosenTab.equals("podiel")) {
            super.getContext().getRequest().getSession().setAttribute("chosenTab", "podiel");
        }
        
        listPodiel = podielService.getAllPodiel();
        //listVlastnik = vlastnikService.getAllVlastnik();
        return new ForwardResolution("/podiel/podielList.jsp");
    }
    
    public Resolution save(){
        log.info("save() - podiel");
        log.info("ID vlastnika: " + vlastnik.getId());
        vlastnik = vlastnikService.getVlastnikById(vlastnik.getId());
        podiel.setVlastnik(vlastnik);
        if (podiel.getId() == null){
            podielService.addPodiel(podiel);
            getContext().getMessages().add(new LocalizableMessage("podiel.add.message"));
        }
        else {
            podielService.updatePodiel(podiel);
            getContext().getMessages().add(new LocalizableMessage("podiel.update.message"));
        }
        return new RedirectResolution(this.getClass(), "list");
    }
    
    public Resolution cancel(){
        log.debug("Cancelling saving or editing.");
        return new RedirectResolution(this.getClass(), "list");
    }
}
