/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.daoImpl;

import fbm.vutbr.martinsakac.bakalarka.daoimpl.VlastnikDaoImpl;
import fbm.vutbr.martinsakac.bakalarka.entity.Vlastnik;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import junit.framework.TestCase;
import org.junit.Ignore;

/**
 *
 * @author martin
 */
public class VlastnikDaoImplTest extends TestCase{
    
    VlastnikDaoImpl dao;
    
    private EntityManagerFactory emFactory;

    private EntityManager em;
    
    private static Logger logger = 
            Logger.getLogger(VlastnikDaoImplTest.class.getName());

    public VlastnikDaoImplTest(String testName){
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception{
        super.setUp();
        logger.info("Building JPA EntityManager for unit tests");
        
        emFactory = Persistence.createEntityManagerFactory("testPU");
        em = emFactory.createEntityManager();
        
        this.dao = new VlastnikDaoImpl();
        this.dao.setEntityManager(em);
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        logger.info("Stopping in-memory database.");
    }
    
    public void testCreateVlastnik(){
        Vlastnik vlastnik = new Vlastnik();
        em.getTransaction().begin();
        dao.createVlastnik(vlastnik);
        em.getTransaction().commit();
        assertNotNull(vlastnik.getId());
    }
    
    public void testGetVlastnikById(){
        Vlastnik vlastnik = new Vlastnik();
        em.getTransaction().begin();
        em.persist(vlastnik);
        em.getTransaction().commit();
        
        Long id1 = vlastnik.getId();
        assertNotNull(id1);
        assertNotNull(dao.getVlastnikById(id1));
        assertEquals(vlastnik, dao.getVlastnikById(id1));
    }
    
//    @Ignore
    public void testGetAllVlastnik(){
        Vlastnik vlastnik1 = new Vlastnik();
        Vlastnik vlastnik2 = new Vlastnik();
        Vlastnik vlastnik3 = new Vlastnik();
        em.getTransaction().begin();
        em.persist(vlastnik1);
        em.persist(vlastnik2);
        em.persist(vlastnik3);
        em.getTransaction().commit();
        //assertEquals(dao.getAllVlastnik().size(), 3);
    }
}
