/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.daoImpl;

import java.util.List;
import fbm.vutbr.martinsakac.bakalarka.daoimpl.NajomnaZmluvaDaoImpl;
import fbm.vutbr.martinsakac.bakalarka.entity.ListVlastnictva;
import fbm.vutbr.martinsakac.bakalarka.entity.NajomnaZmluva;
import fbm.vutbr.martinsakac.bakalarka.entity.Parcela;
import fbm.vutbr.martinsakac.bakalarka.entity.Podiel;
import fbm.vutbr.martinsakac.bakalarka.entity.PodielNajomnaZmluva;
import fbm.vutbr.martinsakac.bakalarka.entity.Vlastnik;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import junit.framework.TestCase;
import org.junit.Ignore;
import java.math.BigDecimal;

/**
 *
 * @author martin
 */
public class NajomnaZmluvaDaoImplTest extends TestCase{
    NajomnaZmluvaDaoImpl dao;

    private EntityManagerFactory emFactory;

    private EntityManager em;
    
    private static Logger logger = 
            Logger.getLogger(VlastnikDaoImplTest.class.getName());
    
    public NajomnaZmluvaDaoImplTest(String testName){
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception{
        super.setUp();
        logger.info("Building JPA EntityManager for unit tests");
        
        emFactory = Persistence.createEntityManagerFactory("testPU");
        em = emFactory.createEntityManager();
        
        this.dao = new NajomnaZmluvaDaoImpl();
        this.dao.setEntityManager(em);
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        logger.info("Stopping in-memory database.");
    }
    
    @Ignore
    public void testGetNajomnaZmluvaByVlastnikId(){
        Vlastnik vlastnik = new Vlastnik();
        Podiel podiel = new Podiel();
        Podiel podiel2 = new Podiel();
        NajomnaZmluva nz = new NajomnaZmluva();
        podiel.setVlastnik(vlastnik);
        podiel2.setVlastnik(vlastnik);
        PodielNajomnaZmluva pnz = new PodielNajomnaZmluva();
        PodielNajomnaZmluva pnz2 = new PodielNajomnaZmluva();
        pnz.setPodiel(podiel);
        pnz.setNajomnaZmluva(nz);
        pnz2.setPodiel(podiel2);
        pnz2.setNajomnaZmluva(nz);
        
        em.getTransaction().begin();
        em.persist(vlastnik);
        em.getTransaction().commit();
        
        em.getTransaction().begin();
        em.persist(podiel);
        em.persist(podiel2);
        em.getTransaction().commit();
        
        em.getTransaction().begin();
        em.persist(nz);
        em.getTransaction().commit();
        
        em.getTransaction().begin();
        em.persist(pnz);
        em.persist(pnz2);
        em.getTransaction().commit();
        
        Long idVlastnika = vlastnik.getId();
        List<NajomnaZmluva> result = dao.getNajomnaZmluvaByVlastnikId(idVlastnika);
        
        assertNotNull(result);
        System.out.println(result.size() + " - pocet");
        //assertEquals(result.size(), 2);
        assertEquals(result.get(0), nz);
    }
    
    public void testGetNajomnaZmluvaSucetVymier(){
        NajomnaZmluva nz = new NajomnaZmluva();
        
        Podiel podiel = new Podiel();
        Podiel podiel2 = new Podiel();
        
        PodielNajomnaZmluva pnz = new PodielNajomnaZmluva();
        pnz.setPodiel(podiel);
        pnz.setNajomnaZmluva(nz);
        
        PodielNajomnaZmluva pnz2 = new PodielNajomnaZmluva();
        pnz2.setPodiel(podiel2);
        pnz2.setNajomnaZmluva(nz);
        
        ListVlastnictva lv = new ListVlastnictva();
        ListVlastnictva lv2 = new ListVlastnictva();
        podiel.setListVlastnictva(lv);
        podiel2.setListVlastnictva(lv2);
        
        Parcela parcela = new Parcela();
        Parcela parcela2 = new Parcela();
        parcela.setListVlastnictva(lv);
        parcela2.setListVlastnictva(lv2);
        parcela.setVymera(new BigDecimal(1.111));
        parcela2.setVymera(new BigDecimal(2.333));
        
        em.getTransaction().begin();
        em.persist(nz);
        em.getTransaction().commit();
        
        em.getTransaction().begin();
        em.persist(lv);
        em.persist(lv2);
        em.getTransaction().commit();
        
        em.getTransaction().begin();
        em.persist(podiel);
        em.persist(podiel2);
        em.getTransaction().commit();
        
        em.getTransaction().begin();
        em.persist(pnz);
        em.persist(pnz2);
        em.getTransaction().commit();
        
        em.getTransaction().begin();
        em.persist(parcela);
        em.persist(parcela2);
        em.getTransaction().commit();
        
//        BigDecimal result = dao.getNajomnaZmluvaSucetVymier(nz);
//        assertNotNull(result);
//        assertEquals(new BigDecimal(3.444), result);
    }
}
