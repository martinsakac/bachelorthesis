/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.daoImpl;

import fbm.vutbr.martinsakac.bakalarka.daoimpl.ParcelaDaoImpl;
import fbm.vutbr.martinsakac.bakalarka.entity.KatastralneUzemie;
import fbm.vutbr.martinsakac.bakalarka.entity.ListVlastnictva;
import fbm.vutbr.martinsakac.bakalarka.entity.Parcela;
import fbm.vutbr.martinsakac.bakalarka.entity.Podiel;
import fbm.vutbr.martinsakac.bakalarka.entity.Vlastnik;
import java.math.BigDecimal;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import junit.framework.TestCase;
import java.util.logging.Logger;
import java.util.List;

/**
 *
 * @author martin
 */
public class ParcelaDaoImplTest extends TestCase{
    ParcelaDaoImpl dao;
    
    private EntityManagerFactory emFactory;

    private EntityManager em;
    
    private static Logger logger = 
            Logger.getLogger(VlastnikDaoImplTest.class.getName());
    
    public ParcelaDaoImplTest(String testName){
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception{
        super.setUp();
        logger.info("Building JPA EntityManager for unit tests");
        
        emFactory = Persistence.createEntityManagerFactory("testPU");
        em = emFactory.createEntityManager();
        
        this.dao = new ParcelaDaoImpl();
        this.dao.setEntityManager(em);
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        logger.info("Stopping in-memory database.");
    }
    
    public void testGetAllParcelaByVlastnikId(){
        Vlastnik vlastnik = new Vlastnik();
        
        Podiel podiel = new Podiel();
        Podiel podiel2 = new Podiel();
        podiel.setVlastnik(vlastnik);
        podiel2.setVlastnik(vlastnik);
        
        ListVlastnictva lv = new ListVlastnictva();
        ListVlastnictva lv2 = new ListVlastnictva();
        podiel.setListVlastnictva(lv);
        podiel2.setListVlastnictva(lv2);
        
        Parcela parcela = new Parcela();
        Parcela parcela2 = new Parcela();
        parcela.setListVlastnictva(lv);
        parcela2.setListVlastnictva(lv2);
        parcela.setVymera(new BigDecimal(1.111));
        parcela2.setVymera(new BigDecimal(2.333));
        
        KatastralneUzemie ku = new KatastralneUzemie();
        ku.setNazov("HAhaha");
        parcela.setKatastralneUzemie(ku);
        parcela2.setKatastralneUzemie(ku);
        
        em.getTransaction().begin();
        em.persist(vlastnik);
        em.persist(ku);
        em.getTransaction().commit();
        
        em.getTransaction().begin();
        em.persist(lv);
        em.persist(lv2);
        em.getTransaction().commit();
        
        em.getTransaction().begin();
        em.persist(podiel);
        em.persist(podiel2);
        em.getTransaction().commit();
        
        em.getTransaction().begin();
        em.persist(parcela);
        em.persist(parcela2);
        em.getTransaction().commit();
        
        List<Parcela> result = dao.getAllParcelaByVlastnikId(vlastnik.getId());
        
        assertNotNull(result);
        assertEquals(result.size(), 2);
        
        System.out.println(result.get(0).getVymera());
        System.out.println(result.get(0).getKatastralneUzemie().getNazov());
    }
}
