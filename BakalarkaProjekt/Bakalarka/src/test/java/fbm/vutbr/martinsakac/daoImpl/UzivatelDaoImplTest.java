/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.daoImpl;

import fbm.vutbr.martinsakac.bakalarka.daoimpl.UzivatelDaoImpl;
import fbm.vutbr.martinsakac.bakalarka.entity.Uzivatel;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import junit.framework.TestCase;

/**
 *
 * @author martin
 */
public class UzivatelDaoImplTest extends TestCase{
    
    UzivatelDaoImpl dao;
    
    private EntityManagerFactory emFactory;

    private EntityManager em;
    
    private static Logger logger = 
            Logger.getLogger(VlastnikDaoImplTest.class.getName());

    public UzivatelDaoImplTest(String testName){
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception{
        super.setUp();
        logger.info("Building JPA EntityManager for unit tests");
        
        emFactory = Persistence.createEntityManagerFactory("testPU");
        em = emFactory.createEntityManager();
        
        this.dao = new UzivatelDaoImpl();
        this.dao.setEntityManager(em);
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        logger.info("Stopping in-memory database.");
    }
    
    public void testGetUzivatelByLogin(){
        Uzivatel uzivatel1 = new Uzivatel();
        Uzivatel uzivatel2 = new Uzivatel();
        uzivatel1.setUzivatelskeMeno("sakinko");
        uzivatel2.setUzivatelskeMeno("saki");
        
        em.getTransaction().begin();
        em.persist(uzivatel1);
        em.persist(uzivatel2);
        em.getTransaction().commit();
        
        Uzivatel result = dao.getUzivatelByLogin("saki");
        
        assertNotNull(result);
        assertEquals(uzivatel2, result);
    }
}
