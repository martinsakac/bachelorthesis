/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.daoimpl;

import fbm.vutbr.martinsakac.bakalarka.dao.UzivatelDao;
import fbm.vutbr.martinsakac.bakalarka.entity.Uzivatel;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author martin
 */

@Repository
public class UzivatelDaoImpl implements UzivatelDao{
    
    @PersistenceContext
    private EntityManager em;

    public EntityManager getEntityManager(){
        return this.em;
    }
    
    public void setEntityManager(EntityManager em){
        this.em = em;
    }
    
    @Override
    public void createUzivatel(Uzivatel uz) {
        Validate.isTrue(uz != null, "Uzivatel must not be null!");
        Validate.isTrue(uz.getId() == null, "Uzivatel ID must be null!");
        em.persist(uz);
    }

    @Override
    public void updateUzivatel(Uzivatel uz) {
        Validate.isTrue(uz != null, "Uzivatel must not be null!");
        Validate.isTrue(uz.getId() != null, "Uzivatel ID must not be null!");
        em.merge(uz);
    }

    @Override
    public void deleteUzivatel(Uzivatel uz) {
        Validate.isTrue(uz != null, "Uzivatel must not be null!");
        Validate.isTrue(uz.getId() != null, "Uzivatel ID must not be null!");
        uz = em.merge(uz);
        if (uz == null){
            throw new IllegalArgumentException("Uzivatel is not in DB, cannot be deleted!");
        }
        else {
            em.remove(uz);
        }
    }

    @Override
    public Uzivatel getUzivatelById(Long id) {
        Validate.isTrue(id != null, "Uzivatel ID must not be null!");
        return em.find(Uzivatel.class, id);
    }

    @Override
    public List<Uzivatel> getAllUzivatel() {
        TypedQuery<Uzivatel> query = em.createQuery("select u from Uzivatel u", Uzivatel.class);
        return query.getResultList();
    }

    @Override
    public Uzivatel getUzivatelByLogin(String login) {
        TypedQuery<Uzivatel> query = em.createQuery("SELECT u FROM Uzivatel u "
                + "WHERE u.uzivatelskeMeno = :login", Uzivatel.class).setParameter("login", login);
        return query.getSingleResult();
    }
    
}
