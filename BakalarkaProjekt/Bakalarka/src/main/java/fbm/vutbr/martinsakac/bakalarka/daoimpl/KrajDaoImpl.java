/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.daoimpl;

import fbm.vutbr.martinsakac.bakalarka.dao.KrajDao;
import fbm.vutbr.martinsakac.bakalarka.entity.Kraj;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author martin
 */
@Repository
public class KrajDaoImpl implements KrajDao{

    @PersistenceContext
    private EntityManager em;
    
    public EntityManager getEntityManager(){
        return this.em;
    }
    
    public void setEntityManager(EntityManager em){
        this.em = em;
    }
    
    @Override
    public void createKraj(Kraj kraj) {
        Validate.isTrue(kraj != null, "Kraj must not be null.");
        Validate.isTrue(kraj.getId() == null, "Kraj ID must be null");
        em.persist(em);
    }

    @Override
    public void updateKraj(Kraj kraj) {
        Validate.isTrue(kraj != null, "Kraj must not be null.");
        Validate.isTrue(kraj.getId() != null, "Kraj ID must not be null");
        em.merge(em);
    }

    @Override
    public void deleteKraj(Kraj kraj) {
        Validate.isTrue(kraj != null, "Kraj must not be null.");
        Validate.isTrue(kraj.getId() != null, "Kraj ID must not be null");
        kraj = em.merge(kraj);
        if (kraj == null){
            throw new IllegalArgumentException("Kraj is not in DB, cannot be deleted.");
        }
        em.remove(em);
    }

    @Override
    public Kraj getKrajById(Long id) {
        Validate.isTrue(id != null, "ID cannot be null");
        return em.find(Kraj.class, id);
    }

    @Override
    public List<Kraj> getAllKraj() {
        TypedQuery<Kraj> query = em.createQuery("select k from Kraj k", Kraj.class);
        return query.getResultList();
    }
    
}
