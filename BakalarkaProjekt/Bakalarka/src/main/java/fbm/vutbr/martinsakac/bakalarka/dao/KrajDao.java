/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.dao;

import fbm.vutbr.martinsakac.bakalarka.entity.Kraj;
import java.util.List;

/**
 *
 * @author martin
 */
public interface KrajDao {
    public void createKraj(Kraj kraj);
    public void updateKraj(Kraj kraj);
    public void deleteKraj(Kraj kraj);
    public Kraj getKrajById(Long id);
    public List<Kraj> getAllKraj();
}
