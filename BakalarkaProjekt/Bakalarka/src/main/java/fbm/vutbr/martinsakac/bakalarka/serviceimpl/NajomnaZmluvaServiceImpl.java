/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.serviceimpl;

import fbm.vutbr.martinsakac.bakalarka.dao.NajomnaZmluvaDao;
import fbm.vutbr.martinsakac.bakalarka.dao.ParcelaDao;
import fbm.vutbr.martinsakac.bakalarka.dao.PodielDao;
import fbm.vutbr.martinsakac.bakalarka.dao.PodielNajomnaZmluvaDao;
import fbm.vutbr.martinsakac.bakalarka.dao.VlastnikDao;
import fbm.vutbr.martinsakac.bakalarka.dto.NajomnaZmluvaDto;
import fbm.vutbr.martinsakac.bakalarka.dto.ParcelaDto;
import fbm.vutbr.martinsakac.bakalarka.dto.PodielDto;
import fbm.vutbr.martinsakac.bakalarka.dto.PodielNajomnaZmluvaDto;
import fbm.vutbr.martinsakac.bakalarka.dto.upravene.NajomnaZmluvaDetailDto;
import fbm.vutbr.martinsakac.bakalarka.dto.upravene.NajomnaZmluvaUpravenaDto;
import fbm.vutbr.martinsakac.bakalarka.entity.NajomnaZmluva;
import fbm.vutbr.martinsakac.bakalarka.entity.Parcela;
import fbm.vutbr.martinsakac.bakalarka.entity.Podiel;
import fbm.vutbr.martinsakac.bakalarka.entity.Vlastnik;
import fbm.vutbr.martinsakac.bakalarka.service.NajomnaZmluvaService;
import fbm.vutbr.martinsakac.bakalarka.utils.NajomnaZmluvaConverter;
import fbm.vutbr.martinsakac.bakalarka.utils.NajomnaZmluvaUpravenaConverter;
import fbm.vutbr.martinsakac.bakalarka.utils.ParcelaConverter;
import fbm.vutbr.martinsakac.bakalarka.utils.PodielConverter;
import fbm.vutbr.martinsakac.bakalarka.utils.PodielNajomnaZmluvaConverter;
import fbm.vutbr.martinsakac.bakalarka.utils.UzivatelConverter;
import fbm.vutbr.martinsakac.bakalarka.utils.VlastnikConverter;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.Validate;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author martin
 */

@Service("najomnaZmluvaServiceImpl")
public class NajomnaZmluvaServiceImpl implements NajomnaZmluvaService{

    @Autowired
    private NajomnaZmluvaDao nzDao;
    
    @Autowired
    private VlastnikDao vlastnikDao;
    
    @Autowired
    private ParcelaDao parcelaDao;
    
    @Autowired
    private PodielDao podielDao;
    
    @Autowired
    private PodielNajomnaZmluvaDao pnzDao;

    public NajomnaZmluvaDao getNzDao() {
        return nzDao;
    }

    public void setNzDao(NajomnaZmluvaDao nzDao) {
        this.nzDao = nzDao;
    }
    
    @Override
    @Transactional
    public void addNajomnaZmluva(NajomnaZmluvaDto nzDto) {
        Validate.isTrue(nzDto != null, "Najomna zmluva must not be null!");
        Validate.isTrue(nzDto.getId() == null, "Najomna zmluva ID must be null.");
        NajomnaZmluva nz = NajomnaZmluvaConverter.najomnaZmluvaDtoToNajomnaZmluva(nzDto);
        nzDao.createNajomnaZmluva(nz);
        nzDto.setId(nz.getId());
    }
    
    @Override
    @Transactional
    public void addNajomnaZmluvaSPodielmi(NajomnaZmluvaDto nzDto, List<PodielDto> podielyList) {
        Validate.isTrue(nzDto != null, "Najomna zmluva must not be null!");
        Validate.isTrue(nzDto.getId() == null, "Najomna zmluva ID must be null.");
        Validate.isTrue(podielyList != null, "K najomnej zmluve musia byt priradene podiely.");
        NajomnaZmluva nz = NajomnaZmluvaConverter.najomnaZmluvaDtoToNajomnaZmluva(nzDto);
        nzDao.createNajomnaZmluva(nz);
        nzDto.setId(nz.getId());
        for (PodielDto podiel : podielyList) {
            PodielNajomnaZmluvaDto pnzDto = new PodielNajomnaZmluvaDto();
            pnzDto.setNajomnaZmluva(nzDto);
            pnzDto.setPodiel(podiel);
            pnzDao.createPodielNajomnaZmluva
                (PodielNajomnaZmluvaConverter.podielNajomnaZmluvaDtoToPodielNajomnaZmluva(pnzDto));
        }
    }

    @Override
    @Transactional
    public void updateNajomnaZmluva(NajomnaZmluvaDto nzDto) {
        Validate.isTrue(nzDto != null, "Najomna zmluva must not be null!");
        Validate.isTrue(nzDto.getId() != null, "Najomna zmluva ID must not be null.");
        nzDao.updateNajomnaZmluva(NajomnaZmluvaConverter.najomnaZmluvaDtoToNajomnaZmluva(nzDto));
    }

    @Override
    @Transactional
    public void deleteNajomnaZmluva(NajomnaZmluvaDto nzDto) {
        Validate.isTrue(nzDto != null, "Najomna zmluva must not be null!");
        Validate.isTrue(nzDto.getId() != null, "Najomna zmluva ID must not be null.");
        nzDao.deleteNajomnaZmluva(NajomnaZmluvaConverter.najomnaZmluvaDtoToNajomnaZmluva(nzDto));
    }

    @Override
    @Transactional
    public NajomnaZmluvaDto getNajomnaZmluvaById(Long id) {
        Validate.isTrue(id != null, "Najomna zmluva ID must not be null.");
        return NajomnaZmluvaConverter.najomnaZmluvaToNajomnaZmluvaDto(nzDao.getNajomnaZmluvaById(id));
    }

    @Override
    @Transactional
    public List<NajomnaZmluvaDto> getAllNajomnaZmluva(String column, String direction) {
        Validate.isTrue(column != null, "Stlpec nemoze byt null.");
        Validate.isTrue(column != null, "Direction nesmie byt null.");
        List<NajomnaZmluvaDto> result = new ArrayList<>();
        for (NajomnaZmluva nz : nzDao.getAllNajomnaZmluva(column, direction)){
            NajomnaZmluvaDto nzDto = NajomnaZmluvaConverter.najomnaZmluvaToNajomnaZmluvaDto(nz);
            if (nzDto.getCurrent() == 0) {
                nzDto.setHasValidFollower(0);
                Vlastnik vlastnik = nzDao.getVlastnikByNajomnaZmluva(nzDto.getId());
                List<NajomnaZmluva> nzByVlastnikList = nzDao.getNajomnaZmluvaByVlastnikId(vlastnik.getId());
                LocalDate currentDate = new LocalDate();
                for (NajomnaZmluva nzByVlastnik : nzByVlastnikList) {
                    if (nzByVlastnik.getPlatnostDo().isAfter(currentDate)) {
                        nzDto.setHasValidFollower(1);
                    }
                }
            }
            result.add(nzDto);
        }
        return result;
    }

    @Override
    public List<NajomnaZmluvaUpravenaDto> getNajomnaZmluvaByVlastnikId(Long vlastnikId) {
        Validate.isTrue(vlastnikId != null, "Cannot, look for Najomna zmluva with vlastnik ID which is null!");
        List<NajomnaZmluvaUpravenaDto> result = new ArrayList<>();
        for (NajomnaZmluva nz : nzDao.getNajomnaZmluvaByVlastnikId(vlastnikId)){
            //result.add(NajomnaZmluvaUpravenaConverter.convertToDto(nz, vlastnikId));
        
            NajomnaZmluvaUpravenaDto nzu = new NajomnaZmluvaUpravenaDto();
            nzu.setId(nz.getId());
            nzu.setIdVlastnika(vlastnikId);
            nzu.setPlatnostDo(nz.getPlatnostDo());
            nzu.setPlatnostOd(nz.getPlatnostOd());
            nzu.setDatumZhotovenia(nz.getDatumZhotovenia());
            nzu.setSadzba(nz.getSadzba());
            if (nz.getUzivatel() != null){
               nzu.setUzivatel(UzivatelConverter.uzivatelToUzivatelDto(nz.getUzivatel()));
            }
            if (nz.getPlatnostDo() != null && nz.getPlatnostOd() != null){
                nzu.setPocetRokov(new Long(nz.getPlatnostDo().getYear() - nz.getPlatnostOd().getYear() + 1));
            }
            nzu.setVyplatenaCiastka(nzDao.getNajomnaZmluvaSucetVymier(nz).multiply(nzu.getSadzba()).setScale(2));
        }
        return result;
    }

    @Override
    public NajomnaZmluvaDetailDto getNajomnaZmluvaDetail(Long id) {
        Validate.isTrue(id != null, "NajomnaZmluva ID is null");
        
        NajomnaZmluvaDto nz = getNajomnaZmluvaById(id);
        NajomnaZmluvaDetailDto nzDetail = new NajomnaZmluvaDetailDto();
        
        nzDetail.setId(nz.getId());
        nzDetail.setDatumZhotovenia(nz.getDatumZhotovenia());
        nzDetail.setPlatnostOd(nz.getPlatnostOd());
        nzDetail.setPlatnostDo(nz.getPlatnostDo());
        nzDetail.setSadzba(nz.getSadzba());
        nzDetail.setUzivatel(nz.getUzivatel());
        
        nzDetail.setPocetRokov(new Long(nz.getPlatnostDo().getYear() - nz.getPlatnostOd().getYear() + 1));
        nzDetail.setSucetVymier(nzDao.getNajomnaZmluvaSucetVymier(NajomnaZmluvaConverter.najomnaZmluvaDtoToNajomnaZmluva(nz)));
        if (nzDetail.getPocetRokov() != null && nzDetail.getSucetVymier() != null) {
            nzDetail.setVyplatenaCiastka(nzDetail.getSucetVymier().multiply(nzDetail.getSadzba()));
        }
        
        nzDetail.setVlastnik(VlastnikConverter.vlastnikToVlastnikDto(vlastnikDao.getVlastnikByNajomnaZmluva(nzDetail.getId())));
        
        List<ParcelaDto> parcelyDto = new ArrayList<>();
        for (Parcela parcela : parcelaDao.getAllParcelaByNajomnaZmluvaId(nzDetail.getId())) {
            parcelyDto.add(ParcelaConverter.parcelaToParcelaDto(parcela));
        }
        nzDetail.setParcely(parcelyDto);
        
        List<PodielDto> podielyDto = new ArrayList<>();
        for (Podiel podiel : podielDao.getAllPodielByNajomnaZmluvaId(nzDetail.getId())){
            podielyDto.add(PodielConverter.podielToPodielDto(podiel));
        }
        nzDetail.setPodiely(podielyDto);
        
        return nzDetail;
    }
    
}
