/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.serviceimpl;

import fbm.vutbr.martinsakac.bakalarka.dao.NajomnaZmluvaDao;
import fbm.vutbr.martinsakac.bakalarka.dao.ParcelaDao;
import fbm.vutbr.martinsakac.bakalarka.dao.VlastnikDao;
import fbm.vutbr.martinsakac.bakalarka.dto.ParcelaDto;
import fbm.vutbr.martinsakac.bakalarka.dto.VlastnikDto;
import fbm.vutbr.martinsakac.bakalarka.dto.upravene.NajomnaZmluvaUpravenaDto;
import fbm.vutbr.martinsakac.bakalarka.dto.upravene.VlastnikSoZmluvamiDto;
import fbm.vutbr.martinsakac.bakalarka.entity.NajomnaZmluva;
import fbm.vutbr.martinsakac.bakalarka.entity.Parcela;
import fbm.vutbr.martinsakac.bakalarka.entity.Vlastnik;
import fbm.vutbr.martinsakac.bakalarka.service.VlastnikService;
import fbm.vutbr.martinsakac.bakalarka.utils.NajomnaZmluvaUpravenaConverter;
import fbm.vutbr.martinsakac.bakalarka.utils.ParcelaConverter;
import fbm.vutbr.martinsakac.bakalarka.utils.UzivatelConverter;
import fbm.vutbr.martinsakac.bakalarka.utils.VlastnikConverter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author martin
 */

@Service("vlastnikServiceImpl")
public class VlastnikServiceImpl implements VlastnikService{

    @Autowired
    private VlastnikDao vlastnikDao;
    
    @Autowired
    private NajomnaZmluvaDao nzDao; 
    
    @Autowired
    private ParcelaDao parcelaDao;

    public NajomnaZmluvaDao getNajomnaZmluvaDao() {
        return nzDao;
    }

    public void setNajomnaZmluvaDao(NajomnaZmluvaDao najomnaZmluvaDao) {
        this.nzDao = najomnaZmluvaDao;
    }
    
    public VlastnikDao getVlastnikDao(){
        return this.vlastnikDao;
    }
    
    public void setVlastnikDao(VlastnikDao vlastnikDao){
        this.vlastnikDao = vlastnikDao;
    }

    public void setParcelaDao(ParcelaDao parcelaDao) {
        this.parcelaDao = parcelaDao;
    }

    public ParcelaDao getParcelaDao() {
        return parcelaDao;
    }
    
    @Override
    @Transactional
    public void addVlastnik(VlastnikDto vlastnikDto) {
        Validate.isTrue(vlastnikDto != null, "Vlastnik in service class cannot be null!");
        Validate.isTrue(vlastnikDto.getId() == null, "Vlastnik ID must be null!");
        Vlastnik vlastnik = VlastnikConverter.vlastnikDtoToVlastnik(vlastnikDto);
        vlastnikDao.createVlastnik(vlastnik);
        vlastnikDto.setId(vlastnik.getId());
    }

    @Override
    @Transactional
    public void updateVlastnik(VlastnikDto vlastnikDto) {
        Validate.isTrue(vlastnikDto != null, "Vlastnik in service class cannot be null!");
        Validate.isTrue(vlastnikDto.getId() != null, "Vlastnik ID must not be null!");
        vlastnikDao.updateVlastnik(VlastnikConverter.vlastnikDtoToVlastnik(vlastnikDto));
    }

    @Override
    @Transactional
    public void deleteVlastnik(VlastnikDto vlastnikDto) {
        Validate.isTrue(vlastnikDto != null, "Vlastnik in service class cannot be null!");
        Validate.isTrue(vlastnikDto.getId() != null, "Vlastnik ID must not be null!");
        vlastnikDao.deleteVlastnik(VlastnikConverter.vlastnikDtoToVlastnik(vlastnikDto));
    }

    @Override
    @Transactional
    public VlastnikDto getVlastnikById(Long id) {
        Validate.isTrue(id != null, "Vlastnik ID in service class cannot be null!");
        return VlastnikConverter.vlastnikToVlastnikDto(vlastnikDao.getVlastnikById(id));
    }

    @Override
    @Transactional
    public List<VlastnikDto> getAllVlastnik(String column, String direction) {
        List<VlastnikDto> result = new ArrayList<>();
        for (Vlastnik vlastnik : vlastnikDao.getAllVlastnik(column, direction)){
            result.add(VlastnikConverter.vlastnikToVlastnikDto(vlastnik));
        }
        return result;
    }

    @Override
    @Transactional
    public List<VlastnikSoZmluvamiDto> getAllVlastnikSoZmluvami(String column, String direction) {
        List<VlastnikSoZmluvamiDto> result = new ArrayList<>();
        for (Vlastnik vlastnik : vlastnikDao.getAllVlastnik(column, direction)){
            VlastnikSoZmluvamiDto vl = new VlastnikSoZmluvamiDto();
            vl.setId(vlastnik.getId());
            vl.setMeno(vlastnik.getMeno());
            vl.setPriezvisko(vlastnik.getPriezvisko());
            vl.setTitul(vlastnik.getTitul());
            vl.setEvidencneCislo(vlastnik.getEvidencneCislo());
            vl.setDatumNarodenia(vlastnik.getDatumNarodenia());
            vl.setMiestoNarodenia(vlastnik.getMiestoNarodenia());
            vl.setAdresa(vlastnik.getAdresa());
            vl.setCisloUctu(vlastnik.getCisloUctu());
            vl.setTelCislo(vlastnik.getTelCislo());
            vl.setIco(vlastnik.getIco());
            // nastavenie vlastnikovych najomnych zmluv
            List<NajomnaZmluvaUpravenaDto> nzUpravena = new ArrayList<>();
            for (NajomnaZmluva nz : nzDao.getNajomnaZmluvaByVlastnikId(vlastnik.getId())){
                NajomnaZmluvaUpravenaDto nzu = new NajomnaZmluvaUpravenaDto();
                nzu.setId(nz.getId());
                nzu.setIdVlastnika(vlastnik.getId());
                nzu.setPlatnostDo(nz.getPlatnostDo());
                nzu.setPlatnostOd(nz.getPlatnostOd());
                nzu.setDatumZhotovenia(nz.getDatumZhotovenia());
                nzu.setSadzba(nz.getSadzba());
                if (nz.getUzivatel() != null){
                   nzu.setUzivatel(UzivatelConverter.uzivatelToUzivatelDto(nz.getUzivatel()));
                }
                if (nz.getPlatnostDo() != null && nz.getPlatnostOd() != null){
                    nzu.setPocetRokov(new Long(nz.getPlatnostDo().getYear() - nz.getPlatnostOd().getYear() + 1));
                }
                BigDecimal sucetVymier = nzDao.getNajomnaZmluvaSucetVymier(nz);
                nzu.setSucetVymier(sucetVymier);
                if (sucetVymier != null && nzu.getSadzba() != null){
                    nzu.setVyplatenaCiastka(sucetVymier.multiply(nzu.getSadzba()));
                }
                nzUpravena.add(nzu);
            }
            vl.setListZmluv(nzUpravena);
            // nastavenie vlastnikovych parciel
            List<ParcelaDto> listParciel = new ArrayList<>();
            for (Parcela parcela : parcelaDao.getAllParcelaByVlastnikId(vlastnik.getId())){
                listParciel.add(ParcelaConverter.parcelaToParcelaDto(parcela));
            }
            vl.setListParciel(listParciel);
            
            result.add(vl);
        }
        return result;
    }
    
}
