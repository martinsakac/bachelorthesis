/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.utils;

import fbm.vutbr.martinsakac.bakalarka.dto.KrajDto;
import fbm.vutbr.martinsakac.bakalarka.entity.Kraj;

/**
 *
 * @author martin
 */
public class KrajConverter {
    public static Kraj krajDtoToKraj(KrajDto krajDto){
        Kraj kraj = new Kraj();
        kraj.setId(krajDto.getId());
        kraj.setNazov(krajDto.getNazov());
        return kraj;
    }
    
    public static KrajDto krajToKrajDto(Kraj kraj){
        KrajDto krajDto = new KrajDto();
        krajDto.setId(kraj.getId());
        krajDto.setNazov(kraj.getNazov());
        return krajDto;
    }
}
