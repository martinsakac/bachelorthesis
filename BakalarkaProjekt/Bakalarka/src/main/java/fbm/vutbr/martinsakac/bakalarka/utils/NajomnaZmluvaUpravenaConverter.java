/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.utils;

import fbm.vutbr.martinsakac.bakalarka.dto.upravene.NajomnaZmluvaUpravenaDto;
import fbm.vutbr.martinsakac.bakalarka.entity.NajomnaZmluva;

/**
 *
 * @author martin
 */
public class NajomnaZmluvaUpravenaConverter {
    public static NajomnaZmluvaUpravenaDto convertToDto(NajomnaZmluva nz, Long vlastnikId){
        NajomnaZmluvaUpravenaDto nzu = new NajomnaZmluvaUpravenaDto();
        nzu.setId(nz.getId());
        nzu.setIdVlastnika(vlastnikId);
        nzu.setPlatnostDo(nz.getPlatnostDo());
        nzu.setPlatnostOd(nz.getPlatnostOd());
        nzu.setDatumZhotovenia(nz.getDatumZhotovenia());
        nzu.setSadzba(nz.getSadzba());
        if (nz.getUzivatel() != null){
           nzu.setUzivatel(UzivatelConverter.uzivatelToUzivatelDto(nz.getUzivatel()));
        }
        if (nz.getPlatnostDo() != null && nz.getPlatnostOd() != null){
            nzu.setPocetRokov(new Long(nz.getPlatnostDo().getYear() - nz.getPlatnostOd().getYear() + 1));
        }
        //nzu.setVyplatenaCiastka(nzDao.getNajomnaZmluvaSucetVymier(nz).multiply(nzu.getSadzba()).setScale(2));
        return nzu;
    }
}
