/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.entity;

import fbm.vutbr.martinsakac.bakalarka.dto.UzivatelDto.Rola;
import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author martin
 */
@Entity
public class Uzivatel implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    
    private String meno;
    
    private String priezvisko;
    
    private String pracovneZaradenie;
    
    private String uzivatelskeMeno;
    
    //mal by to byt len hash, Hashovanie na urovni prezentacnej vrstvy
    private String uzivatelskeHeslo;
    
    private Rola uzivatelskaRola;
    
    @ManyToOne(cascade = CascadeType.ALL)        
    private Spolocnost spolocnost;

    public Uzivatel(String meno, String priezvisko, String pracovneZaradenie, String uzivatelskeMeno, String uzivatelskeHeslo, Spolocnost spolocnost) {
        this.meno = meno;
        this.priezvisko = priezvisko;
        this.pracovneZaradenie = pracovneZaradenie;
        this.uzivatelskeMeno = uzivatelskeMeno;
        this.uzivatelskeHeslo = uzivatelskeHeslo;
        this.spolocnost = spolocnost;
    }

    public Uzivatel(Long id, String meno, String priezvisko, String pracovneZaradenie, String uzivatelskeMeno, String uzivatelskeHeslo, Spolocnost spolocnost) {
        this.id = id;
        this.meno = meno;
        this.priezvisko = priezvisko;
        this.pracovneZaradenie = pracovneZaradenie;
        this.uzivatelskeMeno = uzivatelskeMeno;
        this.uzivatelskeHeslo = uzivatelskeHeslo;
        this.spolocnost = spolocnost;
    }

    public Uzivatel() {
    }

    public void setUzivatelskaRola(Rola uzivatelskaRola) {
        this.uzivatelskaRola = uzivatelskaRola;
    }

    public Rola getUzivatelskaRola() {
        return uzivatelskaRola;
    }

    public void setUzivatelskeHeslo(String uzivatelskeHeslo) {
        this.uzivatelskeHeslo = uzivatelskeHeslo;
    }

    public String getUzivatelskeHeslo() {
        return uzivatelskeHeslo;
    }

    public void setMeno(String meno) {
        this.meno = meno;
    }

    public void setPriezvisko(String priezvisko) {
        this.priezvisko = priezvisko;
    }

    public void setPracovneZaradenie(String pracovneZaradenie) {
        this.pracovneZaradenie = pracovneZaradenie;
    }

    public void setUzivatelskeMeno(String uzivatelskeMeno) {
        this.uzivatelskeMeno = uzivatelskeMeno;
    }

    public void setSpolocnost(Spolocnost spolocnost) {
        this.spolocnost = spolocnost;
    }

    public String getMeno() {
        return meno;
    }

    public String getPriezvisko() {
        return priezvisko;
    }

    public String getPracovneZaradenie() {
        return pracovneZaradenie;
    }

    public String getUzivatelskeMeno() {
        return uzivatelskeMeno;
    }

    public Spolocnost getSpolocnost() {
        return spolocnost;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Uzivatel)) {
            return false;
        }
        Uzivatel other = (Uzivatel) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fbm.vutbr.martinsakac.bakalarka.entity.Uzivatel[ id=" + id + " ]";
    }
    
}
