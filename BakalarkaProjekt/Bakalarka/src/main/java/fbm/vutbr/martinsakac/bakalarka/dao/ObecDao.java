/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.dao;

import fbm.vutbr.martinsakac.bakalarka.entity.Obec;
import java.util.List;

/**
 *
 * @author martin
 */
public interface ObecDao {
    public void createObec(Obec obec);
    public void updateObec(Obec obec);
    public void deleteObec(Obec obec);
    public Obec getObecById(Long id);
    public List<Obec> getAllObec();
}
