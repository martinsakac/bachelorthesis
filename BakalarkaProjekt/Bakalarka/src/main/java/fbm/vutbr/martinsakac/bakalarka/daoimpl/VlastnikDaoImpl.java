/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.daoimpl;

import fbm.vutbr.martinsakac.bakalarka.dao.PodielDao;
import fbm.vutbr.martinsakac.bakalarka.dao.VlastnikDao;
import fbm.vutbr.martinsakac.bakalarka.entity.Podiel;
import fbm.vutbr.martinsakac.bakalarka.entity.Vlastnik;
import java.util.Collections;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author martin
 */

@Repository
public class VlastnikDaoImpl implements VlastnikDao{
    
    @PersistenceContext
    private EntityManager em;

    public EntityManager getEntityManager(){
        return this.em;
    }
    
    public void setEntityManager(EntityManager em){
        this.em = em;
    }

    @Override
    public void createVlastnik(Vlastnik vlastnik) {
        Validate.isTrue(vlastnik != null, "Vlastnik cannot be null!");
        Validate.isTrue(vlastnik.getId() == null, "Vlastnik ID must be null!");
        em.persist(vlastnik);
    }

    @Override
    public void updateVlastnik(Vlastnik vlastnik) {
        Validate.isTrue(vlastnik != null, "Vlastnik cannot be null!");
        Validate.isTrue(vlastnik.getId() != null, "Vlastnik ID must not be null!");
        em.merge(vlastnik);
    }

    @Override
    public void deleteVlastnik(Vlastnik vlastnik) {
        Validate.isTrue(vlastnik != null, "Vlastnik cannot be null!");
        Validate.isTrue(vlastnik.getId() != null, "Vlastnik ID must not be null!");
        vlastnik = em.merge(vlastnik);
        if (vlastnik == null){
            throw new IllegalArgumentException("Vlastnik cannot be removed. He is not in DB.");
        }
        
        final PodielDaoImpl podielDao = new PodielDaoImpl();
        podielDao.setEntityManager(em);
        
        for (Podiel podiel : podielDao.getPodielByOwner(vlastnik)){
            podiel.setVlastnik(null);
            em.merge(podiel);
        }
        em.remove(vlastnik);
    }

    @Override
    public Vlastnik getVlastnikById(Long id) {
        Validate.isTrue(id != null, "ID cannot be null!");
        return em.find(Vlastnik.class, id);
    }

    @Override
    public List<Vlastnik> getAllVlastnik(String column, String direction) {
        TypedQuery<Vlastnik> query = null;
        
        if (column.equals("datumNarodenia")) {
            query = em.createQuery("select v from Vlastnik v order by datumNarodenia", Vlastnik.class);
        }
        else {
            query = em.createQuery("select v from Vlastnik v order by priezvisko", Vlastnik.class);
        }
        List<Vlastnik> result = query.getResultList();
        if (direction.equals("desc")) {
            Collections.reverse(result);
        }
        return result;
    }
    
    @Override
    public List<Vlastnik> getAllVlastnikOrdered(String orderingBy){
        // NEDOKONCENE zoradenie !!!! JPQL syntax
        //
        TypedQuery<Vlastnik> query = em.createQuery("select v from Vlastnik v", Vlastnik.class);
        return query.getResultList();
    }

    @Override
    public Vlastnik getVlastnikByNajomnaZmluva(Long idNajomnaZmluva) {
        TypedQuery<Vlastnik> query = em.createQuery(
                "SELECT v FROM NajomnaZmluva nz, PodielNajomnaZmluva pnz, Podiel p, "
                + "Vlastnik v "
                + "WHERE nz = pnz.najomnaZmluva and pnz.podiel = p and p.vlastnik = v "
                + "and nz.id = :idNajomnaZmluva"
                , Vlastnik.class).setParameter("idNajomnaZmluva", idNajomnaZmluva);
        return query.getSingleResult();
    }

}
