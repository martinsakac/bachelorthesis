/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.dao;

import fbm.vutbr.martinsakac.bakalarka.entity.NajomnaZmluva;
import fbm.vutbr.martinsakac.bakalarka.entity.Vlastnik;
import java.util.List;
import java.math.BigDecimal;
/**
 *
 * @author martin
 */
public interface NajomnaZmluvaDao {
    public void createNajomnaZmluva(NajomnaZmluva nz);
    public void updateNajomnaZmluva(NajomnaZmluva nz);
    public void deleteNajomnaZmluva(NajomnaZmluva nz);
    public NajomnaZmluva getNajomnaZmluvaById(Long id);
    public Vlastnik getVlastnikByNajomnaZmluva(Long idNz);
    public List<NajomnaZmluva> getAllNajomnaZmluva(String column, String direction);
    public List<NajomnaZmluva> getNajomnaZmluvaByVlastnikId(Long vlastnikId);
    public BigDecimal getNajomnaZmluvaSucetVymier(NajomnaZmluva nz);
}
