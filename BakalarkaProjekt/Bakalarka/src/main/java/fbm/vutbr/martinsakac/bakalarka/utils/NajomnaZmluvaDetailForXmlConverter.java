/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fbm.vutbr.martinsakac.bakalarka.utils;

import fbm.vutbr.martinsakac.bakalarka.dto.ParcelaDto;
import fbm.vutbr.martinsakac.bakalarka.dto.upravene.NajomnaZmluvaDetailDto;
import fbm.vutbr.martinsakac.bakalarka.dto.upravene.NajomnaZmluvaDetailForXmlDto;
import java.util.HashMap;

/**
 * 
 * @author martin sakac
 */
public class NajomnaZmluvaDetailForXmlConverter {

    public static NajomnaZmluvaDetailForXmlDto forXml(NajomnaZmluvaDetailDto detail) {
        NajomnaZmluvaDetailForXmlDto forXml = new NajomnaZmluvaDetailForXmlDto();
        
        if (detail.getDatumZhotovenia() != null) {
            forXml.setDatumZhotoveniaDen
                (String.valueOf(detail.getDatumZhotovenia().getDayOfMonth()));
            forXml.setDatumZhotoveniaMesiac
                (String.valueOf(detail.getDatumZhotovenia().getMonthOfYear()));
            forXml.setDatumZhotoveniaRok
                (String.valueOf(detail.getDatumZhotovenia().getYear()));
        }
        
        if (detail.getPlatnostOd() != null) {
            forXml.setPlatnostOdDen
                (String.valueOf(detail.getPlatnostOd().getDayOfMonth()));
            forXml.setPlatnostOdMesiac
                (String.valueOf(detail.getPlatnostOd().getMonthOfYear()));
            forXml.setPlatnostOdRok
                (String.valueOf(detail.getPlatnostOd().getYear()));
        }
        
        if (detail.getPlatnostDo() != null) {
            forXml.setPlatnostDoDen
                (String.valueOf(detail.getPlatnostDo().getDayOfMonth()));
            forXml.setPlatnostDoMesiac
                (String.valueOf(detail.getPlatnostDo().getMonthOfYear()));
            forXml.setPlatnostDoRok
                (String.valueOf(detail.getPlatnostDo().getYear()));
        }
        
        if (detail.getSadzba() != null) {
            forXml.setSadzba(detail.getSadzba().toString());
        }
        
        if (detail.getUzivatel() != null) {
            forXml.setUzivatelMeno(detail.getUzivatel().getMeno());
            forXml.setUzivatelPriezvisko(detail.getUzivatel().getPriezvisko());
        }
        
        if (detail.getPocetRokov() != null) {
            forXml.setPocetRokov(detail.getPocetRokov());
        }
        
        if (detail.getSucetVymier() != null) {
            forXml.setSucetVymier(detail.getSucetVymier().toString());
        }
        
        if (detail.getVyplatenaCiastka() != null ) {
            forXml.setVyplatenaCiastka(detail.getVyplatenaCiastka().toString());
        }
        
        if (detail.getVlastnik() != null) {
            forXml.setVlastnikMeno(detail.getVlastnik().getMeno());
            forXml.setVlastnikPriezvisko(detail.getVlastnik().getPriezvisko());
        }
        
        if (detail.getParcely() != null) {
            forXml.setUzemiaRozloha(new HashMap<String, String>());
            for (ParcelaDto parcela : detail.getParcely()) {
                forXml.getUzemiaRozloha().put(
                        parcela.getKatastralneUzemie().getNazov(), 
                        parcela.getVymera().toString()
                );        
            }
        }
        
        return forXml;
    }
    
}
