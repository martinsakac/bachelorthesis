/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.dao;

import fbm.vutbr.martinsakac.bakalarka.entity.Podiel;
import fbm.vutbr.martinsakac.bakalarka.entity.Vlastnik;
import java.util.List;

/**
 *
 * @author martin
 */
public interface PodielDao {
    public void createPodiel(Podiel podiel);
    public void updatePodiel(Podiel podiel);
    public void deletePodiel(Podiel podiel);
    public Podiel getPodielById(Long id);
    public List<Podiel> getAllPodiel();
    public List<Podiel> getPodielByOwner(Vlastnik owner);
    public List<Podiel> getAllPodielByNajomnaZmluvaId(Long idNajomnaZmluva);
}
