/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.serviceimpl;

import fbm.vutbr.martinsakac.bakalarka.dao.KrajDao;
import fbm.vutbr.martinsakac.bakalarka.dto.KrajDto;
import fbm.vutbr.martinsakac.bakalarka.entity.Kraj;
import fbm.vutbr.martinsakac.bakalarka.service.KrajService;
import fbm.vutbr.martinsakac.bakalarka.utils.KrajConverter;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author martin
 */

@Service
public class KrajServiceImpl implements KrajService{

    @Autowired 
    private KrajDao krajDao;

    public void setKrajDao(KrajDao krajDao) {
        this.krajDao = krajDao;
    }

    public KrajDao getKrajDao() {
        return krajDao;
    }
    
    @Override
    @Transactional
    public void addKraj(KrajDto krajDto) {
        Validate.isTrue(krajDto != null, "KrajDto must not be null to be persisted.");
        Validate.isTrue(krajDto.getId() == null, "KrajDto ID must be null.");
        Kraj kraj = KrajConverter.krajDtoToKraj(krajDto);
        krajDao.createKraj(kraj);
        krajDto.setId(kraj.getId());
    }

    @Override
    @Transactional
    public void updateKraj(KrajDto krajDto) {
        Validate.isTrue(krajDto != null, "KrajDto must not be null to be updated.");
        Validate.isTrue(krajDto.getId() != null, "KrajDto ID must not be null.");
        krajDao.updateKraj(KrajConverter.krajDtoToKraj(krajDto));
    }

    @Override
    @Transactional
    public void deleteKraj(KrajDto krajDto) {
        Validate.isTrue(krajDto != null, "KrajDto must not be null to be deleted.");
        Validate.isTrue(krajDto.getId() != null, "KrajDto ID must not be null.");
        krajDao.deleteKraj(KrajConverter.krajDtoToKraj(krajDto));
    }

    @Override
    @Transactional
    public KrajDto getKrajById(Long id) {
        Validate.isTrue(id != null, "ID must not be null.");
        return KrajConverter.krajToKrajDto(krajDao.getKrajById(id));
    }

    @Override
    @Transactional
    public List<KrajDto> getAllKraj() {
        List<KrajDto> result = new ArrayList<>();
        for (Kraj kraj : krajDao.getAllKraj()){
            result.add(KrajConverter.krajToKrajDto(kraj));
        }
        return result;
    }
    
}
