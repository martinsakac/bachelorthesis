/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.serviceimpl;

import fbm.vutbr.martinsakac.bakalarka.dao.ObecDao;
import fbm.vutbr.martinsakac.bakalarka.dto.ObecDto;
import fbm.vutbr.martinsakac.bakalarka.entity.Obec;
import fbm.vutbr.martinsakac.bakalarka.service.ObecService;
import fbm.vutbr.martinsakac.bakalarka.utils.ObecConverter;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author martin
 */

@Service("obecServiceImpl")
public class ObecServiceImpl implements ObecService{

    @Autowired
    private ObecDao obecDao;

    public ObecDao getObecDao() {
        return obecDao;
    }

    public void setObecDao(ObecDao obecDao) {
        this.obecDao = obecDao;
    }
    
    @Override
    @Transactional
    public void addObec(ObecDto obecDto) {
        Validate.isTrue(obecDto != null, "ObecDto must not be null.");
        Validate.isTrue(obecDto.getId() == null, "Obec ID must be null to be persisted.");
        Obec obec = ObecConverter.obecDtoToObec(obecDto);
        obecDao.createObec(obec);
        obecDto.setId(obec.getId());
    }

    @Override
    @Transactional
    public void updateObec(ObecDto obecDto) {
        Validate.isTrue(obecDto != null, "ObecDto must not be null.");
        Validate.isTrue(obecDto.getId() != null, "Obec ID must not be null.");
        obecDao.updateObec(ObecConverter.obecDtoToObec(obecDto));
    }

    @Override
    @Transactional
    public void deleteObec(ObecDto obecDto) {
        Validate.isTrue(obecDto != null, "ObecDto must not be null.");
        Validate.isTrue(obecDto.getId() != null, "Obec ID must not be null.");
        obecDao.deleteObec(ObecConverter.obecDtoToObec(obecDto));
    }

    @Override
    @Transactional
    public ObecDto getObecById(Long id) {
        Validate.isTrue(id != null, "Obec ID must not be null.");
        return ObecConverter.obecToObecDto(obecDao.getObecById(id));
    }

    @Override
    @Transactional
    public List<ObecDto> getAllObec() {
        List<ObecDto> result = new ArrayList<>();
        for (Obec obec : obecDao.getAllObec()){
            result.add(ObecConverter.obecToObecDto(obec));
        }
        return result;
    }
    
}
