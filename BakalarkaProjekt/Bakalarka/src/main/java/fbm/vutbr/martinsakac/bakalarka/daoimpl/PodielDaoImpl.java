/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.daoimpl;

import fbm.vutbr.martinsakac.bakalarka.dao.PodielDao;
import fbm.vutbr.martinsakac.bakalarka.entity.NajomnaZmluva;
import fbm.vutbr.martinsakac.bakalarka.entity.Podiel;
import fbm.vutbr.martinsakac.bakalarka.entity.Vlastnik;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author martin
 */

@Repository
public class PodielDaoImpl implements PodielDao{
    
    @PersistenceContext
    private EntityManager em;
    
    public EntityManager getEntityManager(){
        return this.em;
    }
    
    public void setEntityManager(EntityManager em){
        this.em = em;
    }

    @Override
    public void createPodiel(Podiel podiel) {
        Validate.isTrue(podiel != null, "Podiel cannot be null.");
        Validate.isTrue(podiel.getId() == null, "ID must be null.");
        em.merge(podiel);
    }

    @Override
    public void updatePodiel(Podiel podiel) {
        Validate.isTrue(podiel != null, "Podiel cannot be null.");
        Validate.isTrue(podiel.getId() != null, "ID must not be null.");
        em.merge(podiel);
    }

    @Override
    public void deletePodiel(Podiel podiel) {
        Validate.isTrue(podiel != null, "Podiel cannot be null.");
        Validate.isTrue(podiel.getId() != null, "ID must not be null.");
        em.remove(podiel);
    }

    @Override
    public Podiel getPodielById(Long id) {
        Validate.isTrue(id != null, "ID must not be null.");
        return em.find(Podiel.class, id);
    }
    
    @Override
    public List<Podiel> getPodielByOwner(Vlastnik owner){
        Validate.isTrue(owner != null, "Owner must not be null!");
        Validate.isTrue(owner.getId() != null, "Owner must be stored in DB, no ID.");
        TypedQuery<Podiel> query = em.createQuery("select p from Podiel p where p.vlastnik = :owner", Podiel.class)
                .setParameter("owner", owner);
        return query.getResultList();
    }

    @Override
    public List<Podiel> getAllPodiel() {
        TypedQuery<Podiel> query = em.createQuery("select p from Podiel p", Podiel.class);
        return query.getResultList();
    }

    @Override
    public List<Podiel> getAllPodielByNajomnaZmluvaId(Long idNajomnaZmluva) {
        TypedQuery<Podiel> query = em.createQuery(
                "SELECT p FROM NajomnaZmluva nz, PodielNajomnaZmluva pnz, Podiel p "
                + "WHERE nz.id = :idNajomnaZmluva", Podiel.class)
                .setParameter("idNajomnaZmluva", idNajomnaZmluva);
        return query.getResultList();
    }

    
}
