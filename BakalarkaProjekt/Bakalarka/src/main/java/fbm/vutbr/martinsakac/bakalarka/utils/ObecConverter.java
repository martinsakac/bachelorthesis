/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.utils;

import fbm.vutbr.martinsakac.bakalarka.dto.ObecDto;
import fbm.vutbr.martinsakac.bakalarka.entity.Obec;

/**
 *
 * @author martin
 */
public class ObecConverter {
    public static Obec obecDtoToObec(ObecDto obecDto){
        Obec obec = new Obec();
        obec.setId(obecDto.getId());
        obec.setNazov(obecDto.getNazov());
        obec.setOkres(OkresConverter.okresDtoToOkres(obecDto.getOkres()));
        return obec;
    }
    
    public static ObecDto obecToObecDto(Obec obec){
        ObecDto obecDto = new ObecDto();
        obecDto.setId(obec.getId());
        obecDto.setNazov(obec.getNazov());
        obecDto.setOkres(OkresConverter.okresToOkresDto(obec.getOkres()));
        return obecDto;
    }
}
