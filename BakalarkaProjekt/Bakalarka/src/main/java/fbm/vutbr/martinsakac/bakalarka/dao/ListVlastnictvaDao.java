/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.dao;

import fbm.vutbr.martinsakac.bakalarka.entity.ListVlastnictva;
import java.util.List;

/**
 *
 * @author martin
 */
public interface ListVlastnictvaDao {
    public void createListVlastnictva(ListVlastnictva lv);
    public void updateListVlastnictva(ListVlastnictva lv);
    public void deleteListVlastnictva(ListVlastnictva lv);
    public ListVlastnictva getListVlastnictvaById(Long id);
    public List<ListVlastnictva> getAllListVlastnictva();
}
