/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.serviceimpl;

import fbm.vutbr.martinsakac.bakalarka.dao.ListVlastnictvaDao;
import fbm.vutbr.martinsakac.bakalarka.dto.ListVlastnictvaDto;
import fbm.vutbr.martinsakac.bakalarka.entity.ListVlastnictva;
import fbm.vutbr.martinsakac.bakalarka.service.ListVlastnictvaService;
import fbm.vutbr.martinsakac.bakalarka.utils.ListVlastnictvaConverter;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author martin
 */

@Service("listVlastnictvaServiceImpl")
public class ListVlastnictvaServiceImpl implements ListVlastnictvaService{

    @Autowired
    private ListVlastnictvaDao lvDao;

    public void setLvDao(ListVlastnictvaDao lvDao) {
        this.lvDao = lvDao;
    }

    public ListVlastnictvaDao getLvDao() {
        return lvDao;
    }
    
    @Override
    @Transactional
    public void addListVlastnictva(ListVlastnictvaDto lvDto) {
        Validate.isTrue(lvDto != null, "List vlastnictva mus not be null.");
        Validate.isTrue(lvDto.getId() == null, "List vlastnictva ID must be null.");
        ListVlastnictva lv = ListVlastnictvaConverter.listVlastnictvaDtoToListVlastnictva(lvDto);
        lvDao.createListVlastnictva(lv);
        lvDto.setId(lv.getId());
    }

    @Override
    @Transactional
    public void updateListVlastnictva(ListVlastnictvaDto lvDto) {
        Validate.isTrue(lvDto != null, "List vlastnictva mus not be null.");
        Validate.isTrue(lvDto.getId() != null, "List vlastnictva ID must not be null.");
        lvDao.updateListVlastnictva(ListVlastnictvaConverter.listVlastnictvaDtoToListVlastnictva(lvDto));
    }

    @Override
    @Transactional
    public void deleteListVlastnictva(ListVlastnictvaDto lvDto) {
        Validate.isTrue(lvDto != null, "List vlastnictva mus not be null.");
        Validate.isTrue(lvDto.getId() != null, "List vlastnictva ID must not be null.");
        lvDao.deleteListVlastnictva(ListVlastnictvaConverter.listVlastnictvaDtoToListVlastnictva(lvDto));
    }

    @Override
    @Transactional
    public ListVlastnictvaDto getListVlastnictvaById(Long id) {
        Validate.isTrue(id != null, "List vlastnictva id must not be null.");
        return ListVlastnictvaConverter.listVlastnictvaToListVlastnictvaDto(lvDao.getListVlastnictvaById(id));
    }

    @Override
    @Transactional
    public List<ListVlastnictvaDto> getAllListVlastnictva() {
        List<ListVlastnictvaDto> result = new ArrayList<>();
        for (ListVlastnictva lv : lvDao.getAllListVlastnictva()){
            result.add(ListVlastnictvaConverter.listVlastnictvaToListVlastnictvaDto(lv));
        }
        return result;
    }
    
}
