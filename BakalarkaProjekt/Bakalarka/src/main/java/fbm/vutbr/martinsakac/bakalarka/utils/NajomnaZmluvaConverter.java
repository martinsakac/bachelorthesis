/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.utils;

import fbm.vutbr.martinsakac.bakalarka.dto.NajomnaZmluvaDto;
import fbm.vutbr.martinsakac.bakalarka.entity.NajomnaZmluva;
import org.joda.time.LocalDate;


/**
 *
 * @author martin
 */
public class NajomnaZmluvaConverter {
    public static NajomnaZmluva najomnaZmluvaDtoToNajomnaZmluva(NajomnaZmluvaDto nzDto){
        NajomnaZmluva nz = new NajomnaZmluva();
        nz.setId(nzDto.getId());
        nz.setDatumZhotovenia(nzDto.getDatumZhotovenia());
        nz.setPlatnostOd(nzDto.getPlatnostOd());
        nz.setPlatnostDo(nzDto.getPlatnostDo());
        nz.setSadzba(nzDto.getSadzba());
        
        if (nzDto.getUzivatel() != null){
            nz.setUzivatel(UzivatelConverter.uzivatelDtoToUzivatel(nzDto.getUzivatel()));
        }
        return nz;
    }
    
    public static NajomnaZmluvaDto najomnaZmluvaToNajomnaZmluvaDto(NajomnaZmluva nz){
        NajomnaZmluvaDto nzDto = new NajomnaZmluvaDto();
        nzDto.setId(nz.getId());
        nzDto.setDatumZhotovenia(nz.getDatumZhotovenia());
        nzDto.setPlatnostOd(nz.getPlatnostOd());
        nzDto.setPlatnostDo(nz.getPlatnostDo());
        nzDto.setSadzba(nz.getSadzba());
        
        if (nz.getUzivatel() != null){
            nzDto.setUzivatel(UzivatelConverter.uzivatelToUzivatelDto(nz.getUzivatel()));
        }
        LocalDate currentDate = new LocalDate();
        if (nz.getPlatnostDo() != null && nz.getPlatnostDo().isBefore(currentDate)) {
            nzDto.setCurrent(0);
        }
        else {
            nzDto.setCurrent(1);
        }
        nzDto.setHasValidFollower(0);
        return nzDto;
    }
}
