/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import org.joda.time.LocalDate;
import java.math.BigDecimal;
import javax.persistence.CascadeType;

/**
 *
 * @author martin
 */
@Entity
public class NajomnaZmluva implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    
    private LocalDate datumZhotovenia;
    
    private LocalDate platnostOd;
    
    private LocalDate platnostDo;
    
    // predtsavuje cenu najmu za 1 hektar na jeden rok
    private BigDecimal sadzba;

    @ManyToOne(cascade = CascadeType.ALL)
    private Uzivatel uzivatel;

    public NajomnaZmluva() {
    }

    public NajomnaZmluva(LocalDate datumZhotovenia, LocalDate platnostOd, LocalDate platnostDo, BigDecimal sadzba) {
        this.datumZhotovenia = datumZhotovenia;
        this.platnostOd = platnostOd;
        this.platnostDo = platnostDo;
        this.sadzba = sadzba;
    }

    public NajomnaZmluva(Long id, LocalDate datumZhotovenia, LocalDate platnostOd, LocalDate platnostDo, BigDecimal sadzba) {
        this.id = id;
        this.datumZhotovenia = datumZhotovenia;
        this.platnostOd = platnostOd;
        this.platnostDo = platnostDo;
        this.sadzba = sadzba;
    }
    
    public void setDatumZhotovenia(LocalDate datumZhotovenia) {
        this.datumZhotovenia = datumZhotovenia;
    }

    public void setPlatnostOd(LocalDate platnostOd) {
        this.platnostOd = platnostOd;
    }

    public void setPlatnostDo(LocalDate platnostDo) {
        this.platnostDo = platnostDo;
    }

    public void setSadzba(BigDecimal sadzba) {
        this.sadzba = sadzba;
    }

    public LocalDate getDatumZhotovenia() {
        return datumZhotovenia;
    }

    public LocalDate getPlatnostOd() {
        return platnostOd;
    }

    public LocalDate getPlatnostDo() {
        return platnostDo;
    }

    public BigDecimal getSadzba() {
        return sadzba;
    }

    public void setUzivatel(Uzivatel uzivatel) {
        this.uzivatel = uzivatel;
    }

    public Uzivatel getUzivatel() {
        return uzivatel;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NajomnaZmluva)) {
            return false;
        }
        NajomnaZmluva other = (NajomnaZmluva) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fbm.vutbr.martinsakac.bakalarka.entity.NajomnaZmluva[ id=" + id + " ]";
    }
    
}
