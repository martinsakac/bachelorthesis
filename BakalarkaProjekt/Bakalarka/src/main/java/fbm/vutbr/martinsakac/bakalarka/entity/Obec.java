/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author martin
 */
@Entity
public class Obec implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private String nazov;
    
    @ManyToOne
    private Okres okres;

    public Obec(String nazov, Okres okres) {
        this.nazov = nazov;
        this.okres = okres;
    }

    public Obec(Long id, String nazov, Okres okres) {
        this.id = id;
        this.nazov = nazov;
        this.okres = okres;
    }

    public Obec() {
    }

    public void setNazov(String nazov) {
        this.nazov = nazov;
    }

    public void setOkres(Okres okres) {
        this.okres = okres;
    }

    public String getNazov() {
        return nazov;
    }

    public Okres getOkres() {
        return okres;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Obec)) {
            return false;
        }
        Obec other = (Obec) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fbm.vutbr.martinsakac.bakalarka.entity.Obec[ id=" + id + " ]";
    }
    
}
