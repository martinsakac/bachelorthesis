/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.utils;

import fbm.vutbr.martinsakac.bakalarka.dto.VlastnikDto;
import fbm.vutbr.martinsakac.bakalarka.entity.Vlastnik;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author martin
 */
public class VlastnikConverter {
    
    public static Vlastnik vlastnikDtoToVlastnik(VlastnikDto vlastnikDto){
        Validate.isTrue(vlastnikDto != null, "VlastnikDto in converter cannot be null!");
        Vlastnik vlastnik = new Vlastnik();
        vlastnik.setId(vlastnikDto.getId());
        vlastnik.setTitul(vlastnikDto.getTitul());
        vlastnik.setMeno(vlastnikDto.getMeno());
        vlastnik.setPriezvisko(vlastnikDto.getPriezvisko());
        vlastnik.setEvidencneCislo(vlastnikDto.getEvidencneCislo());
        vlastnik.setAdresa(vlastnikDto.getAdresa());
        vlastnik.setDatumNarodenia(vlastnikDto.getDatumNarodenia());
        vlastnik.setMiestoNarodenia(vlastnikDto.getMiestoNarodenia());
        vlastnik.setCisloUctu(vlastnikDto.getCisloUctu());
        vlastnik.setTelCislo(vlastnikDto.getTelCislo());
        vlastnik.setIco(vlastnikDto.getIco());
        return vlastnik;
    }
    
    public static VlastnikDto vlastnikToVlastnikDto(Vlastnik vlastnik){
        Validate.isTrue(vlastnik != null, "Vlastnik in converter cannot be null!");
        VlastnikDto vlastnikDto = new VlastnikDto();
        vlastnikDto.setId(vlastnik.getId());
        vlastnikDto.setTitul(vlastnik.getTitul());
        vlastnikDto.setMeno(vlastnik.getMeno());
        vlastnikDto.setPriezvisko(vlastnik.getPriezvisko());
        vlastnikDto.setEvidencneCislo(vlastnik.getEvidencneCislo());
        vlastnikDto.setDatumNarodenia(vlastnik.getDatumNarodenia());
        vlastnikDto.setMiestoNarodenia(vlastnik.getMiestoNarodenia());
        vlastnikDto.setAdresa(vlastnik.getAdresa());
        vlastnikDto.setCisloUctu(vlastnik.getCisloUctu());
        vlastnikDto.setTelCislo(vlastnik.getTelCislo());
        vlastnikDto.setIco(vlastnik.getIco());
        return vlastnikDto;
    }
}
