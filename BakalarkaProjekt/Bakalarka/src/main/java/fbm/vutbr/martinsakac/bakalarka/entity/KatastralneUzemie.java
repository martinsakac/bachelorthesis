/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author martin
 */
@Entity
public class KatastralneUzemie implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private String idCislo;
    
    private String nazov;
    
    @ManyToOne
    private Obec obec;

    public KatastralneUzemie(String idCislo, String nazov, Obec obec) {
        this.idCislo = idCislo;
        this.nazov = nazov;
        this.obec = obec;
    }

    public KatastralneUzemie(Long id, String idCislo, String nazov, Obec obec) {
        this.id = id;
        this.idCislo = idCislo;
        this.nazov = nazov;
        this.obec = obec;
    }

    public KatastralneUzemie() {
    }

    public void setIdCislo(String idCislo) {
        this.idCislo = idCislo;
    }

    public void setNazov(String nazov) {
        this.nazov = nazov;
    }

    public void setObec(Obec obec) {
        this.obec = obec;
    }

    public String getIdCislo() {
        return idCislo;
    }

    public String getNazov() {
        return nazov;
    }

    public Obec getObec() {
        return obec;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof KatastralneUzemie)) {
            return false;
        }
        KatastralneUzemie other = (KatastralneUzemie) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fbm.vutbr.martinsakac.bakalarka.entity.KatastralneUzemie[ id=" + id + " ]";
    }
    
}
