/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.serviceimpl;

import fbm.vutbr.martinsakac.bakalarka.dao.KatastralneUzemieDao;
import fbm.vutbr.martinsakac.bakalarka.dto.KatastralneUzemieDto;
import fbm.vutbr.martinsakac.bakalarka.entity.KatastralneUzemie;
import fbm.vutbr.martinsakac.bakalarka.service.KatastralneUzemieService;
import fbm.vutbr.martinsakac.bakalarka.utils.KatastralneUzemieConverter;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author martin
 */

@Service("katastralneUzemieServiceImpl")
public class KatastralneUzemieServiceImpl implements KatastralneUzemieService{

    @Autowired
    private KatastralneUzemieDao kuDao;

    public KatastralneUzemieDao getKuDao() {
        return kuDao;
    }

    public void setKuDao(KatastralneUzemieDao kuDao) {
        this.kuDao = kuDao;
    }
    
    @Override
    @Transactional
    public void addKatastralneUzemie(KatastralneUzemieDto kuDto) {
        Validate.isTrue(kuDto != null, "Katastralne uzemie cannot be null to be persisted.");
        Validate.isTrue(kuDto.getId() == null, "KU ID must be null.");
        KatastralneUzemie ku = KatastralneUzemieConverter.katastralneUzemieDtoToKatastralneUzemie(kuDto);
        kuDao.createKatastralneUzemie(ku);
        kuDto.setId(ku.getId());
    }

    @Override
    @Transactional
    public void updateKatastralneUzemie(KatastralneUzemieDto kuDto) {
        Validate.isTrue(kuDto != null, "Katastralne uzemie cannot be null.");
        Validate.isTrue(kuDto.getId() != null, "KU ID must not be null.");
        kuDao.updateKatastralneUzemie(KatastralneUzemieConverter.katastralneUzemieDtoToKatastralneUzemie(kuDto));
    }

    @Override
    @Transactional
    public void deleteKatastralneUzemie(KatastralneUzemieDto kuDto) {
        Validate.isTrue(kuDto != null, "Katastralne uzemie cannot be null.");
        Validate.isTrue(kuDto.getId() != null, "KU ID must not be null.");
        kuDao.deleteKatastralneUzemie(KatastralneUzemieConverter.katastralneUzemieDtoToKatastralneUzemie(kuDto));
    }

    @Override
    @Transactional
    public KatastralneUzemieDto getKatastralneUzemieById(Long id) {
        Validate.isTrue(id != null, "ID must not be null to search in DB.");
        return KatastralneUzemieConverter.katastralneUzemieToKatastralneUzemieDto(kuDao.getKatastralneUzemieById(id));
    }

    @Override
    @Transactional
    public List<KatastralneUzemieDto> getAllKatastralneUzemie() {
        List<KatastralneUzemieDto> list = new ArrayList<>();
        for (KatastralneUzemie ku : kuDao.getAllKatastralneUzemie()){
            list.add(KatastralneUzemieConverter.katastralneUzemieToKatastralneUzemieDto(ku));
        }
        return list;
    }
    
}
