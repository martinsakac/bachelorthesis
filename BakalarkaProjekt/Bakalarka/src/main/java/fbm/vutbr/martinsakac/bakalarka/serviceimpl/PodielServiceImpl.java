/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.serviceimpl;

import fbm.vutbr.martinsakac.bakalarka.dao.PodielDao;
import fbm.vutbr.martinsakac.bakalarka.dto.PodielDto;
import fbm.vutbr.martinsakac.bakalarka.entity.Podiel;
import fbm.vutbr.martinsakac.bakalarka.service.PodielService;
import fbm.vutbr.martinsakac.bakalarka.utils.PodielConverter;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author martin
 */

@Service("podielServiceImpl")
public class PodielServiceImpl implements PodielService{

    @Autowired
    private PodielDao podielDao;

    public void setPodielDao(PodielDao podielDao) {
        this.podielDao = podielDao;
    }

    public PodielDao getPodielDao() {
        return podielDao;
    }
    
    @Override
    @Transactional
    public void addPodiel(PodielDto podielDto) {
        Validate.isTrue(podielDto != null, "Podiel dto must not be null.");
        Validate.isTrue(podielDto.getId() == null, "Podiel ID must be null.");
        Podiel podiel = PodielConverter.podielDtoToPodiel(podielDto);
        podielDao.createPodiel(podiel);
        podielDto.setId(podiel.getId());
    }

    @Override
    @Transactional
    public void updatePodiel(PodielDto podielDto) {
        Validate.isTrue(podielDto != null, "Podiel dto must not be null.");
        Validate.isTrue(podielDto.getId() != null, "Podiel ID must not be null.");
        podielDao.updatePodiel(PodielConverter.podielDtoToPodiel(podielDto));
    }

    @Override
    @Transactional
    public void deletePodiel(PodielDto podielDto) {
        Validate.isTrue(podielDto != null, "Podiel dto must not be null.");
        Validate.isTrue(podielDto.getId() != null, "Podiel ID must not be null.");
        podielDao.deletePodiel(PodielConverter.podielDtoToPodiel(podielDto));
    }

    @Override
    @Transactional
    public PodielDto getPodielById(Long id) {
        Validate.isTrue(id != null, "Podiel ID must not be null to retrieve podiel.");
        return PodielConverter.podielToPodielDto(podielDao.getPodielById(id));
    }

    @Override
    @Transactional
    public List<PodielDto> getAllPodiel() {
        List<PodielDto> result = new ArrayList<>();
        for (Podiel podiel : podielDao.getAllPodiel()){
            result.add(PodielConverter.podielToPodielDto(podiel));
        }
        return result;
    }
    
}
