/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.utils;

import fbm.vutbr.martinsakac.bakalarka.dto.ParcelaDto;
import fbm.vutbr.martinsakac.bakalarka.entity.Parcela;

/**
 *
 * @author martin
 */
public class ParcelaConverter {
    public static Parcela parcelaDtoToParcela(ParcelaDto parcelaDto){
        Parcela parcela = new Parcela();
        parcela.setId(parcelaDto.getId());
        if (parcelaDto.getListVlastnictva() != null){
            parcela.setListVlastnictva(ListVlastnictvaConverter
                    .listVlastnictvaDtoToListVlastnictva(parcelaDto.getListVlastnictva()));
        }
        parcela.setParcelneCislo(parcelaDto.getParcelneCislo());
        parcela.setPrislusnostKzuo(parcelaDto.getPrislusnostKzuo());
        parcela.setSposobVyuzitiaPozemku(parcelaDto.getPrislusnostKzuo());
        if (parcelaDto.getKatastralneUzemie() != null){
            parcela.setKatastralneUzemie(KatastralneUzemieConverter.katastralneUzemieDtoToKatastralneUzemie(parcelaDto.getKatastralneUzemie()));
        }
        parcela.setTyp(parcelaDto.getTyp());
        parcela.setVymera(parcelaDto.getVymera());
        return parcela;
    }
    
    public static ParcelaDto parcelaToParcelaDto(Parcela parcela){
        ParcelaDto parcelaDto = new ParcelaDto();
        parcelaDto.setId(parcela.getId());
        if (parcela.getKatastralneUzemie() != null){
            parcelaDto.setKatastralneUzemie(KatastralneUzemieConverter
                    .katastralneUzemieToKatastralneUzemieDto(parcela.getKatastralneUzemie()));
        }
        if (parcela.getListVlastnictva() != null){
            parcelaDto.setListVlastnictva(ListVlastnictvaConverter.listVlastnictvaToListVlastnictvaDto(parcela.getListVlastnictva()));
        }
        parcelaDto.setParcelneCislo(parcela.getParcelneCislo());
        parcelaDto.setPrislusnostKzuo(parcela.getPrislusnostKzuo());
        parcelaDto.setSposobVyuzitiaPozemku(parcela.getSposobVyuzitiaPozemku());
        parcelaDto.setTyp(parcela.getTyp());
        parcelaDto.setVymera(parcela.getVymera());
        return parcelaDto;
    }
}
