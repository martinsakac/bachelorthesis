/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.dao;

import fbm.vutbr.martinsakac.bakalarka.entity.Spolocnost;
import java.util.List;

/**
 *
 * @author martin
 */
public interface SpolocnostDao {
    public void createSpolocnost(Spolocnost spol);
    public void updateSpolocnost(Spolocnost spol);
    public void deleteSpolocnost(Spolocnost spol);
    public Spolocnost getSpolocnostById(Long id);
    public List<Spolocnost> getAllSpolocnost();
}
