/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author martin
 */
@Entity
public class Okres implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private String nazov;
    
    @ManyToOne
    private Kraj kraj;

    public Okres() {
    }

    public Okres(String nazov, Kraj kraj) {
        this.nazov = nazov;
        this.kraj = kraj;
    }

    public Okres(Long id, String nazov, Kraj kraj) {
        this.id = id;
        this.nazov = nazov;
        this.kraj = kraj;
    }

    public void setKraj(Kraj kraj) {
        this.kraj = kraj;
    }

    public Kraj getKraj() {
        return kraj;
    }
    
    public String getNazov() {
        return nazov;
    }

    public void setNazov(String nazov) {
        this.nazov = nazov;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Okres)) {
            return false;
        }
        Okres other = (Okres) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fbm.vutbr.martinsakac.bakalarka.entity.oKRES[ id=" + id + " ]";
    }
    
}
