/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.dao;

import fbm.vutbr.martinsakac.bakalarka.entity.Parcela;
import java.util.List;

/**
 *
 * @author martin
 */
public interface ParcelaDao {
    public void createParcela(Parcela parcela);
    public void updateParcela(Parcela parcela);
    public void deleteParcela(Parcela parcela);
    public Parcela getParcelaById(Long id);
    public List<Parcela> getAllParcela();
    public List<Parcela> getAllParcelaByVlastnikId(Long idVlastnika);
    public List<Parcela> getAllParcelaByNajomnaZmluvaId(Long idNajomnaZmluva);
}
