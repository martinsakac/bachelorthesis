/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.daoimpl;

import fbm.vutbr.martinsakac.bakalarka.dao.ParcelaDao;
import fbm.vutbr.martinsakac.bakalarka.entity.Parcela;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author martin
 */

@Repository
public class ParcelaDaoImpl implements ParcelaDao{
    
    @PersistenceContext
    private EntityManager em;
    
    public EntityManager getEntityManager(){
        return this.em;
    }
    
    public void setEntityManager(EntityManager em){
        this.em = em;
    }

    @Override
    public void createParcela(Parcela parcela) {
        Validate.isTrue(parcela != null, "Parcela cannot be null.");
        Validate.isTrue(parcela.getId() == null, "PArcela ID must be null.");
        em.merge(parcela);
    }

    @Override
    public void updateParcela(Parcela parcela) {
        Validate.isTrue(parcela != null, "Parcela cannot be null.");
        Validate.isTrue(parcela.getId() != null, "PArcela ID must not be null.");
        em.merge(parcela);
    }

    @Override
    public void deleteParcela(Parcela parcela) {
        Validate.isTrue(parcela != null, "Parcela cannot be null.");
        Validate.isTrue(parcela.getId() != null, "PArcela ID must not be null.");
        parcela = em.merge(parcela);
        if (parcela == null){
            throw new IllegalArgumentException("Parcela is not in DB, cannot be deleted!");
        }
        em.remove(parcela);
    }

    @Override
    public Parcela getParcelaById(Long id) {
        Validate.isTrue(id != null, "ID must not be null.");
        return em.find(Parcela.class, id);
    }

    @Override
    public List<Parcela> getAllParcela() {
        TypedQuery<Parcela> query = em.createQuery("select p from Parcela p", Parcela.class);
        return query.getResultList();
    }

    @Override
    public List<Parcela> getAllParcelaByVlastnikId(Long idVlastnika) {
        TypedQuery<Parcela> query = em.createQuery("SELECT pa FROM "
                + "Vlastnik v, Podiel p, ListVlastnictva lv, Parcela pa "
                + "WHERE p.vlastnik = v and p.listVlastnictva = lv and "
                + "pa.listVlastnictva = lv and v.id = :idVlastnika", Parcela.class)
                .setParameter("idVlastnika", idVlastnika);
        return query.getResultList();
    }

    @Override
    public List<Parcela> getAllParcelaByNajomnaZmluvaId(Long idNajomnaZmluva) {
        TypedQuery<Parcela> query = em.createQuery(
                "SELECT pa FROM NajomnaZmluva nz, PodielNajomnaZmluva pnz, Podiel p, "
                        + "ListVlastnictva lv, Parcela pa "
                        + "WHERE nz = pnz.najomnaZmluva and pnz.podiel = p and "
                        + "p.listVlastnictva = lv and lv = pa.listVlastnictva and "
                        + "nz.id = :idNajomnaZmluva", Parcela.class)
                .setParameter("idNajomnaZmluva", idNajomnaZmluva);
        return query.getResultList();
    }
    
}
