/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.joda.time.LocalDate;

/**
 *
 * @author martin
 */
@Entity
public class Vlastnik implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private String meno;
    
    private String priezvisko;
    
    private String titul;
    
    private String evidencneCislo;
    
    private LocalDate datumNarodenia;
    
    private String miestoNarodenia;
    
    private String adresa;
    
    private String cisloUctu;
    
    private String telCislo;
    
    private String ico;
    
    public Vlastnik(){
        
    }
    
    public Vlastnik(String meno, String priezvisko, String titul, LocalDate datumNarodenia, 
            String miestoNarodenia, String adresa, String cisloUctu, String ico){
        this.meno = meno;
        this.priezvisko = priezvisko;
        this.titul = titul;
        this.datumNarodenia = datumNarodenia;
        this.miestoNarodenia = miestoNarodenia;
        this.adresa = adresa;
        this.cisloUctu = cisloUctu;
        this.ico = ico;
    }
    
    public Vlastnik(Long id, String meno, String priezvisko, String titul, LocalDate datumNarodenia, 
            String miestoNarodenia, String adresa, String cisloUctu, String ico){
        this.id = id;
        this.meno = meno;
        this.priezvisko = priezvisko;
        this.titul = titul;
        this.datumNarodenia = datumNarodenia;
        this.miestoNarodenia = miestoNarodenia;
        this.adresa = adresa;
        this.cisloUctu = cisloUctu;
        this.ico = ico;
    }
    
    public Vlastnik(Long id, String meno, String adresa, String cisloUctu, String ico){
        this.id = id;
        this.meno = meno;
        this.adresa = adresa;
        this.cisloUctu = cisloUctu;
        this.ico = ico;
    }
    
    public Vlastnik(String meno, String adresa, String cisloUctu, String ico){
        this.meno = meno;
        this.adresa = adresa;
        this.cisloUctu = cisloUctu;
        this.ico = ico;
    }

    public String getMeno() {
        return meno;
    }

    public String getPriezvisko() {
        return priezvisko;
    }

    public String getTitul() {
        return titul;
    }

    public LocalDate getDatumNarodenia() {
        return datumNarodenia;
    }

    public String getMiestoNarodenia() {
        return miestoNarodenia;
    }

    public String getAdresa() {
        return adresa;
    }

    public String getCisloUctu() {
        return cisloUctu;
    }

    public String getIco() {
        return ico;
    }

    public void setMeno(String meno) {
        this.meno = meno;
    }

    public void setPriezvisko(String priezvisko) {
        this.priezvisko = priezvisko;
    }

    public void setTitul(String titul) {
        this.titul = titul;
    }

    public void setDatumNarodenia(LocalDate datumNarodenia) {
        this.datumNarodenia = datumNarodenia;
    }

    public void setMiestoNarodenia(String miestoNarodenia) {
        this.miestoNarodenia = miestoNarodenia;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public void setCisloUctu(String cisloUctu) {
        this.cisloUctu = cisloUctu;
    }


    public void setIco(String ico) {
        this.ico = ico;
    }
   
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setEvidencneCislo(String evidencneCislo) {
        this.evidencneCislo = evidencneCislo;
    }

    public void setTelCislo(String telCislo) {
        this.telCislo = telCislo;
    }

    public String getEvidencneCislo() {
        return evidencneCislo;
    }

    public String getTelCislo() {
        return telCislo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vlastnik)) {
            return false;
        }
        Vlastnik other = (Vlastnik) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fbm.vutbr.martinsakac.bakalarka.entity.Vlastnik[ id=" + id + " ]";
    }
    
}
