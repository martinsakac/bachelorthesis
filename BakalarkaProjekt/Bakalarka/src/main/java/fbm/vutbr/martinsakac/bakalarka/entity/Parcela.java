/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.math.BigDecimal;
import javax.persistence.CascadeType;

/**
 *
 * @author martin
 */
@Entity
public class Parcela implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private String typ;
    
    private String parcelneCislo;
    
    private BigDecimal vymera;
    
    private String sposobVyuzitiaPozemku;
    
    private String prislusnostKzuo;
    
    @ManyToOne(cascade = CascadeType.ALL)
    private KatastralneUzemie katastralneUzemie;
    
    @ManyToOne(cascade = CascadeType.ALL)
    private ListVlastnictva listVlastnictva;

    public void setKatastralneUzemie(KatastralneUzemie katastralneUzemie) {
        this.katastralneUzemie = katastralneUzemie;
    }

    public KatastralneUzemie getKatastralneUzemie() {
        return katastralneUzemie;
    }

    public void setListVlastnictva(ListVlastnictva listVlastnictva) {
        this.listVlastnictva = listVlastnictva;
    }

    public ListVlastnictva getListVlastnictva() {
        return listVlastnictva;
    }

    public String getTyp() {
        return typ;
    }

    public String getParcelneCislo() {
        return parcelneCislo;
    }

    public BigDecimal getVymera() {
        return vymera;
    }

    public String getSposobVyuzitiaPozemku() {
        return sposobVyuzitiaPozemku;
    }

    public String getPrislusnostKzuo() {
        return prislusnostKzuo;
    }

    public void setTyp(String typ) {
        this.typ = typ;
    }

    public void setParcelneCislo(String parcelneCislo) {
        this.parcelneCislo = parcelneCislo;
    }

    public void setVymera(BigDecimal vymera) {
        this.vymera = vymera;
    }

    public void setSposobVyuzitiaPozemku(String sposobVyuzitiaPozemku) {
        this.sposobVyuzitiaPozemku = sposobVyuzitiaPozemku;
    }

    public void setPrislusnostKzuo(String prislusnostKzuo) {
        this.prislusnostKzuo = prislusnostKzuo;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Parcela)) {
            return false;
        }
        Parcela other = (Parcela) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fbm.vutbr.martinsakac.bakalarka.entity.Parcela[ id=" + id + " ]";
    }
    
}
