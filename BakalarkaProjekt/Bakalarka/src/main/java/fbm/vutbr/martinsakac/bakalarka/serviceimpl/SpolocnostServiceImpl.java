/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.serviceimpl;

import fbm.vutbr.martinsakac.bakalarka.dao.SpolocnostDao;
import fbm.vutbr.martinsakac.bakalarka.dto.SpolocnostDto;
import fbm.vutbr.martinsakac.bakalarka.entity.Spolocnost;
import fbm.vutbr.martinsakac.bakalarka.service.SpolocnostService;
import fbm.vutbr.martinsakac.bakalarka.utils.SpolocnostConverter;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author martin
 */

@Service
public class SpolocnostServiceImpl implements SpolocnostService{
    
    @Autowired
    private SpolocnostDao dao;

    public void setDao(SpolocnostDao dao) {
        this.dao = dao;
    }

    public SpolocnostDao getDao() {
        return dao;
    }

    @Override
    @Transactional
    public void addSpolocnost(SpolocnostDto spolocnostDto) {
        Validate.isTrue(spolocnostDto != null, "Spolocnost must not be null.");
        Validate.isTrue(spolocnostDto.getId() == null, "Spolocnost ID must be null.");
        Spolocnost spolocnost = SpolocnostConverter.spolocnostDtoToSpolocnost(spolocnostDto);
        dao.createSpolocnost(spolocnost);
        spolocnostDto.setId(spolocnost.getId());
    }

    @Override
    @Transactional
    public void updateSpolocnost(SpolocnostDto spolocnostDto) {
        Validate.isTrue(spolocnostDto != null, "Spolocnost must not be null.");
        Validate.isTrue(spolocnostDto.getId() != null, "Spolocnost ID must not be null.");
        dao.updateSpolocnost(SpolocnostConverter.spolocnostDtoToSpolocnost(spolocnostDto));
    }

    @Override
    @Transactional
    public void deleteSpolocnost(SpolocnostDto spolocnostDto) {
        Validate.isTrue(spolocnostDto != null, "Spolocnost must not be null.");
        Validate.isTrue(spolocnostDto.getId() != null, "Spolocnost ID must not be null.");
        dao.deleteSpolocnost(SpolocnostConverter.spolocnostDtoToSpolocnost(spolocnostDto));
    }

    @Override
    @Transactional
    public SpolocnostDto getSpolocnostById(Long id) {
        Validate.isTrue(id != null, "ID must not be null.");
        return SpolocnostConverter.spolocnostToSpolocnostDto(dao.getSpolocnostById(id));
    }

    @Override
    @Transactional
    public List<SpolocnostDto> getAllSpolocnost() {
        List<SpolocnostDto> result = new ArrayList<>();
        for (Spolocnost spolocnost : dao.getAllSpolocnost()){
            result.add(SpolocnostConverter.spolocnostToSpolocnostDto(spolocnost));
        }
        return result;
    }
    
}
