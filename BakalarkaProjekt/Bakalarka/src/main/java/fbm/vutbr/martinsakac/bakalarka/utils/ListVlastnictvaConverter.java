/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.utils;

import fbm.vutbr.martinsakac.bakalarka.dto.ListVlastnictvaDto;
import fbm.vutbr.martinsakac.bakalarka.entity.ListVlastnictva;

/**
 *
 * @author martin
 */
public class ListVlastnictvaConverter {
    public static ListVlastnictvaDto listVlastnictvaToListVlastnictvaDto(ListVlastnictva lv){
        ListVlastnictvaDto lvDto = new ListVlastnictvaDto();
        lvDto.setId(lv.getId());
        lvDto.setCisloListu(lv.getCisloListu());
        return lvDto;
    }
    
    public static ListVlastnictva listVlastnictvaDtoToListVlastnictva(ListVlastnictvaDto lvDto){
        ListVlastnictva lv = new ListVlastnictva();
        lv.setId(lvDto.getId());
        lv.setCisloListu(lvDto.getCisloListu());
        return lv;
    }
}
