/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.entity;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author martin
 */
@Entity
public class PodielNajomnaZmluva implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    
    @ManyToOne(cascade = CascadeType.ALL)
    private Podiel podiel;

    @ManyToOne(cascade = CascadeType.ALL)
    private NajomnaZmluva najomnaZmluva;

    public PodielNajomnaZmluva() {
    }

    public PodielNajomnaZmluva(Podiel podiel, NajomnaZmluva najomnaZmluva) {
        this.podiel = podiel;
        this.najomnaZmluva = najomnaZmluva;
    }

    public PodielNajomnaZmluva(Long id, Podiel podiel, NajomnaZmluva najomnaZmluva) {
        this.id = id;
        this.podiel = podiel;
        this.najomnaZmluva = najomnaZmluva;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setPodiel(Podiel podiel) {
        this.podiel = podiel;
    }

    public void setNajomnaZmluva(NajomnaZmluva najomnaZmluva) {
        this.najomnaZmluva = najomnaZmluva;
    }

    public Podiel getPodiel() {
        return podiel;
    }

    public NajomnaZmluva getNajomnaZmluva() {
        return najomnaZmluva;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PodielNajomnaZmluva)) {
            return false;
        }
        PodielNajomnaZmluva other = (PodielNajomnaZmluva) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fbm.vutbr.martinsakac.bakalarka.entity.podielNajomnaZmluva[ id=" + id + " ]";
    }
    
}
