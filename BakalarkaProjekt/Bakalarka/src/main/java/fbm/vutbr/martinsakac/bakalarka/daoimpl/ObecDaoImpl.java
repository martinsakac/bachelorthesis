/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.daoimpl;

import fbm.vutbr.martinsakac.bakalarka.dao.ObecDao;
import fbm.vutbr.martinsakac.bakalarka.entity.Obec;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author martin
 */
@Repository
public class ObecDaoImpl implements ObecDao{
    
    @PersistenceContext
    private EntityManager em;
    
    public EntityManager getEntityManager(){
        return this.em;
    }
    
    public void setEntityManager(EntityManager em){
        this.em = em;
    }

    @Override
    public void createObec(Obec obec) {
        Validate.isTrue(obec != null, "Obec cannot be null.");
        Validate.isTrue(obec.getId() == null, "Obec ID must be null.");
        em.persist(obec);
    }

    @Override
    public void updateObec(Obec obec) {
        Validate.isTrue(obec != null, "Obec cannot be null.");
        Validate.isTrue(obec.getId() != null, "Obec ID must not be null.");
        em.merge(obec);
    }

    @Override
    public void deleteObec(Obec obec) {
        Validate.isTrue(obec != null, "Obec cannot be null.");
        Validate.isTrue(obec.getId() != null, "Obec ID must not be null.");
        obec = em.merge(obec);
        if (obec == null){
            throw new IllegalArgumentException("Obec is not DB, cannot be deleted.");
        }
        em.remove(obec);
    }

    @Override
    public Obec getObecById(Long id) {
        Validate.isTrue(id != null, "ID must not be null.");
        return em.find(Obec.class, id);
    }

    @Override
    public List<Obec> getAllObec() {
        TypedQuery<Obec> query = em.createQuery("select o from Obec o", Obec.class);
        return query.getResultList();
    }
    
}
