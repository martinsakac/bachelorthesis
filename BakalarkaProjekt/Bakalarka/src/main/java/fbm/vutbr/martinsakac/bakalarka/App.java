package fbm.vutbr.martinsakac.bakalarka;

import fbm.vutbr.martinsakac.bakalarka.dao.PodielDao;
import fbm.vutbr.martinsakac.bakalarka.dto.ListVlastnictvaDto;
import fbm.vutbr.martinsakac.bakalarka.dto.NajomnaZmluvaDto;
import fbm.vutbr.martinsakac.bakalarka.dto.ParcelaDto;
import fbm.vutbr.martinsakac.bakalarka.dto.PodielDto;
import fbm.vutbr.martinsakac.bakalarka.dto.PodielNajomnaZmluvaDto;
import fbm.vutbr.martinsakac.bakalarka.dto.VlastnikDto;
import fbm.vutbr.martinsakac.bakalarka.entity.ListVlastnictva;
import fbm.vutbr.martinsakac.bakalarka.entity.NajomnaZmluva;
import fbm.vutbr.martinsakac.bakalarka.service.ListVlastnictvaService;
import fbm.vutbr.martinsakac.bakalarka.service.NajomnaZmluvaService;
import fbm.vutbr.martinsakac.bakalarka.service.ParcelaService;
import fbm.vutbr.martinsakac.bakalarka.service.PodielNajomnaZmluvaService;
import fbm.vutbr.martinsakac.bakalarka.service.PodielService;
import fbm.vutbr.martinsakac.bakalarka.service.VlastnikService;
import fbm.vutbr.martinsakac.bakalarka.serviceimpl.VlastnikServiceImpl;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.joda.time.LocalDate;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import java.math.BigDecimal;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {   
//        EntityManagerFactory emf = Persistence.createEntityManagerFactory("bakalarkaPU");
//        EntityManager em1 = emf.createEntityManager();
//        
//        ListVlastnictva list = new ListVlastnictva("111/11");
//        
//        em1.getTransaction().begin();
//        em1.persist(list);
//        em1.getTransaction().commit();
//        
//        System.out.println( "Hello World!" );
        
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        System.out.println("hahaha");
        System.out.println(context.getDisplayName());
        
        VlastnikService vlastnikService = (VlastnikService)context.getBean("vlastnikServiceImpl");
        PodielService podielService = (PodielService)context.getBean("podielServiceImpl");
        NajomnaZmluvaService nzService = (NajomnaZmluvaService)context.getBean("najomnaZmluvaServiceImpl");
        PodielNajomnaZmluvaService pnzService = (PodielNajomnaZmluvaService)context.getBean("podielNajomnaZmluvaServiceImpl");
        ListVlastnictvaService lvService = (ListVlastnictvaService)context.getBean("listVlastnictvaServiceImpl");
        ParcelaService parcelaService = (ParcelaService)context.getBean("parcelaServiceImpl");
        
        if (vlastnikService != null && podielService != null && nzService != null && 
                pnzService != null && lvService != null && parcelaService != null){
            System.out.println("Service je natiahnuta.");
        }
        else {
            System.out.println("Service nie je natiahnuta.");
            return;
        }
        
        
        VlastnikDto vlastnik = new VlastnikDto();
        vlastnik.setMeno("Martin");
        vlastnik.setPriezvisko("Sakac");
        NajomnaZmluvaDto nz = new NajomnaZmluvaDto();
        nz.setPlatnostOd(new LocalDate(2014,3,10));
        
        vlastnikService.addVlastnik(vlastnik);
        nzService.addNajomnaZmluva(nz);
        
        ListVlastnictvaDto lv = new ListVlastnictvaDto();
        lvService.addListVlastnictva(lv);
        
//        if (lv.getId() != null){
//            System.out.println("ID nie je null!");
//            return;
//        }
//        else {
//            System.out.println("ID je null!!!");
//            return;
//        }
        //lv = lvService.getListVlastnictvaById(lv.getId());
            
        PodielDto podiel = new PodielDto();
        podiel.setListVlastnictva(lv);
        podiel.setVlastnik(vlastnik);
        //podielService.addPodiel(podiel);
        
        PodielNajomnaZmluvaDto pnz = new PodielNajomnaZmluvaDto();
        pnz.setPodiel(podiel);
        pnz.setNajomnaZmluva(nz);
        pnzService.addPodielNajomnaZmluva(pnz);
        
        ParcelaDto parcela = new ParcelaDto();
        parcela.setListVlastnictva(lv);
        parcelaService.addParcela(parcela);
        
        
//        PodielDto podiel = new PodielDto();
//        podiel.setPercentualnyPodiel(55);
//        
//        NajomnaZmluvaDto nz = new NajomnaZmluvaDto();
//        nz.setSadzba(new BigDecimal(55.5D));
//        
//        PodielNajomnaZmluvaDto pnzDto = new PodielNajomnaZmluvaDto();
//        pnzDto.setNajomnaZmluva(nz);
//        pnzDto.setPodiel(podiel);
//        
//        podielService.addPodiel(podiel);
//        nzService.addNajomnaZmluva(nz);
//        pnzService.addPodielNajomnaZmluva(pnzDto);
//        
//        if (pnzDto.getId() != null){
//            System.out.println("Nie je tam null.");
//        }
//        else {
//            System.out.println("Je tam null!!!!");
//        }
//        
//        for (PodielNajomnaZmluvaDto pnzDto2 : pnzService.getAllPodielNajomnaZmluva()){
//            System.out.println(pnzDto.getId());
//        }
//        
        
        
//        PodielDto podiel2 = podielService.getPodielById(-21474833200L);
//        
//        if (podiel2.getNajomnaZmluva() != null){
//            System.out.println(podiel2.getNajomnaZmluva().size());
//        }
//        else {
//            System.out.println("list je null");
//        }
        
//        if (service.getVlastnikDao() != null){
//            System.out.println("Dao je injeknute.");
//        }
//        else {
//            System.out.println("Dao nie je injeknute");
//            return;
//        }
        
//        VlastnikDto vlastnikDto = new VlastnikDto();
//        vlastnikDto.setMeno("Martin");
//        vlastnikDto.setPriezvisko("Sakac");
//        vlastnikDto.setDatumNarodenia(new LocalDate(1991,4,29));
//        
//        service.addVlastnik(vlastnikDto);
    }
}
