/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.utils;

import fbm.vutbr.martinsakac.bakalarka.dto.PodielNajomnaZmluvaDto;
import fbm.vutbr.martinsakac.bakalarka.entity.PodielNajomnaZmluva;

/**
 *
 * @author martin
 */
public class PodielNajomnaZmluvaConverter {
    
    public static PodielNajomnaZmluva podielNajomnaZmluvaDtoToPodielNajomnaZmluva
            (PodielNajomnaZmluvaDto pnzDto){
        
        PodielNajomnaZmluva pnz = new PodielNajomnaZmluva();
        pnz.setId(pnzDto.getId());
        
        if (pnzDto.getNajomnaZmluva() != null){
            pnz.setNajomnaZmluva(NajomnaZmluvaConverter.najomnaZmluvaDtoToNajomnaZmluva(pnzDto.getNajomnaZmluva()));
        }
        if (pnzDto.getPodiel() != null){
            pnz.setPodiel(PodielConverter.podielDtoToPodiel(pnzDto.getPodiel()));
        }
        return pnz;
    }
    
    public static PodielNajomnaZmluvaDto podielNajomnaZmluvaToPodielNajomnaZmluvaDto
            (PodielNajomnaZmluva pnz){
    
        PodielNajomnaZmluvaDto pnzDto = new PodielNajomnaZmluvaDto();
        pnzDto.setId(pnz.getId());
        
        if (pnz.getNajomnaZmluva() != null){
            pnzDto.setNajomnaZmluva(NajomnaZmluvaConverter.najomnaZmluvaToNajomnaZmluvaDto(pnz.getNajomnaZmluva()));
        }
        if (pnz.getPodiel() != null) {
            pnzDto.setPodiel(PodielConverter.podielToPodielDto(pnz.getPodiel()));
        }
        return pnzDto;
    }
}
