/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.dao;

import fbm.vutbr.martinsakac.bakalarka.entity.PodielNajomnaZmluva;
import java.util.List;

/**
 *
 * @author martin
 */
public interface PodielNajomnaZmluvaDao {
    public void createPodielNajomnaZmluva(PodielNajomnaZmluva pnz);
    public void updatePodielNajomnaZmluva(PodielNajomnaZmluva pnz);
    public void deletePodielNajomnaZmluva(PodielNajomnaZmluva pnz);
    public PodielNajomnaZmluva getPodielNajomnaZmluvaById(Long id);
    public List<PodielNajomnaZmluva> getAllPodielNajomnaZmluva();
}
