/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.utils;

import fbm.vutbr.martinsakac.bakalarka.dto.UzivatelDto;
import fbm.vutbr.martinsakac.bakalarka.entity.Uzivatel;

/**
 *
 * @author martin
 */
public class UzivatelConverter {
    public static UzivatelDto uzivatelToUzivatelDto(Uzivatel uz){
        UzivatelDto uzDto = new UzivatelDto();
        uzDto.setId(uz.getId());
        uzDto.setMeno(uz.getMeno());
        uzDto.setPriezvisko(uz.getPriezvisko());
        uzDto.setPracovneZaradenie(uz.getPracovneZaradenie());
        uzDto.setUzivatelskeMeno(uz.getUzivatelskeMeno());
        uzDto.setUzivatelskeHeslo(uz.getUzivatelskeHeslo());
        uzDto.setUzivatelskaRola(uz.getUzivatelskaRola());
        if (uz.getSpolocnost() != null){
            uzDto.setSpolocnost(SpolocnostConverter.spolocnostToSpolocnostDto(uz.getSpolocnost()));
        }
        return uzDto;
    }
    
    public static Uzivatel uzivatelDtoToUzivatel(UzivatelDto uzDto){
        Uzivatel uz = new Uzivatel();
        uz.setId(uzDto.getId());
        uz.setMeno(uzDto.getMeno());
        uz.setPriezvisko(uzDto.getPriezvisko());
        uz.setPracovneZaradenie(uzDto.getPracovneZaradenie());
        uz.setUzivatelskeMeno(uzDto.getUzivatelskeMeno());
        uz.setUzivatelskeHeslo(uzDto.getUzivatelskeHeslo());
        uz.setUzivatelskaRola(uzDto.getUzivatelskaRola());
        if (uzDto.getSpolocnost() != null){
            uz.setSpolocnost(SpolocnostConverter.spolocnostDtoToSpolocnost(uzDto.getSpolocnost()));
        }
        return uz;
    }
}
