/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.daoimpl;

import fbm.vutbr.martinsakac.bakalarka.dao.SpolocnostDao;
import fbm.vutbr.martinsakac.bakalarka.entity.Spolocnost;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author martin
 */

@Repository
public class SpolocnostDaoImpl implements SpolocnostDao{
    
    @PersistenceContext
    private EntityManager em;
    
    public EntityManager getEntityManager(){
        return this.em;
    }
    
    public void setEntityManager(EntityManager em){
        this.em = em;
    }

    @Override
    public void createSpolocnost(Spolocnost spol) {
        Validate.isTrue(spol != null, "Spolocnost must not be null!");
        Validate.isTrue(spol.getId() == null, "Spolocnost id must be null!");
        em.persist(spol);
    }

    @Override
    public void updateSpolocnost(Spolocnost spol) {
        Validate.isTrue(spol != null, "Spolocnost must not be null!");
        Validate.isTrue(spol.getId() != null, "Spolocnost id must not be null!");
        em.merge(spol);
    }

    @Override
    public void deleteSpolocnost(Spolocnost spol) {
        Validate.isTrue(spol != null, "Spolocnost must not be null!");
        Validate.isTrue(spol.getId() != null, "Spolocnost id must not be null!");
        spol = em.merge(spol);
        if (spol == null){
            throw new IllegalArgumentException("Spolocnost is not in DB, cannot be deleted!");
        }
        else {
            em.remove(spol);
        }
    }

    @Override
    public Spolocnost getSpolocnostById(Long id) {
        Validate.isTrue(id != null, "ID cannot be null!");
        return em.find(Spolocnost.class, id);
    }

    @Override
    public List<Spolocnost> getAllSpolocnost() {
        TypedQuery<Spolocnost> query = em.createQuery("select s from Spolocnost s", 
                Spolocnost.class);
        return query.getResultList();
    }
    
}
