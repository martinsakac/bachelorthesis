/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.inputdata;

import au.com.bytecode.opencsv.CSVReader;
import fbm.vutbr.martinsakac.bakalarka.dto.KatastralneUzemieDto;
import fbm.vutbr.martinsakac.bakalarka.dto.ListVlastnictvaDto;
import fbm.vutbr.martinsakac.bakalarka.dto.NajomnaZmluvaDto;
import fbm.vutbr.martinsakac.bakalarka.dto.ParcelaDto;
import fbm.vutbr.martinsakac.bakalarka.dto.PodielDto;
import fbm.vutbr.martinsakac.bakalarka.dto.PodielNajomnaZmluvaDto;
import fbm.vutbr.martinsakac.bakalarka.dto.VlastnikDto;
import fbm.vutbr.martinsakac.bakalarka.service.KatastralneUzemieService;
import fbm.vutbr.martinsakac.bakalarka.service.ListVlastnictvaService;
import fbm.vutbr.martinsakac.bakalarka.service.NajomnaZmluvaService;
import fbm.vutbr.martinsakac.bakalarka.service.ParcelaService;
import fbm.vutbr.martinsakac.bakalarka.service.PodielNajomnaZmluvaService;
import fbm.vutbr.martinsakac.bakalarka.service.PodielService;
import fbm.vutbr.martinsakac.bakalarka.service.VlastnikService;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import org.joda.time.LocalDate;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author martin
 */
public class MyCsvParser {
    
    public static void main(String[] args) throws FileNotFoundException, IOException{
        
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        System.out.println("hahaha");
        
        VlastnikService vlastnikService = 
                (VlastnikService)context.getBean("vlastnikServiceImpl");
        PodielService podielService = 
                (PodielService)context.getBean("podielServiceImpl");
        PodielNajomnaZmluvaService pnzService = 
                (PodielNajomnaZmluvaService)context.getBean("podielNajomnaZmluvaServiceImpl");
        NajomnaZmluvaService najomnaZmluvaService = 
                (NajomnaZmluvaService)context.getBean("najomnaZmluvaServiceImpl");
        ListVlastnictvaService listVlastnictvaService = 
                (ListVlastnictvaService)context.getBean("listVlastnictvaServiceImpl");
        ParcelaService parcelaService = 
                (ParcelaService)context.getBean("parcelaServiceImpl");
        KatastralneUzemieService katastralneUzemieService = 
                (KatastralneUzemieService)context.getBean("katastralneUzemieServiceImpl");
        
        
        if (vlastnikService == null && podielService == null && najomnaZmluvaService == null 
                && listVlastnictvaService == null && parcelaService == null && katastralneUzemieService == null 
                && pnzService == null){
            System.out.println("Service triedy nie su natiahnute.");
            return;
        }
        
        KatastralneUzemieDto kuVrable = katastralneUzemieService.getKatastralneUzemieById(1L);
        KatastralneUzemieDto kuHornyOhaj = katastralneUzemieService.getKatastralneUzemieById(2L);
        KatastralneUzemieDto kuTehla = katastralneUzemieService.getKatastralneUzemieById(4L);
        KatastralneUzemieDto kuNVNZ = katastralneUzemieService.getKatastralneUzemieById(3L);
        KatastralneUzemieDto kuTajna = katastralneUzemieService.getKatastralneUzemieById(5L);
        
        /*
         * Nacitanie vsetkych riadkov do kolekcie
         */
        // Linux
//        CSVReader reader = new CSVReader(new FileReader("/home/martin/Dropbox/BAKALARKA/DokumentyPD/mojPoskladany-csv.csv"));
        // Windows
        CSVReader reader = new CSVReader(new FileReader("C:/Users/Martin/Dropbox/BAKALARKA/DokumentyPD/mojPoskladany-csv.csv"));
        List<String[]> myEntries = reader.readAll();
        
        if (myEntries.get(1)[6] == null) 
            System.out.println("je to null!");
        if (myEntries.get(1)[6].equals(""))
            System.out.println("je to prazdny retazec!!");
        
        for (int i = 1; i < myEntries.size(); i++){
            String[] aktualnyZaznam = myEntries.get(i);
            
            String priezviskoMenoTitul = aktualnyZaznam[1];
            String evCislo = aktualnyZaznam[2];
            String adresa = aktualnyZaznam[3];
            String telCislo = aktualnyZaznam[4];
            String datumNarodenia = aktualnyZaznam[5];
            String vrable = aktualnyZaznam[6];
            String hornyOhaj = aktualnyZaznam[7];
            String tehla = aktualnyZaznam[8];
            String nvnz = aktualnyZaznam[9];
            String tajna = aktualnyZaznam[10];
            String odZmluva = aktualnyZaznam[11];
            String doZmluva = aktualnyZaznam[12];
            String sadzba = aktualnyZaznam[13];
            String cisloUctu = aktualnyZaznam[14];
            
            VlastnikDto vlastnik = new VlastnikDto();
            NajomnaZmluvaDto najomnaZmluva = new NajomnaZmluvaDto();
            najomnaZmluva.setDatumZhotovenia(new LocalDate());
            
            /*
             * nastavenie mena, priezviska a titulu Vlastnikovi
             */
            String[] menoArray = priezviskoMenoTitul.split(" ");
            switch(menoArray.length) {
                case 2: 
                    vlastnik.setMeno(menoArray[1]);
                    vlastnik.setPriezvisko(menoArray[0]);
                    break;
                case 3: 
                    vlastnik.setMeno(menoArray[1]);
                    vlastnik.setPriezvisko(menoArray[0]);
                    vlastnik.setTitul(menoArray[2]);
                    break;
                default: 
                    break;
            }
            
            /*
             * nastavenie datumu narodenia Vlastnika
             */
            String[] datumArray = datumNarodenia.split("\\.");
            LocalDate datNar = new LocalDate(Integer.valueOf(datumArray[2]), Integer.valueOf(datumArray[1]), Integer.valueOf(datumArray[0]));
            vlastnik.setDatumNarodenia(datNar);
            
            vlastnik.setCisloUctu(cisloUctu);
            vlastnik.setTelCislo(telCislo);
            vlastnik.setAdresa(adresa);
            vlastnik.setEvidencneCislo(evCislo);
            
            /*
             * nastavenie platnosti najomnej zmluvy - od, do, + sadzba
             */
            try {
                najomnaZmluva.setPlatnostOd(new LocalDate(Integer.valueOf(odZmluva), 1, 1));
            }
            catch (RuntimeException ex){    
                najomnaZmluva.setPlatnostOd(null);
            }
            try{
                najomnaZmluva.setPlatnostDo(new LocalDate(Integer.valueOf(doZmluva), 12, 31));
            }
            catch(RuntimeException ex){
                najomnaZmluva.setPlatnostDo(null);
            }
            try{
                najomnaZmluva.setSadzba(new BigDecimal(sadzba));
            }
            catch(RuntimeException ex){
                najomnaZmluva.setSadzba(null);
            }
            
            vlastnikService.addVlastnik(vlastnik);
            najomnaZmluvaService.addNajomnaZmluva(najomnaZmluva);
            
            if (!vrable.equals("")){
                ListVlastnictvaDto lv = new ListVlastnictvaDto();
                listVlastnictvaService.addListVlastnictva(lv);
                
                PodielDto podielDto = new PodielDto();
                podielDto.setVlastnik(vlastnik);
                podielDto.setListVlastnictva(lv);
                //podielService.addPodiel(podielDto);
                
                PodielNajomnaZmluvaDto pnzDto = new PodielNajomnaZmluvaDto();
                pnzDto.setPodiel(podielDto);
                pnzDto.setNajomnaZmluva(najomnaZmluva);
                pnzService.addPodielNajomnaZmluva(pnzDto);
                
                ParcelaDto parcelaDto = new ParcelaDto();
                parcelaDto.setListVlastnictva(lv);
                parcelaDto.setKatastralneUzemie(kuVrable);
                
                try{
                    parcelaDto.setVymera(new BigDecimal(vrable).setScale(4));
                }
                catch(RuntimeException ex){
                    parcelaDto.setVymera(null);
                }
                
                parcelaService.addParcela(parcelaDto);
                
                System.out.println(parcelaDto.getVymera());
                System.out.println(parcelaDto.getVymera().setScale(4));
            }
            
            if (!hornyOhaj.equals("")){
                ListVlastnictvaDto lv = new ListVlastnictvaDto();
                listVlastnictvaService.addListVlastnictva(lv);
                
                PodielDto podielDto = new PodielDto();
                podielDto.setVlastnik(vlastnik);
                podielDto.setListVlastnictva(lv);
                //podielService.addPodiel(podielDto);
                
                PodielNajomnaZmluvaDto pnzDto = new PodielNajomnaZmluvaDto();
                pnzDto.setPodiel(podielDto);
                pnzDto.setNajomnaZmluva(najomnaZmluva);
                pnzService.addPodielNajomnaZmluva(pnzDto);
                
                ParcelaDto parcelaDto = new ParcelaDto();
                parcelaDto.setListVlastnictva(lv);
                parcelaDto.setKatastralneUzemie(kuHornyOhaj);
                
                try{
                    BigDecimal vymera = new BigDecimal(hornyOhaj).setScale(4);
                    parcelaDto.setVymera(vymera);
                }
                catch(RuntimeException ex){
                    parcelaDto.setVymera(null);
                }
                parcelaService.addParcela(parcelaDto);
                
                System.out.println(parcelaDto.getVymera());
                System.out.println(parcelaDto.getVymera().setScale(4));
            }
            
            if (!tehla.equals("")) {
                
            }
            
            if (!tajna.equals("")){
                
            }
            
            if (!nvnz.equals("")){
                
            }
            
            
         }
        
    }
}
