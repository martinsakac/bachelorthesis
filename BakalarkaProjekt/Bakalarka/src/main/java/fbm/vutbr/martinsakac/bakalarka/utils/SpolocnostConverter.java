/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.utils;

import fbm.vutbr.martinsakac.bakalarka.dto.SpolocnostDto;
import fbm.vutbr.martinsakac.bakalarka.entity.Spolocnost;

/**
 *
 * @author martin
 */
public class SpolocnostConverter {
    public static Spolocnost spolocnostDtoToSpolocnost(SpolocnostDto spolDto){
        Spolocnost spol = new Spolocnost();
        spol.setId(spolDto.getId());
        spol.setIco(spolDto.getIco());
        spol.setNazov(spolDto.getNazov());
        spol.setPravnaForma(spolDto.getPravnaForma());
        return spol;
    }
    
    public static SpolocnostDto spolocnostToSpolocnostDto(Spolocnost spol){
        SpolocnostDto spolDto = new SpolocnostDto();
        spolDto.setId(spol.getId());
        spolDto.setIco(spol.getIco());
        spolDto.setNazov(spol.getNazov());
        spolDto.setPravnaForma(spol.getNazov());
        return spolDto;
    }
}
