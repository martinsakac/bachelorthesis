/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.entity;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author martin
 */
@Entity
public class Podiel implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    
    private Integer percentualnyPodiel;
    
    @ManyToOne(cascade = CascadeType.ALL)
    private Vlastnik vlastnik;
    
    @ManyToOne(cascade = CascadeType.ALL)
    private ListVlastnictva listVlastnictva;

    public Podiel() {
    }
    
    public Podiel(Long id, Integer percentualnyPodiel, Vlastnik vlastnik, 
            ListVlastnictva listVlastnistva) {
        this.id = id;
        this.percentualnyPodiel = percentualnyPodiel;
        this.vlastnik = vlastnik;
        this.listVlastnictva = listVlastnistva;
    }

    public Podiel(Integer percentualnyPodiel, Vlastnik vlastnik, 
            ListVlastnictva listVlastnistva) {
        this.percentualnyPodiel = percentualnyPodiel;
        this.vlastnik = vlastnik;
        this.listVlastnictva = listVlastnistva;
    }
    
    public ListVlastnictva getListVlastnictva() {
        return listVlastnictva;
    }

    public void setListVlastnictva(ListVlastnictva listVlastnistva) {
        this.listVlastnictva = listVlastnistva;
    }

    public Integer getPercentualnyPodiel() {
        return percentualnyPodiel;
    }

    public Vlastnik getVlastnik() {
        return vlastnik;
    }

    public void setPercentualnyPodiel(Integer percentualnyPodiel) {
        this.percentualnyPodiel = percentualnyPodiel;
    }

    public void setVlastnik(Vlastnik vlastnik) {
        this.vlastnik = vlastnik;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Podiel)) {
            return false;
        }
        Podiel other = (Podiel) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fbm.vutbr.martinsakac.bakalarka.entity.Podiel[ id=" + id + " ]";
    }
    
}
