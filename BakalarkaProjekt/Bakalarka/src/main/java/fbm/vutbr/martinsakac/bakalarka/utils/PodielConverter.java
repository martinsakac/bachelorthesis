/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.utils;

import fbm.vutbr.martinsakac.bakalarka.dto.PodielDto;
import fbm.vutbr.martinsakac.bakalarka.entity.Podiel;


/**
 *
 * @author martin
 */
public class PodielConverter {
    public static Podiel podielDtoToPodiel(PodielDto podielDto){
        Podiel podiel = new Podiel();
        
        podiel.setId(podielDto.getId());
        podiel.setPercentualnyPodiel(podielDto.getPercentualnyPodiel());
        
        if (podielDto.getListVlastnictva() != null){
            podiel.setListVlastnictva(ListVlastnictvaConverter
                    .listVlastnictvaDtoToListVlastnictva(podielDto.getListVlastnictva()));
        }
        if (podielDto.getVlastnik() != null){
            podiel.setVlastnik(VlastnikConverter.vlastnikDtoToVlastnik
                    (podielDto.getVlastnik()));
        }
        
        return podiel;
    }
    
    public static PodielDto podielToPodielDto(Podiel podiel){
        PodielDto podielDto = new PodielDto();
        
        podielDto.setId(podiel.getId());
        podielDto.setPercentualnyPodiel(podiel.getPercentualnyPodiel());
        
        if (podiel.getListVlastnictva() != null){
            podielDto.setListVlastnictva(ListVlastnictvaConverter
                    .listVlastnictvaToListVlastnictvaDto(podiel.getListVlastnictva()));
        }
        if (podiel.getVlastnik() != null){
            podielDto.setVlastnik(VlastnikConverter.vlastnikToVlastnikDto(podiel.getVlastnik()));
        }
        
        return podielDto;
    }
}
