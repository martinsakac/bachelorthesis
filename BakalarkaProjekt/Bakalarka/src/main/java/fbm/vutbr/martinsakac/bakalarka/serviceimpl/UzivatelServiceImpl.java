/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.serviceimpl;

import fbm.vutbr.martinsakac.bakalarka.dao.UzivatelDao;
import fbm.vutbr.martinsakac.bakalarka.dto.UzivatelDto;
import fbm.vutbr.martinsakac.bakalarka.entity.Uzivatel;
import fbm.vutbr.martinsakac.bakalarka.service.UzivatelService;
import fbm.vutbr.martinsakac.bakalarka.utils.UzivatelConverter;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author martin
 */

@Service
public class UzivatelServiceImpl implements UzivatelService{
    
    @Autowired
    private UzivatelDao dao;

    public void setDao(UzivatelDao dao) {
        this.dao = dao;
    }

    public UzivatelDao getDao() {
        return dao;
    }

    @Override
    @Transactional
    public void addUzivatel(UzivatelDto uzivatelDto) {
        Validate.isTrue(uzivatelDto != null, "Uzivatel dto must not be null.");
        Validate.isTrue(uzivatelDto.getId() == null, "Uzivatel ID must be null.");
        Uzivatel uzivatel = UzivatelConverter.uzivatelDtoToUzivatel(uzivatelDto);
        dao.createUzivatel(uzivatel);
        uzivatelDto.setId(uzivatel.getId());
    }

    @Override
    @Transactional
    public void updateUzivatel(UzivatelDto uzivatelDto) {
        Validate.isTrue(uzivatelDto != null, "Uzivatel dto must not be null.");
        Validate.isTrue(uzivatelDto.getId() != null, "Uzivatel ID must not be null.");
        dao.updateUzivatel(UzivatelConverter.uzivatelDtoToUzivatel(uzivatelDto));
    }

    @Override
    @Transactional
    public void deleteUzivatel(UzivatelDto uzivatelDto) {
        Validate.isTrue(uzivatelDto != null, "Uzivatel dto must not be null.");
        Validate.isTrue(uzivatelDto.getId() != null, "Uzivatel ID must not be null.");
        dao.deleteUzivatel(UzivatelConverter.uzivatelDtoToUzivatel(uzivatelDto));
    }

    @Override
    @Transactional
    public UzivatelDto getUzivatelById(Long id) {
        Validate.isTrue(id != null, "ID must not be null.");
        return UzivatelConverter.uzivatelToUzivatelDto(dao.getUzivatelById(id));
    }

    @Override
    @Transactional
    public List<UzivatelDto> getAllUzivatel() {
        List<UzivatelDto> result = new ArrayList<>();
        for (Uzivatel uzivatel : dao.getAllUzivatel()){
            result.add(UzivatelConverter.uzivatelToUzivatelDto(uzivatel));
        }
        return result;
    }

    @Override
    public UzivatelDto login(String login, String password) {
        Uzivatel uzivatel = null;
//        try {
//            uzivatel = dao.getUzivatelByLogin(login);
//        }
//        catch(Exception ex){
//            return null;
//        }
        uzivatel = dao.getUzivatelByLogin(login);
        if (uzivatel.getUzivatelskeHeslo().equals(password)){
            return UzivatelConverter.uzivatelToUzivatelDto(uzivatel);
        }
        else {
            return null;
        }
    }
    
}
