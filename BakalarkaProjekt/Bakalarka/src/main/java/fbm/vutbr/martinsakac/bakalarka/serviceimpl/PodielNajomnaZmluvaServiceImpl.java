/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.serviceimpl;

import fbm.vutbr.martinsakac.bakalarka.dao.PodielNajomnaZmluvaDao;
import fbm.vutbr.martinsakac.bakalarka.dto.PodielNajomnaZmluvaDto;
import fbm.vutbr.martinsakac.bakalarka.entity.PodielNajomnaZmluva;
import fbm.vutbr.martinsakac.bakalarka.service.PodielNajomnaZmluvaService;
import fbm.vutbr.martinsakac.bakalarka.utils.PodielNajomnaZmluvaConverter;
import java.util.List;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang3.Validate;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author martin
 */

@Service("podielNajomnaZmluvaServiceImpl")
public class PodielNajomnaZmluvaServiceImpl implements PodielNajomnaZmluvaService{

    @Autowired
    private PodielNajomnaZmluvaDao pnzDao;

    public void setPodielDao(PodielNajomnaZmluvaDao pnzDao) {
        this.pnzDao = pnzDao;
    }

    public PodielNajomnaZmluvaDao getPodielDao() {
        return pnzDao;
    }
   
    @Override
    @Transactional
    public void addPodielNajomnaZmluva(PodielNajomnaZmluvaDto pnzDto) {
        Validate.isTrue(pnzDto != null, "PNZ cannot be null!");
        Validate.isTrue(pnzDto.getId() == null, "PNZ ID must be null!");
        PodielNajomnaZmluva pnz = PodielNajomnaZmluvaConverter
                .podielNajomnaZmluvaDtoToPodielNajomnaZmluva(pnzDto);
        pnzDao.createPodielNajomnaZmluva(pnz);
        pnzDto.setId(pnz.getId());
    }

    @Override
    @Transactional
    public void updatePodielNajomnaZmluva(PodielNajomnaZmluvaDto pnzDto) {
        Validate.isTrue(pnzDto != null, "PNZ cannot be null!");
        Validate.isTrue(pnzDto.getId() != null, "PNZ ID must not be null!");
        pnzDao.updatePodielNajomnaZmluva(PodielNajomnaZmluvaConverter
                .podielNajomnaZmluvaDtoToPodielNajomnaZmluva(pnzDto));
    }

    @Override
    @Transactional
    public void deletePodielNajomnaZmluva(PodielNajomnaZmluvaDto pnzDto) {
        Validate.isTrue(pnzDto != null, "PNZ cannot be null!");
        Validate.isTrue(pnzDto.getId() != null, "PNZ ID must not be null!");
        pnzDao.deletePodielNajomnaZmluva(PodielNajomnaZmluvaConverter
                .podielNajomnaZmluvaDtoToPodielNajomnaZmluva(pnzDto));
    }

    @Override
    @Transactional
    public PodielNajomnaZmluvaDto getPodielNajomnaZmluvaById(Long id) {
        Validate.isTrue(id != null, "PNZ ID cannot be null!");
        return PodielNajomnaZmluvaConverter
                .podielNajomnaZmluvaToPodielNajomnaZmluvaDto(pnzDao.getPodielNajomnaZmluvaById(id));
    }

    @Override
    @Transactional
    public List<PodielNajomnaZmluvaDto> getAllPodielNajomnaZmluva() {
        List<PodielNajomnaZmluvaDto> result = new ArrayList<>();
        for (PodielNajomnaZmluva pnz : pnzDao.getAllPodielNajomnaZmluva()){
            result.add(PodielNajomnaZmluvaConverter.podielNajomnaZmluvaToPodielNajomnaZmluvaDto(pnz));
        }
        return result;
    }
    
}
