/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.daoimpl;

import fbm.vutbr.martinsakac.bakalarka.dao.ListVlastnictvaDao;
import fbm.vutbr.martinsakac.bakalarka.entity.ListVlastnictva;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author martin
 */

@Repository
public class ListVlastnictvaDaoImpl implements ListVlastnictvaDao{
    
    @PersistenceContext
    private EntityManager em;
    
    public EntityManager getEntityManager(){
        return this.em;
    }
    
    public void setEntityManager(EntityManager em){
        this.em = em;
    }

    @Override
    public void createListVlastnictva(ListVlastnictva lv) {
        Validate.isTrue(lv != null, "List vlastnictva must not be null.");
        Validate.isTrue(lv.getId() == null, "List vlastnictva ID must be null.");
        em.persist(lv);
    }

    @Override
    public void updateListVlastnictva(ListVlastnictva lv) {
        Validate.isTrue(lv != null, "List vlastnictva must not be null.");
        Validate.isTrue(lv.getId() != null, "List vlastnictva ID must not be null.");
        em.merge(lv);
    }

    @Override
    public void deleteListVlastnictva(ListVlastnictva lv) {
        Validate.isTrue(lv != null, "List vlastnictva must not be null.");
        Validate.isTrue(lv.getId() != null, "List vlastnictva ID must not be null.");
        lv = em.merge(lv);
        if (lv == null){
            throw new IllegalArgumentException("List vlastnictva is not in DB, cannot be null.");
        }
        em.remove(lv);
    }

    @Override
    public ListVlastnictva getListVlastnictvaById(Long id) {
        Validate.isTrue(id != null, "ID cannot be null");
        return em.find(ListVlastnictva.class, id);
    }

    @Override
    public List<ListVlastnictva> getAllListVlastnictva() {
        TypedQuery<ListVlastnictva> query = em.createQuery("select lv from ListVlastnictva lv", ListVlastnictva.class);
        return query.getResultList();
    }
    
}
