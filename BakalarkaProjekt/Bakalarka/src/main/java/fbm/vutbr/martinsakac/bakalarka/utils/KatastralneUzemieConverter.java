/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.utils;

import fbm.vutbr.martinsakac.bakalarka.dto.KatastralneUzemieDto;
import fbm.vutbr.martinsakac.bakalarka.entity.KatastralneUzemie;

/**
 *
 * @author martin
 */
public class KatastralneUzemieConverter {
    public static KatastralneUzemie katastralneUzemieDtoToKatastralneUzemie(KatastralneUzemieDto kuDto){
        KatastralneUzemie ku = new KatastralneUzemie();
        ku.setId(kuDto.getId());
        ku.setIdCislo(kuDto.getIdCislo());
        ku.setNazov(kuDto.getNazov());
        ku.setObec(ObecConverter.obecDtoToObec(kuDto.getObec()));
        return ku;
    }
    
    public static KatastralneUzemieDto katastralneUzemieToKatastralneUzemieDto(KatastralneUzemie ku){
        KatastralneUzemieDto kuDto = new KatastralneUzemieDto();
        kuDto.setId(ku.getId());
        kuDto.setIdCislo(ku.getIdCislo());
        kuDto.setNazov(ku.getNazov());
        kuDto.setObec(ObecConverter.obecToObecDto(ku.getObec()));
        return kuDto;
    }
}
