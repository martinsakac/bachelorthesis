/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.daoimpl;

import fbm.vutbr.martinsakac.bakalarka.dao.KatastralneUzemieDao;
import fbm.vutbr.martinsakac.bakalarka.entity.KatastralneUzemie;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author martin
 */

@Repository
public class KatastralneUzemieDaoImpl implements KatastralneUzemieDao{
    
    @PersistenceContext
    private EntityManager em;
    
    public EntityManager getEntityManager(){
        return this.em;
    }
    
    public void setEntityManager(EntityManager em){
        this.em = em;
    }

    @Override
    public void createKatastralneUzemie(KatastralneUzemie ku) {
        Validate.isTrue(ku != null, "KatastralneUzemie cannot be null.");
        Validate.isTrue(ku.getId() == null, "Katastralne uzemie ID must be null.");
        em.persist(ku);
    }

    @Override
    public void updateKatastralneUzemie(KatastralneUzemie ku) {
        Validate.isTrue(ku != null, "KatastralneUzemie cannot be null.");
        Validate.isTrue(ku.getId() != null, "Katastralne uzemie ID must not be null.");
        em.merge(ku);
    }

    @Override
    public void deleteKatastralneUzemie(KatastralneUzemie ku) {
        Validate.isTrue(ku != null, "KatastralneUzemie cannot be null.");
        Validate.isTrue(ku.getId() != null, "Katastralne uzemie ID must not be null.");
        ku = em.merge(ku);
        if (ku == null){
            throw new IllegalArgumentException("Katastralne uzemie is not in DB, cannot be deleted.");
        }
        em.remove(ku);
    }

    @Override
    public KatastralneUzemie getKatastralneUzemieById(Long id) {
        Validate.isTrue(id != null, "ID cannot be null");
        return em.find(KatastralneUzemie.class, id);
    }

    @Override
    public List<KatastralneUzemie> getAllKatastralneUzemie() {
        TypedQuery<KatastralneUzemie> query = em.createQuery("select ku from KatastralneUzemie ku", 
                KatastralneUzemie.class);
        return query.getResultList();
    }
    
}
