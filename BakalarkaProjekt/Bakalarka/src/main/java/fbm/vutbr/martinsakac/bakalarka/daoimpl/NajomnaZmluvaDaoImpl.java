/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.daoimpl;

import fbm.vutbr.martinsakac.bakalarka.dao.NajomnaZmluvaDao;
import fbm.vutbr.martinsakac.bakalarka.entity.NajomnaZmluva;
import fbm.vutbr.martinsakac.bakalarka.entity.Podiel;
import fbm.vutbr.martinsakac.bakalarka.entity.Vlastnik;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author martin
 */

@Repository
public class NajomnaZmluvaDaoImpl implements NajomnaZmluvaDao{
    
    @PersistenceContext
    private EntityManager em;
    
    public EntityManager getEntityManager(){
        return this.em;
    }
    
    public void setEntityManager(EntityManager em){
        this.em = em;
    }

    @Override
    public void createNajomnaZmluva(NajomnaZmluva nz) {
        Validate.isTrue(nz != null, "Najomna zmluva cannot be null.");
        Validate.isTrue(nz.getId() == null, "Najomna zmluva ID must be null.");
        em.persist(nz);
    }

    @Override
    public void updateNajomnaZmluva(NajomnaZmluva nz) {
        Validate.isTrue(nz != null, "Najomna zmluva cannot be null.");
        Validate.isTrue(nz.getId() != null, "Najomna zmluva ID must not be null.");
        em.merge(nz);
    }

    @Override
    public void deleteNajomnaZmluva(NajomnaZmluva nz) {
        Validate.isTrue(nz != null, "Najomna zmluva cannot be null.");
        Validate.isTrue(nz.getId() != null, "Najomna zmluva ID must not be null.");
        em.remove(nz);
    }

    @Override
    public NajomnaZmluva getNajomnaZmluvaById(Long id) {
        Validate.isTrue(id != null, "ID cannot be null.");
        return em.find(NajomnaZmluva.class, id);
    }
    
    @Override
    public Vlastnik getVlastnikByNajomnaZmluva(Long idNz) {
        TypedQuery<Vlastnik> query = em.createQuery("select v "
                + "from NajomnaZmluva nz, PodielNajomnaZmluva pnz, Podiel p, Vlastnik v "
                + "where nz = pnz.najomnaZmluva and pnz.podiel = p and p.vlastnik = v "
                + "and nz.id = :idNz", Vlastnik.class)
                .setParameter("idNz", idNz);
        return query.getSingleResult();
    }

    @Override
    public List<NajomnaZmluva> getAllNajomnaZmluva(String column, String direction) {
        TypedQuery<NajomnaZmluva> query = null;
        
        if (column.equals("platnostDo")) {
            query = em.createQuery("select nz from NajomnaZmluva nz order by platnostDo", NajomnaZmluva.class);
        }
        if (column.equals("platnostOd")) {
            query = em.createQuery("select nz from NajomnaZmluva nz order by platnostOd", NajomnaZmluva.class);
        }
        if (column.equals("zhotovenie")) {
            query = em.createQuery("select nz from NajomnaZmluva nz order by datumZhotovenia", NajomnaZmluva.class);
        }
        if (column.equals("")) {
            query = em.createQuery("select nz from NajomnaZmluva nz", NajomnaZmluva.class);
        }
        
        List<NajomnaZmluva> result = query.getResultList();
        if (direction.equals("desc")) {
            Collections.reverse(result);
        }
        
        return result;
    }

    @Override
    public List<NajomnaZmluva> getNajomnaZmluvaByVlastnikId(Long vlastnikId) {
        TypedQuery<NajomnaZmluva> query = em.createQuery(
                "SELECT DISTINCT nz FROM NajomnaZmluva nz, PodielNajomnaZmluva pnz, Podiel p, "
                + "Vlastnik v "
                + "WHERE nz = pnz.najomnaZmluva and pnz.podiel = p and p.vlastnik = v "
                + "and v.id = :vlastnikId"
                , NajomnaZmluva.class)
                .setParameter("vlastnikId", vlastnikId);
        //and v.id = :vlastnikId 
        return query.getResultList();
    }

    @Override
    public BigDecimal getNajomnaZmluvaSucetVymier(NajomnaZmluva nz) {
        TypedQuery<Object[]> query = em.createQuery("SELECT SUM(pa.vymera) FROM NajomnaZmluva nz, "
                + "PodielNajomnaZmluva pnz, Podiel p, ListVlastnictva lv, Parcela pa "
                + "WHERE nz = pnz.najomnaZmluva and pnz.podiel = p and "
                + "p.listVlastnictva = lv and pa.listVlastnictva = lv and "
                + "nz.id = :nzid "
                + "GROUP BY nz.id", Object[].class).setParameter("nzid", nz.getId());
        List<Object[]> result = query.getResultList();
        if (result == null || result.size() != 1){
            throw new IllegalArgumentException("Sucet vymier neprebehol v poriadku!");
        }
        BigDecimal sucet = new BigDecimal(String.valueOf(result.get(0)));
        return sucet;
    }   
}