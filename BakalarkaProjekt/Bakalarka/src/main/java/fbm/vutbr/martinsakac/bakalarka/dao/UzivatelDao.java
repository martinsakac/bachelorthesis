/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.dao;

import fbm.vutbr.martinsakac.bakalarka.entity.Uzivatel;
import java.util.List;

/**
 *
 * @author martin
 */
public interface UzivatelDao {
    public void createUzivatel(Uzivatel uz);
    public void updateUzivatel(Uzivatel uz);
    public void deleteUzivatel(Uzivatel uz);
    public Uzivatel getUzivatelById(Long id);
    public List<Uzivatel> getAllUzivatel();
    public Uzivatel getUzivatelByLogin(String login);
}
