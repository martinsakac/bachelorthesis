/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.dao;

import fbm.vutbr.martinsakac.bakalarka.entity.KatastralneUzemie;
import java.util.List;

/**
 *
 * @author martin
 */
public interface KatastralneUzemieDao {
    public void createKatastralneUzemie(KatastralneUzemie ku);
    
    public void updateKatastralneUzemie(KatastralneUzemie ku);
    
    public void deleteKatastralneUzemie(KatastralneUzemie ku);
    
    public KatastralneUzemie getKatastralneUzemieById(Long id);
    
    public List<KatastralneUzemie> getAllKatastralneUzemie();
}
