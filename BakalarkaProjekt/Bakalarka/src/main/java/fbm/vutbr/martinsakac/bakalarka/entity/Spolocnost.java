/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author martin
 */
@Entity
public class Spolocnost implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private String nazov;
    
    private String ico;
    
    private String pravnaForma;

    public String getNazov() {
        return nazov;
    }

    public String getIco() {
        return ico;
    }

    public String getPravnaForma() {
        return pravnaForma;
    }

    public void setNazov(String nazov) {
        this.nazov = nazov;
    }

    public void setIco(String ico) {
        this.ico = ico;
    }

    public void setPravnaForma(String pravnaForma) {
        this.pravnaForma = pravnaForma;
    }

    public Spolocnost(Long id, String nazov, String ico, String pravnaForma) {
        this.id = id;
        this.nazov = nazov;
        this.ico = ico;
        this.pravnaForma = pravnaForma;
    }

    public Spolocnost(String nazov, String ico, String pravnaForma) {
        this.nazov = nazov;
        this.ico = ico;
        this.pravnaForma = pravnaForma;
    }

    public Spolocnost() {
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Spolocnost)) {
            return false;
        }
        Spolocnost other = (Spolocnost) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fbm.vutbr.martinsakac.bakalarka.entity.Spolocnost[ id=" + id + " ]";
    }
    
}
