/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.serviceimpl;

import fbm.vutbr.martinsakac.bakalarka.dao.OkresDao;
import fbm.vutbr.martinsakac.bakalarka.dto.OkresDto;
import fbm.vutbr.martinsakac.bakalarka.entity.Okres;
import fbm.vutbr.martinsakac.bakalarka.service.OkresService;
import fbm.vutbr.martinsakac.bakalarka.utils.OkresConverter;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author martin
 */

@Service
public class OkresServiceImpl implements OkresService{

    @Autowired
    private OkresDao okresDao;

    public OkresDao getOkresDao() {
        return okresDao;
    }

    public void setOkresDao(OkresDao okresDao) {
        this.okresDao = okresDao;
    }
    
    @Override
    @Transactional
    public void addOkres(OkresDto okresDto) {
        Validate.isTrue(okresDto != null, "Okres dto must not be null.");
        Validate.isTrue(okresDto.getId() == null, "Okres ID must be null to be persisted.");
        Okres okres = OkresConverter.okresDtoToOkres(okresDto);
        okresDao.createOkre(okres);
        okresDto.setId(okres.getId());
    }

    @Override
    @Transactional
    public void updateOkres(OkresDto okresDto) {
        Validate.isTrue(okresDto != null, "Okres dto must not be null.");
        Validate.isTrue(okresDto.getId() != null, "Okres ID must not be null.");
        okresDao.updateOkres(OkresConverter.okresDtoToOkres(okresDto));
    }

    @Override
    @Transactional
    public void deleteOkres(OkresDto okresDto) {
        Validate.isTrue(okresDto != null, "Okres dto must not be null.");
        Validate.isTrue(okresDto.getId() != null, "Okres ID must not be null.");
        okresDao.deleteOkres(OkresConverter.okresDtoToOkres(okresDto));
    }

    @Override
    @Transactional
    public OkresDto getOkresById(Long id) {
        Validate.isTrue(id != null, "ID must not be null.");
        return OkresConverter.okresToOkresDto(okresDao.getOkresById(id));
    }

    @Override
    @Transactional
    public List<OkresDto> getAllOkres() {
        List<OkresDto> result = new ArrayList<>();
        for (Okres okres : okresDao.getAllOkres()){
            result.add(OkresConverter.okresToOkresDto(okres));
        }
        return result;
    }
    
}
