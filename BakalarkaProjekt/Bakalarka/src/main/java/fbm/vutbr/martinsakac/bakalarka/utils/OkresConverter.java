/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.utils;

import fbm.vutbr.martinsakac.bakalarka.dto.OkresDto;
import fbm.vutbr.martinsakac.bakalarka.entity.Okres;

/**
 *
 * @author martin
 */
public class OkresConverter {
    public static Okres okresDtoToOkres(OkresDto okresDto){
        Okres okres = new Okres();
        okres.setId(okresDto.getId());
        okres.setKraj(KrajConverter.krajDtoToKraj(okresDto.getKraj()));
        okres.setNazov(okresDto.getNazov());
        return okres;
    }
    
    public static OkresDto okresToOkresDto(Okres okres){
        OkresDto okresDto = new OkresDto();
        okresDto.setId(okres.getId());
        okresDto.setNazov(okres.getNazov());
        okresDto.setKraj(KrajConverter.krajToKrajDto(okres.getKraj()));
        return okresDto;
    }
}
