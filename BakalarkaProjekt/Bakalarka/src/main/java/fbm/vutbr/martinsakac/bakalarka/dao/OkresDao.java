/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.dao;

import fbm.vutbr.martinsakac.bakalarka.entity.Okres;
import java.util.List;

/**
 *
 * @author martin
 */
public interface OkresDao {
    public void createOkre(Okres okres);
    public void updateOkres(Okres okres);
    public void deleteOkres(Okres okres);
    public Okres getOkresById(Long id);
    public List<Okres> getAllOkres();
}
