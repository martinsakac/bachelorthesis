/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.daoimpl;

import fbm.vutbr.martinsakac.bakalarka.dao.OkresDao;
import fbm.vutbr.martinsakac.bakalarka.entity.Okres;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author martin
 */

@Repository
public class OkresDaoImpl implements OkresDao{
    
    @PersistenceContext
    private EntityManager em;
    
    public EntityManager getEntityManager(){
        return this.em;
    }
    
    public void setEntityManager(EntityManager em){
        this.em = em;
    }

    @Override
    public void createOkre(Okres okres) {
        Validate.isTrue(okres != null, "Okres cannot be null.");
        Validate.isTrue(okres.getId() == null, "Okres ID must be null.");
        em.persist(okres);
    }

    @Override
    public void updateOkres(Okres okres) {
        Validate.isTrue(okres != null, "Okres cannot be null.");
        Validate.isTrue(okres.getId() != null, "Okres ID must not be null.");
        em.merge(okres);
    }

    @Override
    public void deleteOkres(Okres okres) {
        Validate.isTrue(okres != null, "Okres cannot be null.");
        Validate.isTrue(okres.getId() != null, "Okres ID must not be null.");
        okres = em.merge(okres);
        if (okres == null){
            throw new IllegalArgumentException("Okres is not in DB, cannot be deleted.");
        }
        em.remove(okres);
    }

    @Override
    public Okres getOkresById(Long id) {
        Validate.isTrue(id != null, "ID must not be null.");
        return em.find(Okres.class, id);
    }

    @Override
    public List<Okres> getAllOkres() {
        TypedQuery<Okres> query = em.createQuery("select o from Okres o", Okres.class);
        return query.getResultList();
    }
    
}
