/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.dao;

import fbm.vutbr.martinsakac.bakalarka.entity.Parcela;
import fbm.vutbr.martinsakac.bakalarka.entity.Vlastnik;
import java.util.List;

/**
 *
 * @author martin
 */
public interface VlastnikDao {
    
    public void createVlastnik(Vlastnik vlastnik);

    public void updateVlastnik(Vlastnik vlastnik);
    
    public void deleteVlastnik(Vlastnik vlastnik);

    public Vlastnik getVlastnikById(Long id);
    
    public List<Vlastnik> getAllVlastnik(String column, String direction);
    
    public List<Vlastnik> getAllVlastnikOrdered(String orderingBy); 
    
    public Vlastnik getVlastnikByNajomnaZmluva(Long idNajomnaZmluva);
}
