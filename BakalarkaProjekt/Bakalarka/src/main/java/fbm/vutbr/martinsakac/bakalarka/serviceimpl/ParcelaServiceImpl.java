/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.serviceimpl;

import fbm.vutbr.martinsakac.bakalarka.dao.ParcelaDao;
import fbm.vutbr.martinsakac.bakalarka.dto.ParcelaDto;
import fbm.vutbr.martinsakac.bakalarka.entity.Parcela;
import fbm.vutbr.martinsakac.bakalarka.service.ParcelaService;
import fbm.vutbr.martinsakac.bakalarka.utils.ParcelaConverter;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author martin
 */

@Service("parcelaServiceImpl")
public class ParcelaServiceImpl implements ParcelaService{

    @Autowired
    private ParcelaDao parcelaDao;

    public void setParcelaDao(ParcelaDao parcelaDao) {
        this.parcelaDao = parcelaDao;
    }

    public ParcelaDao getParcelaDao() {
        return parcelaDao;
    }
    
    @Override
    @Transactional
    public void addParcela(ParcelaDto parcelaDto) {
        Validate.isTrue(parcelaDto != null, "Parcela dto must not be null.");
        Validate.isTrue(parcelaDto.getId() == null, "Parcela ID must be null.");
        Parcela parcela = ParcelaConverter.parcelaDtoToParcela(parcelaDto);
        parcelaDao.createParcela(parcela);
        parcelaDto.setId(parcela.getId());
    }

    @Override
    @Transactional
    public void updateParcela(ParcelaDto parcelaDto) {
        Validate.isTrue(parcelaDto != null, "Parcela dto must not be null.");
        Validate.isTrue(parcelaDto.getId() != null, "Parcela ID must not be null.");
        parcelaDao.updateParcela(ParcelaConverter.parcelaDtoToParcela(parcelaDto));
    }

    @Override
    @Transactional
    public void deleteParcela(ParcelaDto parcelaDto) {
        Validate.isTrue(parcelaDto != null, "Parcela dto must not be null.");
        Validate.isTrue(parcelaDto.getId() != null, "Parcela ID must not be null.");
        parcelaDao.deleteParcela(ParcelaConverter.parcelaDtoToParcela(parcelaDto));
    }

    @Override
    @Transactional
    public ParcelaDto getParcelaById(Long id) {
        Validate.isTrue(id != null, "ID must not be null.");
        return ParcelaConverter.parcelaToParcelaDto(parcelaDao.getParcelaById(id));
    }

    @Override
    @Transactional
    public List<ParcelaDto> getAllParcela() {
        List<ParcelaDto> result = new ArrayList<>();
        for (Parcela parcela : parcelaDao.getAllParcela()){
            result.add(ParcelaConverter.parcelaToParcelaDto(parcela));
        }
        return result;
    }

    @Override
    public List<ParcelaDto> getAllParcelaByVlastnikId(Long idVlastnik) {
        List<ParcelaDto> result = new ArrayList<>();
        for (Parcela parcela : parcelaDao.getAllParcelaByVlastnikId(idVlastnik)){
            result.add(ParcelaConverter.parcelaToParcelaDto(parcela));
        }
        return result;
    }
    
}
