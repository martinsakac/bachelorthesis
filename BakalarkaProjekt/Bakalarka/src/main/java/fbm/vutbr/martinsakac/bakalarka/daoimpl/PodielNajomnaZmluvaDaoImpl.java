/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.daoimpl;

import fbm.vutbr.martinsakac.bakalarka.dao.PodielNajomnaZmluvaDao;
import fbm.vutbr.martinsakac.bakalarka.entity.PodielNajomnaZmluva;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author martin
 */

@Repository
public class PodielNajomnaZmluvaDaoImpl implements PodielNajomnaZmluvaDao{

    @PersistenceContext
    private EntityManager em;
    
    public EntityManager getEntityManager(){
        return this.em;
    }
    
    public void setEntityManager(EntityManager em){
        this.em = em;
    }
    
    @Override
    public void createPodielNajomnaZmluva(PodielNajomnaZmluva pnz) {
        Validate.isTrue(pnz != null, "PodielNajomnaZmluva cannot be null!");
        Validate.isTrue(pnz.getId() == null, "PodielNajomnaZmluva is probably already in DB!");
        em.merge(pnz);
    }

    @Override
    public void updatePodielNajomnaZmluva(PodielNajomnaZmluva pnz) {
        Validate.isTrue(pnz != null, "PodielNajomnaZmluva cannot be null!");
        Validate.isTrue(pnz.getId() != null, "PodielNajomnaZmluva ID must not be null!");
        em.merge(pnz);
    }

    @Override
    public void deletePodielNajomnaZmluva(PodielNajomnaZmluva pnz) {
        Validate.isTrue(pnz != null, "PodielNajomnaZmluva cannot be null!");
        Validate.isTrue(pnz.getId() != null, "PodielNajomnaZmluva ID must not be null!");
        em.remove(pnz);
    }

    @Override
    public PodielNajomnaZmluva getPodielNajomnaZmluvaById(Long id) {
        Validate.isTrue(id != null, "PodielNajomnaZmluva cannot be null!");
        return em.find(PodielNajomnaZmluva.class, id);
    }

    @Override
    public List<PodielNajomnaZmluva> getAllPodielNajomnaZmluva() {
        TypedQuery<PodielNajomnaZmluva> query = em.createQuery
                ("select pnz from PodielNajomnaZmluva pnz", PodielNajomnaZmluva.class);
        return query.getResultList();
    }
    
}
