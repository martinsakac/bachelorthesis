/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.service;

import fbm.vutbr.martinsakac.bakalarka.dto.OkresDto;
import java.util.List;

/**
 *
 * @author martin
 */
public interface OkresService {
    
    public void addOkres(OkresDto okres);
    public void updateOkres(OkresDto okres);
    public void deleteOkres(OkresDto okres);
    public OkresDto getOkresById(Long id);
    public List<OkresDto> getAllOkres();
}
