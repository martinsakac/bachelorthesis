/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.dto;

/**
 *
 * @author martin
 */
public class KrajDto {
    
    private Long id;
    private String nazov;

    public KrajDto(Long id, String nazov) {
        this.id = id;
        this.nazov = nazov;
    }

    public KrajDto(String nazov) {
        this.nazov = nazov;
    }

    public KrajDto() {
    }

    public Long getId() {
        return id;
    }

    public String getNazov() {
        return nazov;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setNazov(String nazov) {
        this.nazov = nazov;
    }

    @Override
    public String toString() {
        return "KrajDto{" + "id=" + id + ", nazov=" + nazov + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final KrajDto other = (KrajDto) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }
}
