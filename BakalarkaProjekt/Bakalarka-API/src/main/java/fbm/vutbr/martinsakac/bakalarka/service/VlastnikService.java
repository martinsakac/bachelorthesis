/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.service;

import fbm.vutbr.martinsakac.bakalarka.dto.VlastnikDto;
import fbm.vutbr.martinsakac.bakalarka.dto.upravene.VlastnikSoZmluvamiDto;
import java.util.List;

/**
 *
 * @author martin
 */
public interface VlastnikService {
    
    public void addVlastnik(VlastnikDto vlastnik);
    public void updateVlastnik(VlastnikDto vlastnik);
    public void deleteVlastnik(VlastnikDto vlastnik);
    public VlastnikDto getVlastnikById(Long id);
    public List<VlastnikDto> getAllVlastnik(String column, String direction);
    public List<VlastnikSoZmluvamiDto> getAllVlastnikSoZmluvami(String column, String direction);
}
