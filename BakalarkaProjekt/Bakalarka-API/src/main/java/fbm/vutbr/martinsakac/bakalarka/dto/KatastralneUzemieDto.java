/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.dto;

/**
 *
 * @author martin
 */
public class KatastralneUzemieDto {
    
    private Long id;
    private String idCislo;
    private String nazov;
    private ObecDto obec;
    
    public KatastralneUzemieDto(Long id, String idCislo, String nazov, ObecDto obec) {
        this.id = id;
        this.idCislo = idCislo;
        this.nazov = nazov;
        this.obec = obec;
    }

    public KatastralneUzemieDto(String idCislo, String nazov, ObecDto obec) {
        this.idCislo = idCislo;
        this.nazov = nazov;
        this.obec = obec;
    }

    public KatastralneUzemieDto() {
    }

    public ObecDto getObec() {
        return obec;
    }

    public void setObec(ObecDto obec) {
        this.obec = obec;
    }
    
    public Long getId() {
        return id;
    }

    public String getIdCislo() {
        return idCislo;
    }

    public String getNazov() {
        return nazov;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setIdCislo(String idCislo) {
        this.idCislo = idCislo;
    }

    public void setNazov(String nazov) {
        this.nazov = nazov;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (this.id != null ? this.id.hashCode() : 0);
        hash = 97 * hash + (this.idCislo != null ? this.idCislo.hashCode() : 0);
        hash = 97 * hash + (this.nazov != null ? this.nazov.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final KatastralneUzemieDto other = (KatastralneUzemieDto) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        if ((this.idCislo == null) ? (other.idCislo != null) : !this.idCislo.equals(other.idCislo)) {
            return false;
        }
        if ((this.nazov == null) ? (other.nazov != null) : !this.nazov.equals(other.nazov)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "KatastralneUzemieDto{" + "id=" + id + ", idCislo=" + idCislo + ", nazov=" + nazov + ", obec=" + obec + '}';
    }
}
