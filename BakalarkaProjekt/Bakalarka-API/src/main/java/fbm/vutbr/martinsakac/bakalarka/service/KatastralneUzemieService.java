/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.service;

import fbm.vutbr.martinsakac.bakalarka.dto.KatastralneUzemieDto;
import java.util.List;

/**
 *
 * @author martin
 */
public interface KatastralneUzemieService {
    
    public void addKatastralneUzemie(KatastralneUzemieDto ku);
    
    public void updateKatastralneUzemie(KatastralneUzemieDto ku);
    
    public void deleteKatastralneUzemie(KatastralneUzemieDto ku);
    
    public KatastralneUzemieDto getKatastralneUzemieById(Long id);
    
    public List<KatastralneUzemieDto> getAllKatastralneUzemie();
}
