/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fbm.vutbr.martinsakac.bakalarka.dto.upravene;

import fbm.vutbr.martinsakac.bakalarka.dto.ListVlastnictvaDto;
import fbm.vutbr.martinsakac.bakalarka.dto.ParcelaDto;
import fbm.vutbr.martinsakac.bakalarka.dto.PodielDto;
import fbm.vutbr.martinsakac.bakalarka.dto.UzivatelDto;
import fbm.vutbr.martinsakac.bakalarka.dto.VlastnikDto;
import java.math.BigDecimal;
import java.util.List;
import org.joda.time.LocalDate;

/**
 * 
 * @author martin sakac
 */
public class NajomnaZmluvaDetailDto {
    
    private Long id;
    private LocalDate datumZhotovenia;
    private LocalDate platnostOd;
    private LocalDate platnostDo;
    private BigDecimal sadzba;
    private UzivatelDto uzivatel;
    
    private Long pocetRokov;
    private BigDecimal sucetVymier;
    private BigDecimal vyplatenaCiastka;
    
    private VlastnikDto vlastnik;
    private List<PodielDto> podiely;
    private List<ParcelaDto> parcely;

    public void setPocetRokov(Long pocetRokov) {
        this.pocetRokov = pocetRokov;
    }

    public void setSucetVymier(BigDecimal sucetVymier) {
        this.sucetVymier = sucetVymier;
    }

    public void setVyplatenaCiastka(BigDecimal vyplatenaCiastka) {
        this.vyplatenaCiastka = vyplatenaCiastka;
    }

    public Long getPocetRokov() {
        return pocetRokov;
    }

    public BigDecimal getSucetVymier() {
        return sucetVymier;
    }

    public BigDecimal getVyplatenaCiastka() {
        return vyplatenaCiastka;
    }

    public Long getId() {
        return id;
    }

    public LocalDate getDatumZhotovenia() {
        return datumZhotovenia;
    }

    public LocalDate getPlatnostOd() {
        return platnostOd;
    }

    public LocalDate getPlatnostDo() {
        return platnostDo;
    }

    public BigDecimal getSadzba() {
        return sadzba;
    }

    public UzivatelDto getUzivatel() {
        return uzivatel;
    }

    public VlastnikDto getVlastnik() {
        return vlastnik;
    }

    public List<PodielDto> getPodiely() {
        return podiely;
    }

    public List<ParcelaDto> getParcely() {
        return parcely;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setDatumZhotovenia(LocalDate datumZhotovenia) {
        this.datumZhotovenia = datumZhotovenia;
    }

    public void setPlatnostOd(LocalDate platnostOd) {
        this.platnostOd = platnostOd;
    }

    public void setPlatnostDo(LocalDate platnostDo) {
        this.platnostDo = platnostDo;
    }

    public void setSadzba(BigDecimal sadzba) {
        this.sadzba = sadzba;
    }

    public void setUzivatel(UzivatelDto uzivatel) {
        this.uzivatel = uzivatel;
    }

    public void setVlastnik(VlastnikDto vlastnik) {
        this.vlastnik = vlastnik;
    }

    public void setPodiely(List<PodielDto> podiely) {
        this.podiely = podiely;
    }

    public void setParcely(List<ParcelaDto> parcely) {
        this.parcely = parcely;
    }

    public NajomnaZmluvaDetailDto() {
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final NajomnaZmluvaDetailDto other = (NajomnaZmluvaDetailDto) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "NajomnaZmluvaDetailDto{" + "id=" + id + ", datumZhotovenia=" + datumZhotovenia + ", platnostOd=" + platnostOd + ", platnostDo=" + platnostDo + ", sadzba=" + sadzba + ", uzivatel=" + uzivatel + ", vlastnik=" + vlastnik + ", podiely=" + podiely + ", parcely=" + parcely + ", katastralneUzemia=" + '}';
    }
    
    
}
