/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.service;

import fbm.vutbr.martinsakac.bakalarka.dto.SpolocnostDto;
import java.util.List;

/**
 *
 * @author martin
 */
public interface SpolocnostService {
    
    public void addSpolocnost(SpolocnostDto spolocnost);
    public void updateSpolocnost(SpolocnostDto spolocnost);
    public void deleteSpolocnost(SpolocnostDto spolocnost);
    public SpolocnostDto getSpolocnostById(Long id);
    public List<SpolocnostDto> getAllSpolocnost();
}
