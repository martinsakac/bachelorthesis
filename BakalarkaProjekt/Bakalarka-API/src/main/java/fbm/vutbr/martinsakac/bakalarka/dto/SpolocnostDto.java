/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.dto;

/**
 *
 * @author martin
 */
public class SpolocnostDto {
    
    private Long id;
    private String nazov;
    private String ico;
    private String pravnaForma;

    public SpolocnostDto(Long id, String nazov, String ico, String pravnaForma) {
        this.id = id;
        this.nazov = nazov;
        this.ico = ico;
        this.pravnaForma = pravnaForma;
    }

    public SpolocnostDto(String nazov, String ico, String pravnaForma) {
        this.nazov = nazov;
        this.ico = ico;
        this.pravnaForma = pravnaForma;
    }

    public SpolocnostDto() {
    }

    public Long getId() {
        return id;
    }

    public String getNazov() {
        return nazov;
    }

    public String getIco() {
        return ico;
    }

    public String getPravnaForma() {
        return pravnaForma;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setNazov(String nazov) {
        this.nazov = nazov;
    }

    public void setIco(String ico) {
        this.ico = ico;
    }

    public void setPravnaForma(String pravnaForma) {
        this.pravnaForma = pravnaForma;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SpolocnostDto other = (SpolocnostDto) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "SpolocnostDto{" + "id=" + id + ", nazov=" + nazov + ", ico=" + ico + ", pravnaForma=" + pravnaForma + '}';
    }
    
    
}
