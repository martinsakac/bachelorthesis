/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.service;

import fbm.vutbr.martinsakac.bakalarka.dto.ObecDto;
import java.util.List;

/**
 *
 * @author martin
 */
public interface ObecService {
    
    public void addObec(ObecDto obec);
    public void updateObec(ObecDto obec);
    public void deleteObec(ObecDto obec);
    public ObecDto getObecById(Long id);
    public List<ObecDto> getAllObec();
}
