/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.service;

import fbm.vutbr.martinsakac.bakalarka.dto.KrajDto;
import java.util.List;

/**
 *
 * @author martin
 */
public interface KrajService {
    
    public void addKraj(KrajDto kraj);
    public void updateKraj(KrajDto kraj);
    public void deleteKraj(KrajDto kraj);
    public KrajDto getKrajById(Long id);
    public List<KrajDto> getAllKraj();
}
