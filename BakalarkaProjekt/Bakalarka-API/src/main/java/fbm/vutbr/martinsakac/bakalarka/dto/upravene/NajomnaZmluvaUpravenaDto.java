/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.dto.upravene;

import fbm.vutbr.martinsakac.bakalarka.dto.UzivatelDto;
import org.joda.time.LocalDate;
import java.math.BigDecimal;

/**
 *
 * @author martin
 */
public class NajomnaZmluvaUpravenaDto {
    private Long id;
    private Long idVlastnika;
    private LocalDate datumZhotovenia;
    private LocalDate platnostOd;
    private LocalDate platnostDo;
    private BigDecimal sadzba;
    private UzivatelDto uzivatel;
    private Long pocetRokov;
    private BigDecimal sucetVymier;
    private BigDecimal vyplatenaCiastka;

    public NajomnaZmluvaUpravenaDto() {
    }

    public Long getId() {
        return id;
    }

    public void setIdVlastnika(Long idVlastnika) {
        this.idVlastnika = idVlastnika;
    }

    public void setSucetVymier(BigDecimal sucetVymier) {
        this.sucetVymier = sucetVymier;
    }

    public BigDecimal getSucetVymier() {
        return sucetVymier;
    }

    public Long getIdVlastnika() {
        return idVlastnika;
    }

    public LocalDate getDatumZhotovenia() {
        return datumZhotovenia;
    }

    public LocalDate getPlatnostOd() {
        return platnostOd;
    }

    public LocalDate getPlatnostDo() {
        return platnostDo;
    }

    public BigDecimal getSadzba() {
        return sadzba;
    }

    public UzivatelDto getUzivatel() {
        return uzivatel;
    }

    public Long getPocetRokov() {
        return pocetRokov;
    }

    public BigDecimal getVyplatenaCiastka() {
        return vyplatenaCiastka;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setDatumZhotovenia(LocalDate datumZhotovenia) {
        this.datumZhotovenia = datumZhotovenia;
    }

    public void setPlatnostOd(LocalDate platnostOd) {
        this.platnostOd = platnostOd;
    }

    public void setPlatnostDo(LocalDate platnostDo) {
        this.platnostDo = platnostDo;
    }

    public void setSadzba(BigDecimal sadzba) {
        this.sadzba = sadzba;
    }

    public void setUzivatel(UzivatelDto uzivatel) {
        this.uzivatel = uzivatel;
    }

    public void setPocetRokov(Long pocetRokov) {
        this.pocetRokov = pocetRokov;
    }

    public void setVyplatenaCiastka(BigDecimal vyplatenaCiastka) {
        this.vyplatenaCiastka = vyplatenaCiastka;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final NajomnaZmluvaUpravenaDto other = (NajomnaZmluvaUpravenaDto) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "NajomnaZmluvaUpravenaDto{" + "id=" + id + ", datumZhotovenia=" + datumZhotovenia + ", platnostOd=" + platnostOd + ", platnostDo=" + platnostDo + ", sadzba=" + sadzba + ", uzivatel=" + uzivatel + ", pocetRokov=" + pocetRokov + ", vyplatenaCiastka=" + vyplatenaCiastka + '}';
    }
}
