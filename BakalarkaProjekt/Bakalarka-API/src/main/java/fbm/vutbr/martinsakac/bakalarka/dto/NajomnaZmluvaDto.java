/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.dto;

import java.util.List;
import org.joda.time.LocalDate;
import java.math.BigDecimal;

/**
 *
 * @author martin
 */
public class NajomnaZmluvaDto {
    
    private Long id;
    private LocalDate datumZhotovenia;
    private LocalDate platnostOd;
    private LocalDate platnostDo;
    private BigDecimal sadzba;
    private UzivatelDto uzivatel;
    private Integer current;
    private Integer hasValidFollower;

    public NajomnaZmluvaDto(Long id, LocalDate datumZhotovenia, LocalDate platnostOd, LocalDate platnostDo, BigDecimal sadzba, UzivatelDto uzivatel) {
        this.id = id;
        this.datumZhotovenia = datumZhotovenia;
        this.platnostOd = platnostOd;
        this.platnostDo = platnostDo;
        this.sadzba = sadzba;
        this.uzivatel = uzivatel;
    }

    public NajomnaZmluvaDto(LocalDate datumZhotovenia, LocalDate platnostOd, LocalDate platnostDo, BigDecimal sadzba, UzivatelDto uzivatel) {
        this.datumZhotovenia = datumZhotovenia;
        this.platnostOd = platnostOd;
        this.platnostDo = platnostDo;
        this.sadzba = sadzba;
        this.uzivatel = uzivatel;
    }

    public NajomnaZmluvaDto() {
    }

    public Integer getCurrent() {
        return current;
    }

    public Integer getHasValidFollower() {
        return hasValidFollower;
    }

    public void setCurrent(Integer current) {
        this.current = current;
    }

    public void setHasValidFollower(Integer hasValidFollower) {
        this.hasValidFollower = hasValidFollower;
    }

    public Long getId() {
        return id;
    }

    public LocalDate getDatumZhotovenia() {
        return datumZhotovenia;
    }

    public LocalDate getPlatnostOd() {
        return platnostOd;
    }

    public LocalDate getPlatnostDo() {
        return platnostDo;
    }

    public BigDecimal getSadzba() {
        return sadzba;
    }

    public UzivatelDto getUzivatel() {
        return uzivatel;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setDatumZhotovenia(LocalDate datumZhotovenia) {
        this.datumZhotovenia = datumZhotovenia;
    }

    public void setPlatnostOd(LocalDate platnostOd) {
        this.platnostOd = platnostOd;
    }

    public void setPlatnostDo(LocalDate platnostDo) {
        this.platnostDo = platnostDo;
    }

    public void setSadzba(BigDecimal sadzba) {
        this.sadzba = sadzba;
    }

    public void setUzivatel(UzivatelDto uzivatel) {
        this.uzivatel = uzivatel;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 41 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final NajomnaZmluvaDto other = (NajomnaZmluvaDto) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "NajomnaZmluvaDto{" + "id=" + id + ", datumZhotovenia=" + datumZhotovenia + ", platnostOd=" + platnostOd + ", platnostDo=" + platnostDo + ", sadzba=" + sadzba + ", uzivatel=" + uzivatel + '}';
    }
    
    
}
