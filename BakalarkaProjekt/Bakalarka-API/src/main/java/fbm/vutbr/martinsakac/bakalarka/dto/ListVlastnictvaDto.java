/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.dto;

/**
 *
 * @author martin
 */
public class ListVlastnictvaDto {
    
    private Long id;
    private String cisloListu;

    public ListVlastnictvaDto(Long id, String cisloListu) {
        this.id = id;
        this.cisloListu = cisloListu;
    }

    public ListVlastnictvaDto(String cisloListu) {
        this.cisloListu = cisloListu;
    }

    public ListVlastnictvaDto() {
    }

    public Long getId() {
        return id;
    }

    public String getCisloListu() {
        return cisloListu;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setCisloListu(String cisloListu) {
        this.cisloListu = cisloListu;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ListVlastnictvaDto other = (ListVlastnictvaDto) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ListVlastnictvaDto{" + "id=" + id + ", cisloListu=" + cisloListu + '}';
    }
    
    
}
