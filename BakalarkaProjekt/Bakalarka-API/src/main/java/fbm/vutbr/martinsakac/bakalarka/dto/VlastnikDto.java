/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.dto;

import org.joda.time.LocalDate;

/**
 *
 * @author martin
 */
public class VlastnikDto {
    
    private Long id;
    private String meno;
    private String priezvisko;
    private String titul;
    private String evidencneCislo;
    private LocalDate datumNarodenia;
    private String miestoNarodenia;
    private String adresa;
    private String cisloUctu;
    private String telCislo;
    private String ico;

    public VlastnikDto() {
    }

    public VlastnikDto(Long id, String meno, String priezvisko, String titul, LocalDate datumNarodenia, String miestoNarodenia, String adresa, String cisloUctu, String ico) {
        this.id = id;
        this.meno = meno;
        this.priezvisko = priezvisko;
        this.titul = titul;
        this.datumNarodenia = datumNarodenia;
        this.miestoNarodenia = miestoNarodenia;
        this.adresa = adresa;
        this.cisloUctu = cisloUctu;
        this.ico = ico;
    }

    public VlastnikDto(String meno, String priezvisko, String titul, LocalDate datumNarodenia, String miestoNarodenia, String adresa, String cisloUctu, String ico) {
        this.meno = meno;
        this.priezvisko = priezvisko;
        this.titul = titul;
        this.datumNarodenia = datumNarodenia;
        this.miestoNarodenia = miestoNarodenia;
        this.adresa = adresa;
        this.cisloUctu = cisloUctu;
        this.ico = ico;
    }

    public Long getId() {
        return id;
    }

    public String getMeno() {
        return meno;
    }

    public String getPriezvisko() {
        return priezvisko;
    }

    public String getTitul() {
        return titul;
    }

    public LocalDate getDatumNarodenia() {
        return datumNarodenia;
    }

    public String getMiestoNarodenia() {
        return miestoNarodenia;
    }

    public String getAdresa() {
        return adresa;
    }

    public String getCisloUctu() {
        return cisloUctu;
    }

    public String getIco() {
        return ico;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setMeno(String meno) {
        this.meno = meno;
    }

    public void setPriezvisko(String priezvisko) {
        this.priezvisko = priezvisko;
    }

    public void setTitul(String titul) {
        this.titul = titul;
    }

    public void setDatumNarodenia(LocalDate datumNarodenia) {
        this.datumNarodenia = datumNarodenia;
    }

    public void setMiestoNarodenia(String miestoNarodenia) {
        this.miestoNarodenia = miestoNarodenia;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public void setCisloUctu(String cisloUctu) {
        this.cisloUctu = cisloUctu;
    }

    public void setIco(String ico) {
        this.ico = ico;
    }

    public void setEvidencneCislo(String evidencneCislo) {
        this.evidencneCislo = evidencneCislo;
    }

    public void setTelCislo(String telCislo) {
        this.telCislo = telCislo;
    }

    public String getEvidencneCislo() {
        return evidencneCislo;
    }

    public String getTelCislo() {
        return telCislo;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 19 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final VlastnikDto other = (VlastnikDto) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "VlastnikDto{" + "id=" + id + ", meno=" + meno + ", priezvisko=" + priezvisko + ", titul=" + titul + ", datumNarodenia=" + datumNarodenia + ", miestoNarodenia=" + miestoNarodenia + ", adresa=" + adresa + ", cisloUctu=" + cisloUctu + ", ico=" + ico + '}';
    }
    
    
}
