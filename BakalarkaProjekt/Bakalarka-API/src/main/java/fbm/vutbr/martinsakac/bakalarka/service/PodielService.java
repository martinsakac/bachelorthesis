/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.service;

import fbm.vutbr.martinsakac.bakalarka.dto.PodielDto;
import java.util.List;

/**
 *
 * @author martin
 */
public interface PodielService {
    
    public void addPodiel(PodielDto podiel);
    public void updatePodiel(PodielDto podiel);
    public void deletePodiel(PodielDto podiel);
    public PodielDto getPodielById(Long id);
    public List<PodielDto> getAllPodiel();
}
