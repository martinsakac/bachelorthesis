/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.dto;

import java.util.List;

/**
 *
 * @author martin
 */
public class PodielDto {
    
    private Long id;
    private Integer percentualnyPodiel;
    private VlastnikDto vlastnik;
    private ListVlastnictvaDto listVlastnictva;

    public PodielDto(Long id, Integer percentualnyPodiel, VlastnikDto vlastnik, ListVlastnictvaDto listVlastnictva) {
        this.id = id;
        this.percentualnyPodiel = percentualnyPodiel;
        this.vlastnik = vlastnik;
        this.listVlastnictva = listVlastnictva;
    }

    public PodielDto(Integer percentualnyPodiel, VlastnikDto vlastnik, ListVlastnictvaDto listVlastnictva) {
        this.percentualnyPodiel = percentualnyPodiel;
        this.vlastnik = vlastnik;
        this.listVlastnictva = listVlastnictva;
    }

    public PodielDto() {
    }

    public Long getId() {
        return id;
    }

    public Integer getPercentualnyPodiel() {
        return percentualnyPodiel;
    }

    public VlastnikDto getVlastnik() {
        return vlastnik;
    }

    public ListVlastnictvaDto getListVlastnictva() {
        return listVlastnictva;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setPercentualnyPodiel(Integer percentualnyPodiel) {
        this.percentualnyPodiel = percentualnyPodiel;
    }

    public void setVlastnik(VlastnikDto vlastnik) {
        this.vlastnik = vlastnik;
    }

    public void setListVlastnictva(ListVlastnictvaDto listVlastnictva) {
        this.listVlastnictva = listVlastnictva;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PodielDto other = (PodielDto) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "PodielDto{" + "id=" + id + ", percentualnyPodiel=" + percentualnyPodiel + ", vlastnik=" + vlastnik + ", listVlastnictva=" + listVlastnictva + '}';
    }
    
    
}
