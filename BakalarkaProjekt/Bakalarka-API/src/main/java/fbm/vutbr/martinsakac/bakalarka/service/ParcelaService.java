/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.service;

import fbm.vutbr.martinsakac.bakalarka.dto.ParcelaDto;
import java.util.List;

/**
 *
 * @author martin
 */
public interface ParcelaService {
    
    public void addParcela(ParcelaDto parcela);
    public void updateParcela(ParcelaDto parcela);
    public void deleteParcela(ParcelaDto parcela);
    public ParcelaDto getParcelaById(Long id);
    public List<ParcelaDto> getAllParcela();
    public List<ParcelaDto> getAllParcelaByVlastnikId(Long idVlastnik);
}
