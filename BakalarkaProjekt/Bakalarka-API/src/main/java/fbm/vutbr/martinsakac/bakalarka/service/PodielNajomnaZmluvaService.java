/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.service;

import fbm.vutbr.martinsakac.bakalarka.dto.PodielNajomnaZmluvaDto;
import java.util.List;

/**
 *
 * @author martin
 */
public interface PodielNajomnaZmluvaService {
    
    public void addPodielNajomnaZmluva(PodielNajomnaZmluvaDto pnzDto);
    public void updatePodielNajomnaZmluva(PodielNajomnaZmluvaDto pnzDto);
    public void deletePodielNajomnaZmluva(PodielNajomnaZmluvaDto pnzDto);
    public PodielNajomnaZmluvaDto getPodielNajomnaZmluvaById(Long id);
    public List<PodielNajomnaZmluvaDto> getAllPodielNajomnaZmluva();
}
