/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.dto.upravene;

import fbm.vutbr.martinsakac.bakalarka.dto.ParcelaDto;
import org.joda.time.LocalDate;
import java.util.List;

/**
 *
 * @author martin
 */

public class VlastnikSoZmluvamiDto {
    private Long id;
    private String meno;
    private String priezvisko;
    private String titul;
    private String evidencneCislo;
    private LocalDate datumNarodenia;
    private String miestoNarodenia;
    private String adresa;
    private String cisloUctu;
    private String telCislo;
    private String ico;
    private List<NajomnaZmluvaUpravenaDto> listZmluv;
    private List<ParcelaDto> listParciel;

    public VlastnikSoZmluvamiDto() {
    }

    public Long getId() {
        return id;
    }

    public String getMeno() {
        return meno;
    }

    public void setListParciel(List<ParcelaDto> listParciel) {
        this.listParciel = listParciel;
    }

    public List<ParcelaDto> getListParciel() {
        return listParciel;
    }

    public String getPriezvisko() {
        return priezvisko;
    }

    public String getTitul() {
        return titul;
    }

    public String getEvidencneCislo() {
        return evidencneCislo;
    }

    public LocalDate getDatumNarodenia() {
        return datumNarodenia;
    }

    public String getMiestoNarodenia() {
        return miestoNarodenia;
    }

    public String getAdresa() {
        return adresa;
    }

    public String getCisloUctu() {
        return cisloUctu;
    }

    public String getTelCislo() {
        return telCislo;
    }

    public String getIco() {
        return ico;
    }

    public List<NajomnaZmluvaUpravenaDto> getListZmluv() {
        return listZmluv;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setMeno(String meno) {
        this.meno = meno;
    }

    public void setPriezvisko(String priezvisko) {
        this.priezvisko = priezvisko;
    }

    public void setTitul(String titul) {
        this.titul = titul;
    }

    public void setEvidencneCislo(String evidencneCislo) {
        this.evidencneCislo = evidencneCislo;
    }

    public void setDatumNarodenia(LocalDate datumNarodenia) {
        this.datumNarodenia = datumNarodenia;
    }

    public void setMiestoNarodenia(String miestoNarodenia) {
        this.miestoNarodenia = miestoNarodenia;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public void setCisloUctu(String cisloUctu) {
        this.cisloUctu = cisloUctu;
    }

    public void setTelCislo(String telCislo) {
        this.telCislo = telCislo;
    }

    public void setIco(String ico) {
        this.ico = ico;
    }

    public void setListZmluv(List<NajomnaZmluvaUpravenaDto> listZmluv) {
        this.listZmluv = listZmluv;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 19 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final VlastnikSoZmluvamiDto other = (VlastnikSoZmluvamiDto) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "VlastnikSoZmluvamiDto{" + "id=" + id + ", meno=" + meno + ", priezvisko=" + priezvisko + ", titul=" + titul + ", evidencneCislo=" + evidencneCislo + ", datumNarodenia=" + datumNarodenia + ", miestoNarodenia=" + miestoNarodenia + ", adresa=" + adresa + ", cisloUctu=" + cisloUctu + ", telCislo=" + telCislo + ", ico=" + ico + ", listZmluv=" + listZmluv + '}';
    }
    
    
}
