/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.dto;

import java.math.BigDecimal;
        
/**
 *
 * @author martin
 */
public class ParcelaDto {

    private Long id;
    private String typ;
    private String parcelneCislo;
    private BigDecimal vymera;
    private String sposobVyuzitiaPozemku;
    private String prislusnostKzuo;
    private KatastralneUzemieDto katastralneUzemie;
    private ListVlastnictvaDto listVlastnictva;

    public ParcelaDto(Long id, String typ, String parcelneCislo, BigDecimal vymera, 
            String sposobVyuzitiaPozemku, String prislusnostKzuo, KatastralneUzemieDto katastralneUzemie, ListVlastnictvaDto listVlastnictva) {
        this.id = id;
        this.typ = typ;
        this.parcelneCislo = parcelneCislo;
        this.vymera = vymera;
        this.sposobVyuzitiaPozemku = sposobVyuzitiaPozemku;
        this.prislusnostKzuo = prislusnostKzuo;
        this.katastralneUzemie = katastralneUzemie;
        this.listVlastnictva = listVlastnictva;
    }

    public ParcelaDto(String typ, String parcelneCislo, BigDecimal vymera, String sposobVyuzitiaPozemku, 
            String prislusnostKzuo, KatastralneUzemieDto katastralneUzemie, ListVlastnictvaDto listVlastnictva) {
        this.typ = typ;
        this.parcelneCislo = parcelneCislo;
        this.vymera = vymera;
        this.sposobVyuzitiaPozemku = sposobVyuzitiaPozemku;
        this.prislusnostKzuo = prislusnostKzuo;
        this.katastralneUzemie = katastralneUzemie;
        this.listVlastnictva = listVlastnictva;
    }

    public ParcelaDto() {
    }

    public Long getId() {
        return id;
    }

    public String getTyp() {
        return typ;
    }

    public String getParcelneCislo() {
        return parcelneCislo;
    }

    public BigDecimal getVymera() {
        return vymera;
    }

    public String getSposobVyuzitiaPozemku() {
        return sposobVyuzitiaPozemku;
    }

    public String getPrislusnostKzuo() {
        return prislusnostKzuo;
    }

    public KatastralneUzemieDto getKatastralneUzemie() {
        return katastralneUzemie;
    }

    public ListVlastnictvaDto getListVlastnictva() {
        return listVlastnictva;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setTyp(String typ) {
        this.typ = typ;
    }

    public void setParcelneCislo(String parcelneCislo) {
        this.parcelneCislo = parcelneCislo;
    }

    public void setVymera(BigDecimal vymera) {
        this.vymera = vymera;
    }

    public void setSposobVyuzitiaPozemku(String sposobVyuzitiaPozemku) {
        this.sposobVyuzitiaPozemku = sposobVyuzitiaPozemku;
    }

    public void setPrislusnostKzuo(String prislusnostKzuo) {
        this.prislusnostKzuo = prislusnostKzuo;
    }

    public void setKatastralneUzemie(KatastralneUzemieDto katastralneUzemie) {
        this.katastralneUzemie = katastralneUzemie;
    }

    public void setListVlastnictva(ListVlastnictvaDto listVlastnictva) {
        this.listVlastnictva = listVlastnictva;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ParcelaDto other = (ParcelaDto) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ParcelaDto{" + "id=" + id + ", typ=" + typ + ", parcelneCislo=" + parcelneCislo + ", vymera=" + vymera + ", sposobVyuzitiaPozemku=" + sposobVyuzitiaPozemku + ", prislusnostKzuo=" + prislusnostKzuo + ", katastralneUzemie=" + katastralneUzemie + ", listVlastnictva=" + listVlastnictva + '}';
    }
    
    
}
