/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.dto;

/**
 *
 * @author martin
 */
public class UzivatelDto {
    
    public enum Rola {USER, ADMIN}
    
    private Long id;
    private String meno;
    private String priezvisko;
    private String pracovneZaradenie;
    private String uzivatelskeMeno;
    private String uzivatelskeHeslo;
    private Rola uzivatelskaRola;
    private SpolocnostDto spolocnost;

    public UzivatelDto(Long id, String meno, String priezvisko, String pracovneZaradenie, String uzivatelskeMeno, String uzivatelskeHeslo, SpolocnostDto spolocnost) {
        this.id = id;
        this.meno = meno;
        this.priezvisko = priezvisko;
        this.pracovneZaradenie = pracovneZaradenie;
        this.uzivatelskeMeno = uzivatelskeMeno;
        this.uzivatelskeHeslo = uzivatelskeHeslo;
        this.spolocnost = spolocnost;
    }

    public UzivatelDto(String meno, String priezvisko, String pracovneZaradenie, String uzivatelskeMeno, String uzivatelskeHeslo, SpolocnostDto spolocnost) {
        this.meno = meno;
        this.priezvisko = priezvisko;
        this.pracovneZaradenie = pracovneZaradenie;
        this.uzivatelskeMeno = uzivatelskeMeno;
        this.uzivatelskeHeslo = uzivatelskeHeslo;
        this.spolocnost = spolocnost;
    }

    public UzivatelDto() {
    }

    public void setUzivatelskaRola(Rola uzivatelskaRola) {
        this.uzivatelskaRola = uzivatelskaRola;
    }

    public Rola getUzivatelskaRola() {
        return uzivatelskaRola;
    }

    public Long getId() {
        return id;
    }

    public String getMeno() {
        return meno;
    }

    public String getPriezvisko() {
        return priezvisko;
    }

    public String getPracovneZaradenie() {
        return pracovneZaradenie;
    }

    public String getUzivatelskeMeno() {
        return uzivatelskeMeno;
    }

    public String getUzivatelskeHeslo() {
        return uzivatelskeHeslo;
    }

    public SpolocnostDto getSpolocnost() {
        return spolocnost;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setMeno(String meno) {
        this.meno = meno;
    }

    public void setPriezvisko(String priezvisko) {
        this.priezvisko = priezvisko;
    }

    public void setPracovneZaradenie(String pracovneZaradenie) {
        this.pracovneZaradenie = pracovneZaradenie;
    }

    public void setUzivatelskeMeno(String uzivatelskeMeno) {
        this.uzivatelskeMeno = uzivatelskeMeno;
    }

    public void setUzivatelskeHeslo(String uzivatelskeHeslo) {
        this.uzivatelskeHeslo = uzivatelskeHeslo;
    }

    public void setSpolocnost(SpolocnostDto spolocnost) {
        this.spolocnost = spolocnost;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UzivatelDto other = (UzivatelDto) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "UzivatelDto{" + "id=" + id + ", meno=" + meno + ", priezvisko=" + priezvisko + ", pracovneZaradenie=" + pracovneZaradenie + ", uzivatelskeMeno=" + uzivatelskeMeno + ", uzivatelskeHeslo=" + uzivatelskeHeslo + ", spolocnost=" + spolocnost + '}';
    }
    
    
}
