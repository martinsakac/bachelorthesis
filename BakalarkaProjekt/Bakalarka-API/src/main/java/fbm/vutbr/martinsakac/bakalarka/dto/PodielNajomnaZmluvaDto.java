/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.dto;

/**
 *
 * @author martin
 */

public class PodielNajomnaZmluvaDto {
    
    private Long id;
    private PodielDto podiel;
    private NajomnaZmluvaDto najomnaZmluva;

    public PodielNajomnaZmluvaDto() {
    }

    public PodielNajomnaZmluvaDto(Long id, PodielDto podiel, NajomnaZmluvaDto najomnaZmluvaDto) {
        this.id = id;
        this.podiel = podiel;
        this.najomnaZmluva = najomnaZmluvaDto;
    }

    public PodielNajomnaZmluvaDto(PodielDto podiel, NajomnaZmluvaDto najomnaZmluvaDto) {
        this.podiel = podiel;
        this.najomnaZmluva = najomnaZmluvaDto;
    }

    public Long getId() {
        return id;
    }

    public PodielDto getPodiel() {
        return podiel;
    }

    public NajomnaZmluvaDto getNajomnaZmluva() {
        return najomnaZmluva;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setPodiel(PodielDto podiel) {
        this.podiel = podiel;
    }

    public void setNajomnaZmluva(NajomnaZmluvaDto najomnaZmluvaDto) {
        this.najomnaZmluva = najomnaZmluvaDto;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PodielNajomnaZmluvaDto other = (PodielNajomnaZmluvaDto) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "PodielNajomnaZmluvaDto{" + "id=" + id + ", podiel=" + podiel + ", najomnaZmluvaDto=" + najomnaZmluva + '}';
    }
    
    
}
