/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.service;

import fbm.vutbr.martinsakac.bakalarka.dto.UzivatelDto;
import java.util.List;

/**
 *
 * @author martin
 */
public interface UzivatelService {

    public void addUzivatel(UzivatelDto uzivatel);
    public void updateUzivatel(UzivatelDto uzivatel);
    public void deleteUzivatel(UzivatelDto uzivatel);
    public UzivatelDto getUzivatelById(Long id);
    public List<UzivatelDto> getAllUzivatel();
    public UzivatelDto login(String login, String password);
}
