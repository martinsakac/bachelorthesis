/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.dto;

/**
 *
 * @author martin
 */
public class ObecDto {
    
    private Long id;
    private String nazov;
    private OkresDto okres;

    public ObecDto(Long id, String nazov, OkresDto okres) {
        this.id = id;
        this.nazov = nazov;
        this.okres = okres;
    }

    public ObecDto(String nazov, OkresDto okres) {
        this.nazov = nazov;
        this.okres = okres;
    }

    public ObecDto() {
    }

    public Long getId() {
        return id;
    }

    public String getNazov() {
        return nazov;
    }

    public OkresDto getOkres() {
        return okres;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setNazov(String nazov) {
        this.nazov = nazov;
    }

    public void setOkres(OkresDto okres) {
        this.okres = okres;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ObecDto other = (ObecDto) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ObecDto{" + "id=" + id + ", nazov=" + nazov + ", okres=" + okres + '}';
    }
    
    
}
