/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.dto;

/**
 *
 * @author martin
 */
public class OkresDto {
    
    private Long id;
    private String nazov;
    private KrajDto kraj;

    public OkresDto(Long id, String nazov, KrajDto kraj) {
        this.id = id;
        this.nazov = nazov;
        this.kraj = kraj;
    }

    public OkresDto(String nazov, KrajDto kraj) {
        this.nazov = nazov;
        this.kraj = kraj;
    }

    public OkresDto() {
    }

    public Long getId() {
        return id;
    }

    public String getNazov() {
        return nazov;
    }

    public KrajDto getKraj() {
        return kraj;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setNazov(String nazov) {
        this.nazov = nazov;
    }

    public void setKraj(KrajDto kraj) {
        this.kraj = kraj;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OkresDto other = (OkresDto) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "OkresDto{" + "id=" + id + ", nazov=" + nazov + ", kraj=" + kraj + '}';
    }
    
    
}
