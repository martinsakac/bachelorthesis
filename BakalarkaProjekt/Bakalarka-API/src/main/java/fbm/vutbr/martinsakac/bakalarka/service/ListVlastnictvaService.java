/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.service;

import fbm.vutbr.martinsakac.bakalarka.dto.ListVlastnictvaDto;
import java.util.List;

/**
 *
 * @author martin
 */
public interface ListVlastnictvaService {
    
    public void addListVlastnictva(ListVlastnictvaDto lv);
    public void updateListVlastnictva(ListVlastnictvaDto lv);
    public void deleteListVlastnictva(ListVlastnictvaDto lv);
    public ListVlastnictvaDto getListVlastnictvaById(Long id);
    public List<ListVlastnictvaDto> getAllListVlastnictva();
}
