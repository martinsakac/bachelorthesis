/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fbm.vutbr.martinsakac.bakalarka.dto.upravene;

import java.math.BigDecimal;
import java.util.Map;

/**
 * 
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
public class NajomnaZmluvaDetailForXmlDto {
    private String datumZhotoveniaDen;
    private String datumZhotoveniaMesiac;
    private String datumZhotoveniaRok;
    
    private String platnostOdDen;
    private String platnostOdMesiac;
    private String platnostOdRok;
    
    private String platnostDoDen;
    private String platnostDoMesiac;
    private String platnostDoRok;
    
    private String sadzba;
    private String uzivatelMeno;
    private String uzivatelPriezvisko;
    
    private Long pocetRokov;
    private String sucetVymier;
    private String vyplatenaCiastka;
    
    private String vlastnikMeno;
    private String vlastnikPriezvisko;
    
    private Map<String, String> uzemiaRozloha;

    public String getDatumZhotoveniaDen() {
        return datumZhotoveniaDen;
    }

    public String getDatumZhotoveniaMesiac() {
        return datumZhotoveniaMesiac;
    }

    public String getDatumZhotoveniaRok() {
        return datumZhotoveniaRok;
    }

    public String getPlatnostOdDen() {
        return platnostOdDen;
    }

    public String getPlatnostOdMesiac() {
        return platnostOdMesiac;
    }

    public String getPlatnostOdRok() {
        return platnostOdRok;
    }

    public String getPlatnostDoDen() {
        return platnostDoDen;
    }

    public String getPlatnostDoMesiac() {
        return platnostDoMesiac;
    }

    public String getPlatnostDoRok() {
        return platnostDoRok;
    }

    public String getSadzba() {
        return sadzba;
    }

    public String getUzivatelMeno() {
        return uzivatelMeno;
    }

    public String getUzivatelPriezvisko() {
        return uzivatelPriezvisko;
    }

    public Long getPocetRokov() {
        return pocetRokov;
    }

    public String getSucetVymier() {
        return sucetVymier;
    }

    public String getVyplatenaCiastka() {
        return vyplatenaCiastka;
    }

    public String getVlastnikMeno() {
        return vlastnikMeno;
    }

    public String getVlastnikPriezvisko() {
        return vlastnikPriezvisko;
    }

    public Map<String, String> getUzemiaRozloha() {
        return uzemiaRozloha;
    }

    public void setDatumZhotoveniaDen(String datumZhotoveniaDen) {
        this.datumZhotoveniaDen = datumZhotoveniaDen;
    }

    public void setDatumZhotoveniaMesiac(String datumZhotoveniaMesiac) {
        this.datumZhotoveniaMesiac = datumZhotoveniaMesiac;
    }

    public void setDatumZhotoveniaRok(String datumZhotoveniaRok) {
        this.datumZhotoveniaRok = datumZhotoveniaRok;
    }

    public void setPlatnostOdDen(String platnostOdDen) {
        this.platnostOdDen = platnostOdDen;
    }

    public void setPlatnostOdMesiac(String platnostOdMesiac) {
        this.platnostOdMesiac = platnostOdMesiac;
    }

    public void setPlatnostOdRok(String platnostOdRok) {
        this.platnostOdRok = platnostOdRok;
    }

    public void setPlatnostDoDen(String platnostDoDen) {
        this.platnostDoDen = platnostDoDen;
    }

    public void setPlatnostDoMesiac(String platnostDoMesiac) {
        this.platnostDoMesiac = platnostDoMesiac;
    }

    public void setPlatnostDoRok(String platnostDoRok) {
        this.platnostDoRok = platnostDoRok;
    }

    public void setSadzba(String sadzba) {
        this.sadzba = sadzba;
    }

    public void setUzivatelMeno(String uzivatelMeno) {
        this.uzivatelMeno = uzivatelMeno;
    }

    public void setUzivatelPriezvisko(String uzivatelPriezvisko) {
        this.uzivatelPriezvisko = uzivatelPriezvisko;
    }

    public void setPocetRokov(Long pocetRokov) {
        this.pocetRokov = pocetRokov;
    }

    public void setSucetVymier(String sucetVymier) {
        this.sucetVymier = sucetVymier;
    }

    public void setVyplatenaCiastka(String vyplatenaCiastka) {
        this.vyplatenaCiastka = vyplatenaCiastka;
    }

    public void setVlastnikMeno(String vlastnikMeno) {
        this.vlastnikMeno = vlastnikMeno;
    }

    public void setVlastnikPriezvisko(String vlastnikPriezvisko) {
        this.vlastnikPriezvisko = vlastnikPriezvisko;
    }

    public void setUzemiaRozloha(Map<String, String> uzemiaRozloha) {
        this.uzemiaRozloha = uzemiaRozloha;
    }

    @Override
    public String toString() {
        return "NajomnaZmluvaDetailForXmlDto{" + "datumZhotoveniaDen=" + datumZhotoveniaDen + ", datumZhotoveniaMesiac=" + datumZhotoveniaMesiac + ", datumZhotoveniaRok=" + datumZhotoveniaRok + ", platnostOdDen=" + platnostOdDen + ", platnostOdMesiac=" + platnostOdMesiac + ", platnostOdRok=" + platnostOdRok + ", platnostDoDen=" + platnostDoDen + ", platnostDoMesiac=" + platnostDoMesiac + ", platnostDoRok=" + platnostDoRok + ", sadzba=" + sadzba + ", uzivatelMeno=" + uzivatelMeno + ", uzivatelPriezvisko=" + uzivatelPriezvisko + ", pocetRokov=" + pocetRokov + ", sucetVymier=" + sucetVymier + ", vyplatenaCiastka=" + vyplatenaCiastka + ", vlastnikMeno=" + vlastnikMeno + ", vlastnikPriezvisko=" + vlastnikPriezvisko + ", uzemiaRozloha=" + uzemiaRozloha + '}';
    }
    
    
}
