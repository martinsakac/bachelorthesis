/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fbm.vutbr.martinsakac.bakalarka.service;

import fbm.vutbr.martinsakac.bakalarka.dto.NajomnaZmluvaDto;
import fbm.vutbr.martinsakac.bakalarka.dto.PodielDto;
import fbm.vutbr.martinsakac.bakalarka.dto.upravene.NajomnaZmluvaDetailDto;
import fbm.vutbr.martinsakac.bakalarka.dto.upravene.NajomnaZmluvaUpravenaDto;
import java.util.List;

/**
 *
 * @author martin
 */
public interface NajomnaZmluvaService {
    
    public void addNajomnaZmluva(NajomnaZmluvaDto nz);
    public void addNajomnaZmluvaSPodielmi(NajomnaZmluvaDto nz, List<PodielDto> podielList);
    public void updateNajomnaZmluva(NajomnaZmluvaDto nz);
    public void deleteNajomnaZmluva(NajomnaZmluvaDto nz);
    public NajomnaZmluvaDto getNajomnaZmluvaById(Long id);
    public List<NajomnaZmluvaDto> getAllNajomnaZmluva(String column, String direction);
    public List<NajomnaZmluvaUpravenaDto> getNajomnaZmluvaByVlastnikId(Long vlastnikId);
    public NajomnaZmluvaDetailDto getNajomnaZmluvaDetail(Long id);
}
